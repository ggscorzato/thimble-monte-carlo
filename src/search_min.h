/*****************************************************************************
 file: search_min.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _SEARCH_MIN_H
#define _SEARCH_MIN_H

dcomplex search_min_model(dcomplex phi0);
dcomplex search_min_lapack_model(dcomplex phi0);
void test_search_min();

#endif
