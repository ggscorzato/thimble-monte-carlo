/*****************************************************************************
 file: ferm_force_stoch.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "cgne.h"
#include "ferm_matrices.h"
#include "ferm_force.h"
#ifdef WITHFERM

/*
  Compute (stochastically) the fermionic part of the force, and accumulate the result on ff.
  ff_k += - conj(d_k S_F ) += xi M^-1 d_k M xi += xi conj( FM^-1 d_k FM ) xi
  M is the conjugate fermion matrix FM
  Mt is the transposed fermion matrix FM
  phi is the field
*/

void ferm_force_stoch(double * ff, double * phi, int nt, matrix_mult_ferm M, matrix_mult_ferm Mt){

  int r,fl,j,iter;
  double iNR=1./((double)g_NR);

  for(r=0;r<g_NR;r++){
    for(fl=0;fl<g_NF;fl++){

      /* find pb0 = xi FM^-1  (if no similar solution is available use the one at nt-1) */
      if(g_pb0[fl][r][nt][0]==0 && nt!=0) for(j=0;j<g_csize;j++) g_pb0[fl][r][nt][j] = g_pb0[fl][r][nt-1][j];
      iter=ferm_cgne(g_pb0[fl][r][nt],Mt,M,g_xi[r],g_noise_cgf_itermax,g_noise_cgf_tol,phi,g_csize,fl);

      //ferm_inverse_debug(fl,r,nt);

      /* Compute f_k += conj( pb0 * d_k FM)  * xi ~ conj( Tr[FM^-1 * d_k FM])  */
      ferm_cntr23_dM_stoch_model(ff,g_pb0[fl][r][nt],g_xi[r],iNR);
    }
  }
}

#if (USEMAGMA == 0)
void ferm_inverse_debug(int fl, int r, int nt){
  int i;
  double delta;
  dcomplex ctmp;

  fprintf(stdout,"cfr inv Mat[%d,%d,%d]\n",fl,r,nt);
  for(i=0;i<g_csize;i++){
    ctmp=conj(g_pb0[fl][r][nt][i])/sqrt((double)g_NR);
    delta=cabs(ctmp-g_A[i*g_csize+r]);
    if(delta>1e-15) fprintf(stdout,"diff cgne vs full inv Mat[%d,%d]=%g, [(%g+i*%g)!=(%g+i*%g)]\n",
			    r,i,delta,creal(ctmp),cimag(ctmp),creal(g_A[i*g_csize+r]),cimag(g_A[i*g_csize+r]));
  }
}
#endif
#endif
