/*******************************************************************************
 file: alloc_fields.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "global.h"
#include "init_model.h"
#include "ferm_cuda.h"
#include "alloc_fields.h"

void alloc_fields(int Nt, int first_alloc){

  int i,j,Ntp1,encond,ncond,edof,dof;

  if(first_alloc) if(g_proc_id==0) fprintf(stdout,"Allocating fields...");
  if(!first_alloc) if(g_proc_id==0) fprintf(stdout,"Re-allocating fields...");
  
  Ntp1=Nt+1;
  if(g_proc_coords[g_flowdir]==0){
    ncond = (Nt)*g_size + MAX(1,EXTRACOND);
    encond= (Nt)*g_esize + MAX(1,EXTRACOND);
  } else {
    ncond = (Ntp1)*g_size;
    encond= (Ntp1)*g_esize;
  }
  dof = (Ntp1)*g_size;
  edof= (Ntp1)*g_esize;
  g_lwfact=10;

  /********* fields of O(g_tvol x g_vol x g_nt_tot) *********/
  if(g_with_projected_mhessian==1){
    if(!first_alloc){
      free(g_thimb_tan_basis__); free(g_thimb_tan_basis_); free(g_thimb_tan_basis);
    }
    if(( g_thimb_tan_basis__= (double*)calloc((g_tvol*CPS)*Ntp1*g_size,sizeof(double))  )==NULL)
      fprintf(stderr,"g_thimb_tan_basis__ malloc error");
    if(( g_thimb_tan_basis_ = (double**)calloc((g_tvol*CPS)*Ntp1,sizeof(double*)) )==NULL)
      fprintf(stderr,"g_thimb_tan_basis_ malloc error");
    if(( g_thimb_tan_basis  = (double***)calloc((g_tvol*CPS),sizeof(double**)) )==NULL)
      fprintf(stderr,"g_thimb_tan_basis malloc error");
    g_thimb_tan_basis_[0]=g_thimb_tan_basis__;
    for(j=1;j<(g_tvol*CPS)*Ntp1;j++) g_thimb_tan_basis_[j]=g_thimb_tan_basis_[j-1] + g_size;
    g_thimb_tan_basis[0]=g_thimb_tan_basis_;
    for(j=1;j<(g_tvol*CPS);j++) g_thimb_tan_basis[j]=g_thimb_tan_basis[j-1] + Ntp1;
    /* Basis matrix for exact evaluation of det */
    if(( g_B =(dcomplex *)calloc(g_csize*g_csize,sizeof(dcomplex)) )==NULL) 
      fprintf(stderr,"B malloc error");
    if(( g_evalues =(dcomplex *)malloc(g_csize*sizeof(dcomplex)) )==NULL) 
      fprintf(stderr,"evalues malloc error");
    if(( g_rwork =(double *)malloc(2*g_csize*sizeof(double)) )==NULL) 
      fprintf(stderr,"rwork malloc error");
    if(( g_evwork =(dcomplex *)malloc(g_lwfact*g_csize*sizeof(dcomplex)) )==NULL)
      fprintf(stderr,"rwork malloc error");
  }
  /********* fields of O(g_vol x g_nt_loc) *********/
  /*** Main fields ***/
  if(( g_field_ = (double*)realloc(g_field_,Ntp1*g_esize*sizeof(double)) )==NULL) 
    fprintf(stderr,"field_ malloc error");
  if(( g_field  = (double**)realloc(g_field,Ntp1*sizeof(double*)) )==NULL) 
    fprintf(stderr,"field malloc error");
  g_field[0] = g_field_;
  for(i=1;i<Ntp1;i++) g_field[i] = g_field[i-1] + g_esize;

  if(( g_field_new_ = (double*)realloc(g_field_new_,Ntp1*g_esize*sizeof(double)) )==NULL) 
    fprintf(stderr,"field_new_ malloc error");
  if(( g_field_new  = (double**)realloc(g_field_new,Ntp1*sizeof(double*)) )==NULL) 
    fprintf(stderr,"field_new malloc error");
  g_field_new[0] = g_field_new_;
  for(i=1;i<Ntp1;i++) g_field_new[i] = g_field_new[i-1] + g_esize;

  if(( g_noise_ = (double*)realloc(g_noise_,Ntp1*g_esize*sizeof(double)) )==NULL) 
    fprintf(stderr,"noise_ malloc error");
  if(( g_noise  = (double**)realloc(g_noise,Ntp1*sizeof(double*)) )==NULL) 
    fprintf(stderr,"noise malloc error");
  g_noise[0] = g_noise_;
  for(i=1;i<Ntp1;i++) g_noise[i] = g_noise[i-1] + g_esize;

  /*** service in steepest_descent (Newton-Raphson)  ***/
  if(( g_diff_ = (double*)realloc(g_diff_,Ntp1*g_esize*sizeof(double)) )==NULL) 
    fprintf(stderr,"diff_ malloc error");
  if(( g_diff  = (double**)realloc(g_diff,Ntp1*sizeof(double*)) )==NULL) 
    fprintf(stderr,"diff malloc error");
  g_diff[0] = g_diff_;
  for(i=1;i<Ntp1;i++) g_diff[i] = g_diff[i-1] + g_esize;

  /*** service in ODE (Nsub>1)  ***/
  if(( g_yy_ = (double*)realloc(g_yy_,2*g_esize*sizeof(double)) )==NULL) 
    fprintf(stderr,"yy_ malloc error");
  if(( g_yy  = (double**)realloc(g_yy,2*sizeof(double*)) )==NULL) 
    fprintf(stderr,"yy malloc error");
  g_yy[0] = g_yy_;
  for(i=1;i<2;i++) g_yy[i] = g_yy[i-1] + g_esize;

  /*** service in CGNE, tests ***/ 
  if(( g_r_ = (double*)realloc(g_r_,(encond)*sizeof(double)) )==NULL) 
    fprintf(stderr,"r_ malloc error"); 
  if(( g_r  = (double**)realloc(g_r,Ntp1*sizeof(double*)) )==NULL) 
    fprintf(stderr,"r malloc error");
  g_r[0] = g_r_;
  for(i=1;i<Ntp1;i++) g_r[i] = g_r[i-1] + g_esize;

  if(( g_p_ = (double*)realloc(g_p_,(edof)*sizeof(double)) )==NULL) 
    fprintf(stderr,"p_ malloc error");
  if(( g_p  = (double**)realloc(g_p,Ntp1*sizeof(double*)) )==NULL) 
    fprintf(stderr,"p malloc error");
  g_p[0] = g_p_;
  for(i=1;i<Ntp1;i++) g_p[i] = g_p[i-1] + g_esize;

  if(( g_ap_ = (double*)realloc(g_ap_,(ncond)*sizeof(double)) )==NULL) 
    fprintf(stderr,"ap_ malloc error");
  if(( g_ap  = (double**)realloc(g_ap,Ntp1*sizeof(double*)) )==NULL) 
    fprintf(stderr,"ap malloc error");
  g_ap[0] = g_ap_;
  for(i=1;i<Ntp1;i++) g_ap[i] = g_ap[i-1] + g_size;

  /* ...also in load_store */
  if(( g_ar_ = (double*)realloc(g_ar_,(dof)*sizeof(double)) )==NULL) 
    fprintf(stderr,"ar_ malloc error");
  if(( g_ar  = (double**)realloc(g_ar,Ntp1*sizeof(double*)) )==NULL) 
    fprintf(stderr,"ar malloc error");
  g_ar[0] = g_ar_;
  for(i=1;i<Ntp1;i++) g_ar[i] = g_ar[i-1] + g_size;

  /*** Service in the cgne for noise (constant term b) ***/
  if(( g_b_ = (double*)realloc(g_b_,Ntp1*g_size*sizeof(double)) )==NULL) 
    fprintf(stderr,"b_ malloc error");
  if(( g_b  = (double**)realloc(g_b,Ntp1*sizeof(double*)) )==NULL) 
    fprintf(stderr,"b malloc error");
  g_b[0] = g_b_;
  for(i=1;i<Ntp1;i++) g_b[i] = g_b[i-1] + g_size;
  for(i=0;i<Ntp1;i++) for(j=0;j<g_size;j++) g_b[i][j]=0;

  /*** Service in the cgne for SD (constant term Cnstr) ***/
  if(( g_cnstr_ = (double*)realloc(g_cnstr_,(ncond)*sizeof(double)) )==NULL) 
    fprintf(stderr,"cnstr_ malloc error");
  if(( g_cnstr  = (double**)realloc(g_cnstr,Ntp1*sizeof(double*)) )==NULL) 
    fprintf(stderr,"cnstr malloc error");
  g_cnstr[0] = g_cnstr_;
  for(i=1;i<Ntp1;i++) g_cnstr[i] = g_cnstr[i-1] + g_size;

  /*** debug fields ***/
  if(g_start==TESTONLY){
    if(( g_debug_ = (double*)realloc(g_debug_,Ntp1*g_esize*sizeof(double)) )==NULL) 
      fprintf(stderr,"debug_ malloc error");
    if(( g_debug  = (double**)realloc(g_debug,Ntp1*sizeof(double*)) )==NULL) 
      fprintf(stderr,"debug malloc error");
    g_debug[0] = g_debug_;
    for(i=1;i<Ntp1;i++) g_debug[i] = g_debug[i-1] + g_esize;
  }

  /********* FERMIONIC fields  *********/
#ifdef WITHFERM
  if(first_alloc){    
    if(( g_phi_mid = (double*)calloc(g_esize,sizeof(double)) )==NULL) 
      fprintf(stderr,"phi_mid malloc error");
  }
  /*** FERMION fields (exact) ***/
  if(first_alloc){
    if(g_ferm_stoch==0 || g_start==TESTONLY){
      if(( g_ipiv =(int *)malloc(g_csize*sizeof(int)) )==NULL) 
	fprintf(stderr,"ipiv malloc error");
#if (USEMAGMA==1)
      magma_zmalloc((magmaDoubleComplex_ptr *)&gpu_work,g_lwork);
      magma_zmalloc((magmaDoubleComplex_ptr *)&gpu_A,g_csize*g_csize);
      magma_zmalloc((magmaDoubleComplex_ptr *)&gpu_M,g_NF*g_csize*g_csize);
      magma_dmalloc((double **)&gpu_ff,g_size);
      magma_dmalloc((double **)&gpu_gg,g_size);
      magma_dmalloc((double **)&gpu_field,g_esize);
#else
      if(( g_work =(dcomplex *)malloc(g_lwork*sizeof(dcomplex)) )==NULL) 
	fprintf(stderr,"work malloc error");
      if(( g_A =(dcomplex *)calloc(g_csize*g_csize,sizeof(dcomplex)) )==NULL) 
	fprintf(stderr,"A malloc error");
      if(( g_M_ =(dcomplex *)calloc(g_NF*g_csize*g_csize,sizeof(dcomplex)) )==NULL) 
	fprintf(stderr,"M_ malloc error");
      if(( g_M =(dcomplex **)calloc(g_NF,sizeof(dcomplex*)) )==NULL) 
	fprintf(stderr,"M malloc error");
      g_M[0] = g_M_;
      for(j=1;j<g_NF;j++) g_M[j] = g_M[j-1] + g_csize*g_csize;
#endif
    }
  }
  /*** FERMION fields (stochastic) ***/
  if(g_ferm_stoch==1 || g_start==TESTONLY){
    if(first_alloc){    
      if(( g_aux = (dcomplex*)calloc(g_cesize,sizeof(dcomplex)) )==NULL) 
	fprintf(stderr,"aux malloc error");
      if(( g_cr = (dcomplex*)calloc(g_cesize,sizeof(dcomplex)) )==NULL) 
	fprintf(stderr,"cr malloc error");
      if(( g_cp = (dcomplex*)calloc(g_cesize,sizeof(dcomplex)) )==NULL) 
	fprintf(stderr,"cp malloc error");
      if(( g_car = (dcomplex*)calloc(g_csize,sizeof(dcomplex)) )==NULL) 
	fprintf(stderr,"car malloc error");
      if(( g_cap = (dcomplex*)calloc(g_csize,sizeof(dcomplex)) )==NULL) 
	fprintf(stderr,"cap malloc error");
      if(( g_xi_ = (dcomplex*)calloc(g_NR*g_cesize,sizeof(dcomplex)) )==NULL) 
	fprintf(stderr,"xi_ malloc error");
      if(( g_xi = (dcomplex**)calloc(g_NR,sizeof(dcomplex*)) )==NULL) 
	fprintf(stderr,"xi malloc error");
      g_xi[0] = g_xi_;
      for(j=1;j<g_NR;j++) g_xi[j] = g_xi[j-1] + g_cesize;
    }
    /* (consider using realloc for pb0 and pb2) */
    if(!first_alloc){
      free(g_pb0___); free(g_pb2___); free(g_pb0__); free(g_pb2__); 
      free(g_pb0_); free(g_pb2_); free(g_pb0); free(g_pb2); 
    }
    if(( g_pb0___ = (dcomplex*)calloc(g_NF*g_NR*(Ntp1)*g_cesize,sizeof(dcomplex)) )==NULL) 
      fprintf(stderr,"pb0___ malloc error");
    if(( g_pb2___ = (dcomplex*)calloc(g_NF*g_NR*(Ntp1)*g_cesize,sizeof(dcomplex)) )==NULL) 
      fprintf(stderr,"pb2___ malloc error");
    if(( g_pb0__  = (dcomplex**)calloc(g_NF*g_NR*(Ntp1),sizeof(dcomplex*)) )==NULL) 
      fprintf(stderr,"pb0__  malloc error");
    if(( g_pb2__  = (dcomplex**)calloc(g_NF*g_NR*(Ntp1),sizeof(dcomplex*)) )==NULL) 
      fprintf(stderr,"pb2__  malloc error");
    if(( g_pb0_   = (dcomplex***)calloc(g_NF*g_NR,sizeof(dcomplex**)) )==NULL) 
      fprintf(stderr,"pb0_   malloc error");
    if(( g_pb2_   = (dcomplex***)calloc(g_NF*g_NR,sizeof(dcomplex**)) )==NULL) 
      fprintf(stderr,"pb2_   malloc error");
    if(( g_pb0    = (dcomplex****)calloc(g_NF,sizeof(dcomplex***)) )==NULL) 
      fprintf(stderr,"pb0    malloc error");
    if(( g_pb2    = (dcomplex****)calloc(g_NF,sizeof(dcomplex***)) )==NULL) 
      fprintf(stderr,"pb2    malloc error");
    g_pb0__[0] = g_pb0___;
    g_pb2__[0] = g_pb2___;
    for(j=1;j<g_NF*g_NR*(Ntp1);j++) g_pb0__[j] = g_pb0__[j-1] + g_cesize;
    for(j=1;j<g_NF*g_NR*(Ntp1);j++) g_pb2__[j] = g_pb2__[j-1] + g_cesize;
    g_pb0_[0] = g_pb0__;
    g_pb2_[0] = g_pb2__;
    for(j=1;j<g_NF*g_NR;j++) g_pb0_[j] = g_pb0_[j-1] + (Ntp1);
    for(j=1;j<g_NF*g_NR;j++) g_pb2_[j] = g_pb2_[j-1] + (Ntp1);
    g_pb0[0] = g_pb0_;
    g_pb2[0] = g_pb2_;
    for(j=1;j<g_NF;j++) g_pb0[j] = g_pb0[j-1] + g_NR;
    for(j=1;j<g_NF;j++) g_pb2[j] = g_pb2[j-1] + g_NR;
  }
#endif // WITHFERM

  /********* fields of O(g_vol) *********/
  if(first_alloc){
    /*** stationary configuration ***/
    if(( g_stat_conf = (double*)realloc(g_stat_conf,g_size*sizeof(double)) )==NULL) 
      fprintf(stderr,"stat_conf malloc error");

    /*** service in ode_rk4, in constraints, tests, matrix(t)_SD/noise, in search_min ***/
    if(( g_kk = (double*)realloc(g_kk,g_esize*sizeof(double)) )==NULL) 
      fprintf(stderr,"kk malloc error");

    /*** service in ode_rk4, constraints, tests, matrix(t)_SD/noise, accept_reject, langevin, tests ***/
    if(( g_ff = (double*)realloc(g_ff,g_size*sizeof(double)) )==NULL) 
      fprintf(stderr,"ff malloc error");

    /*** service in matrix(t)_SD ***/
    if(( g_hh = (double*)realloc(g_hh,g_size*sizeof(double)) )==NULL) 
      fprintf(stderr,"ff malloc error");
    
    /*** save old cofig while SD in main ***/
    if(( g_ll = (double*)realloc(g_ll,g_size*sizeof(double)) )==NULL) 
      fprintf(stderr,"ll malloc error");

    /*** FFT ***/
    if(( g_fftf = (dcomplex*)realloc(g_fftf,g_vol*CPS*sizeof(dcomplex)) )==NULL) 
      fprintf(stderr,"fftf malloc error");
    if(( g_fftb = (dcomplex*)realloc(g_fftb,g_vol*CPS*sizeof(dcomplex)) )==NULL) 
      fprintf(stderr,"fftb malloc error");

    /*** projector ***/
    g_samemom=calloc(g_vol,sizeof(int));

    g_proj__= (double*)calloc(g_vol*DPS*DPMP,sizeof(double));
    g_proj_ = (double**)calloc(g_vol*DPS,sizeof(double*));
    g_proj  = (double***)calloc(g_vol,sizeof(double**));
    g_proj_[0]=g_proj__;
    for(j=1;j<g_vol*DPS;j++) g_proj_[j]=g_proj_[j-1] + DPMP;
    g_proj[0]=g_proj_;
    for(j=1;j<g_vol;j++) g_proj[j]=g_proj[j-1] + DPS;

    if(g_start==TESTONLY){
      g_projdb__= (double*)calloc(g_vol*DPS*DPMP,sizeof(double));
      g_projdb_ = (double**)calloc(g_vol*DPS,sizeof(double*));
      g_projdb  = (double***)calloc(g_vol,sizeof(double**));
      g_projdb_[0]=g_projdb__;
      for(j=1;j<g_vol*DPS;j++) g_projdb_[j]=g_projdb_[j-1] + DPMP;
      g_projdb[0]=g_projdb_;
      for(j=1;j<g_vol;j++) g_projdb[j]=g_projdb[j-1] + DPS;
    }
  }

  MPI_Barrier(g_cart);
  if(g_proc_id==0) fprintf(stdout,"OK!\n");
}


/*
  Note that the needed sizes of the temporary vectors in CGNE are: (r: encond, p: endof, ar: ndof, ap: ncond)
  (e)ndof_noise = ntau*(e)vol
  (e)ncond_noise = (e)ndof_noise
  (e)ndof_SD = (ntau+1)*(e)vol
  (e)ncond_SD = (ntau)*(e)vol + 2

  So, I allocate the largest space needed (SD).
*/
