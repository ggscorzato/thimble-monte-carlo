/*****************************************************************************
 file: mhessian.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _MHESSIAN_IWASAWA_H
#define _MHESSIAN_IWASAWA_H

void mhessian_model(double * etap, double *eta, double **phi, int n_tau, int plus_half, int acc, int Iwa);
void projected_mhessian(double * etap, double *eta, double **phi, int n_tau, int plus_half, int acc, int Iwa);

#endif
