/*****************************************************************************
 file: init_geometry.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include "global.h"
#include "init_model.h"
#include "init_geometry.h"

/*****************************************************************************/
/* Convert lexicographic index into cartesian coordinates. The lexicographic order is defined by cbasis */
inline void index2coord(int * coord, int index, int *cbasis, int *l){
  int mu;
  for(mu=0;mu<DIM;mu++){
    coord[mu] = (index / cbasis[mu]) % l[mu];    
  }
}

/*****************************************************************************/
/* Convert cartesian coordinates into lexicographic index, defined by cbasis */
inline int coord2index(int *coord,int *cbasis){
  int ind=0,mu;
  for(mu=0;mu<DIM;mu++){
    ind += coord[mu]*cbasis[mu];
  }
  return ind;
}

/*****************************************************************************/
/* Initialize all the variables related to the mapping of the lattice in the processes */
void init_geometry(){

  int mu,nu,en,pnt,epnt,j,ver,edge_num,bpnt,isb,gnproc;
  int gpc[DIM],vielbein[DIM][DIM],*elex2olex,*olex2elex,periods[DIM+1],phys_dim[DIM+1];
  int coord[DIM],ecoord[DIM],ncoord[DIM];
  int sign[]={-1,1};
  int bind[MAXEDGES][DIM][2][2];

  if(g_proc_id==0) fprintf(stdout,"Initializing geometry...");

  /* g_nprocl[], g_L[], g_indexord[] are read from input */

  /*************/
  /* 0. Parallelization in the flow direction. The flow direction is fixed to the last one, and cannot be changed.*/

  g_flowdir=DIM;
  g_nprocl[g_flowdir]=g_np_ntau;

  /*************/
  /* 1. Define volume, extended volume (with borders), local lattice sizes and total number of procs: g_vol, g_l[],
     g_evol, g_el[], g_nproc */

  g_vol=1;
  g_evol=1;
  g_tvol=1;
  gnproc=1;
  g_npd=0;
  for(mu=0;mu<DIM;mu++) {  
    g_tvol*=g_L[mu];                /* total space-time volume */
    g_l[mu] = g_L[mu]/g_nprocl[mu]; /* local extensions of the lattice */
    if(g_nprocl[mu]>1) {            /* parallelized directions */
      g_el[mu]=g_l[mu]+2*THICK;     /* local extensions of the extended-lattice (with borders) */
      g_npd++;                      /* number of parallelized directions */
    } else {
      g_el[mu]=g_l[mu];
    }
    g_vol*=g_l[mu];                 /* local volume */
    gnproc*=g_nprocl[mu];           /* total number of processors */
    g_evol*=g_el[mu];               /* local extended volume (with borders) */
  }
  gnproc*=g_nprocl[g_flowdir];

  if(gnproc!=g_nproc){
    if(g_proc_id==0){
      fprintf(stderr,"init_geometry ERROR: mismatch in total number of procs from inputfile and mpirun:"
	      "%d != %d\n",gnproc,g_nproc);
    }
    MPI_Finalize();
    exit(-1);
  }

  /*************/
  /* 2. Define a basis for the coodinate index (used e.g. in coord2index and index2coord) */

  g_cbasis[g_indexord[0]]=1; /* basis for the local lattice index */
  for(mu=1;mu<DIM;mu++){
    g_cbasis[g_indexord[mu]]=g_cbasis[g_indexord[mu-1]]*g_l[g_indexord[mu-1]];
  }
  g_gcbasis[g_indexord[0]]=1; /* basis for the global lattice index */
  for(mu=1;mu<DIM;mu++){
    g_gcbasis[g_indexord[mu]]=g_gcbasis[g_indexord[mu-1]]*g_L[g_indexord[mu-1]];
  }
  g_ecbasis[g_indexord[0]]=1; /* basis for the local extended lattice index */
  for(mu=1;mu<DIM;mu++){
    g_ecbasis[g_indexord[mu]]=g_ecbasis[g_indexord[mu-1]]*g_el[g_indexord[mu-1]];
  }
  for(mu=0;mu<DIM;mu++){  /* canonical DIM dimensional basis */
    for(nu=0;nu<mu;nu++){
      vielbein[mu][nu]=0;
      vielbein[nu][mu]=0;
    }
    vielbein[mu][mu]=1;
  }

  /*************/
  /* 3. Conversion between lexic index in the extended lattice (elex) and the lexic index for all the internal
     points first, and the border points afterwords (olex).  (historical note: the old g_ext_ind coincides with the
     first part of olex2elex) */

  olex2elex=(int*)calloc(g_evol,sizeof(int));
  elex2olex=(int*)calloc(g_evol,sizeof(int));
  for(pnt=0;pnt<g_vol;pnt++){ /* define olex -> elex[olex] in bulk */
    index2coord(coord,pnt,g_cbasis,g_l);
    for(mu=0;mu<DIM;mu++){
      if(g_nprocl[mu]>1) coord[mu]+=THICK;
    }
    olex2elex[pnt] = coord2index(coord,g_ecbasis);
  }
  bpnt=g_vol;
  for(epnt=0;epnt<g_evol;epnt++){ /* define olex -> elex[olex] for the borders */
    index2coord(ecoord,epnt,g_ecbasis,g_el);
    isb=0;
    for(mu=0;mu<DIM;mu++){
      if(g_nprocl[mu]>1){
	if((ecoord[mu]<THICK) || (ecoord[mu]>=g_l[mu]-THICK))
	  isb=1;
      }
    }
    if(isb){
      olex2elex[bpnt]=epnt;
      bpnt++;
    }
  }
  for(epnt=0;epnt<g_evol;epnt++){ /* define the inverse olex[elex] */
    elex2olex[olex2elex[epnt]]=epnt;
    if(g_debug_level>3) fprintf(stdout,"opnt:%d,epnt:%d\n",epnt,olex2elex[epnt]);
  }

  /*************/
  /* 4. Define the vector g_ineigh[mu][ver][epnt] that contains the olex index of the neighbor of epnt (ext latt
     lex index) in direction mu,ver */

  g_ineigh__=(int*)calloc(DIM*2*g_vol,sizeof(int));
  g_ineigh_ =(int**)calloc(DIM*2,sizeof(int*));
  g_ineigh  =(int***)calloc(DIM,sizeof(int**));
  g_ineigh_[0]=g_ineigh__;
  for(j=1;j<DIM*2;j++) g_ineigh_[j]= g_ineigh_[j-1] + g_vol;
  g_ineigh[0]=g_ineigh_;
  for(j=1;j<DIM;j++) g_ineigh[j]= g_ineigh[j-1] + 2;

  for(pnt=0;pnt<g_vol;pnt++){
    epnt=olex2elex[pnt];
    index2coord(ecoord,epnt,g_ecbasis,g_el);
    for(mu=0;mu<DIM;mu++){
      for(ver=0;ver<2;ver++){
	g_ineigh[mu][ver][pnt] = -1;
	for(nu=0;nu<DIM;nu++) {
	  ncoord[nu]=ecoord[nu] + sign[ver] * vielbein[mu][nu];
	  ncoord[nu]=(ncoord[nu]+g_el[nu])%g_el[nu];
	}
	j=coord2index(ncoord,g_ecbasis);
	g_ineigh[mu][ver][pnt] = elex2olex[j];
	if(g_debug_level>3) fprintf(stdout,"epnt:%d,opnt:%d,mu:%d,ver:%d,neigh_e:%d,neigh_o:%d\n",
				    epnt,pnt,mu,ver,g_ineigh[mu][ver][pnt],j);
      }
    }
  }

  /*************/
  /* 5. Define the vector g_neigh_node[mu][ver] that contains the rank (i.e. the lex index) of the neighbor node of
     the present node in direction mu,ver.  We rely on MPI implementation (e.g. in open-mpi direction 0 is
     slowest).  Define also an MPI_Comm g_cart for DIM+1 dimensions. (DIM=g_flowdir) */

  for(mu=0;mu<DIM+1;mu++) periods[mu]=1; /* I put it periodic also on flowdir (it can be useful) */
  for(mu=0;mu<DIM+1;mu++) phys_dim[mu]=1;
  phys_dim[g_flowdir]=0;
  
  MPI_Cart_create(MPI_COMM_WORLD, DIM+1, g_nprocl, periods, 1, &g_cart); /* global communicator. Here we let MPI
									    choose the mpping ranks-coordinates. */
  MPI_Comm_rank(g_cart,&g_proc_id);
  MPI_Cart_coords(g_cart,g_proc_id,DIM+1,g_proc_coords);
  MPI_Cart_sub(g_cart,phys_dim,&g_sub_cart); /* communicator for space time averages and other communications at
						fixed flow time */
  for(mu=0;mu<DIM;mu++){ /* space time directions are defined within the sub-communicator */
    MPI_Cart_shift(g_sub_cart,mu,1,&g_neigh_node[mu][DN],&g_neigh_node[mu][UP]);
  }
  MPI_Cart_shift(g_cart,DIM,1,&g_neigh_node[DIM][DN],&g_neigh_node[DIM][UP]);

  g_sub_print_proc=0;
  nu=0;
  for(mu=0;mu<DIM;mu++){
    nu+=g_proc_coords[mu];
  }
  if(nu==0) g_sub_print_proc=1;
  MPI_Comm_split(g_cart, g_sub_print_proc, g_proc_id, &g_print_comm); /* comm. of printing procs along the flow */
  
  if(g_proc_id==0){
    if((g_sub_print_proc!=1) || (g_proc_coords[DIM] != 0)){
      fprintf(stderr,"init_geometry ERROR: the 0th rank PE should belong to the printing group AND have flow coord 0:"
	      "instead they are: printing=%d; flow_coord= %d\n",g_sub_print_proc,g_proc_coords[DIM]);
      MPI_Abort(g_cart,22);
      exit(-2);
    }
  }

  /*************/
  /* 6. Define the global coordinates g_coord for all the points in the (extended) local lattice (as a function of
     olex). Define also a global lexicografic index g_lid (as a function of olex) */

  g_lid=(int*)calloc(g_evol,sizeof(int));
  for(mu=0;mu<DIM;mu++){
    g_coord[mu]=(int*)calloc(g_evol,sizeof(int));
  }
  for(pnt=0;pnt<g_evol;pnt++){
    epnt=olex2elex[pnt];
    index2coord(coord,epnt,g_ecbasis,g_el);
    for(mu=0;mu<DIM;mu++){
      gpc[mu]=g_proc_coords[mu];
      if(g_nprocl[mu]>1){ 
	if(coord[mu] < THICK){
	  gpc[mu]--;
	  coord[mu]=(coord[mu]-THICK+g_l[mu])%g_l[mu];
	}
	if(coord[mu] >= g_el[mu]-THICK){
	  gpc[mu]++;
	  coord[mu]=(coord[mu]-THICK+g_l[mu])%g_l[mu];
	}
      }
      ncoord[mu] = coord[mu] + g_l[mu]*gpc[mu];
      g_coord[mu][pnt]=ncoord[mu];
    }
    g_lid[pnt] = coord2index(ncoord,g_gcbasis);
  }

  /*************/
  /* 7. Define the array BndInd containing the indices of the fields to be communicated. */  
  /* 7a. Run the whole loop first once to compute the sizes of the borders (needed for allocating BndInd) */
  /* (for fields defined on sites. Relevant borders for link fields still missing) */

  for(mu=0;mu<DIM;mu++){
    if(g_nprocl[mu]>1){
      for(en=0;en<MAXEDGES;en++){
	bind[en][mu][DN][RECV]=0;
	bind[en][mu][DN][SEND]=0;
	bind[en][mu][UP][RECV]=0;
	bind[en][mu][UP][SEND]=0;
      }
      for(epnt=0;epnt<g_evol;epnt++){
	index2coord(ecoord,epnt,g_ecbasis,g_el);
	edge_num=0;
	for(nu=0;nu<DIM;nu++){
	  if(g_nprocl[mu]>1){
	    if(ecoord[nu]<THICK) edge_num++;
	    if(ecoord[nu]>=(g_el[nu]-THICK)) edge_num++;
	  }
	}
	if(ecoord[mu] < THICK) 
	  bind[edge_num-1][mu][DN][RECV]++;

	if((ecoord[mu] >= THICK) && (ecoord[mu] < 2*THICK)) 
	  bind[edge_num-1][mu][DN][SEND]++;

	if((ecoord[mu] >= g_el[mu]-2*THICK) && (ecoord[mu] < g_el[mu]-THICK)) 
	  bind[edge_num-1][mu][UP][SEND]++;

	if(ecoord[mu] >= g_el[mu]-THICK) 
	  bind[edge_num-1][mu][UP][RECV]++;
      }
    }
  }


  /* 7b. Then to allocate BndInd, compute the indices that go into them */

  for(mu=0;mu<DIM;mu++){
    if(g_nprocl[mu]>1){
      for(en=0;en<MAXEDGES;en++){
	g_BndInd[en][mu][DN][RECV]=(int*)calloc(bind[en][mu][DN][RECV],sizeof(int));
	g_BndInd[en][mu][DN][SEND]=(int*)calloc(bind[en][mu][DN][SEND],sizeof(int));
	g_BndInd[en][mu][UP][RECV]=(int*)calloc(bind[en][mu][UP][RECV],sizeof(int));
	g_BndInd[en][mu][UP][SEND]=(int*)calloc(bind[en][mu][UP][SEND],sizeof(int));
	bind[en][mu][DN][RECV]=0;
	bind[en][mu][DN][SEND]=0;
	bind[en][mu][UP][RECV]=0;
	bind[en][mu][UP][SEND]=0;
      }
      for(epnt=0;epnt<g_evol;epnt++){
	index2coord(ecoord,epnt,g_ecbasis,g_el);
	edge_num=0;
	for(nu=0;nu<DIM;nu++){
	  if(g_nprocl[mu]>1){
	    if(ecoord[nu]<THICK) edge_num++;
	    if(ecoord[nu]>=(g_el[nu]-THICK)) edge_num++;
	  }
	}
	if(ecoord[mu] < THICK) 
	  g_BndInd[edge_num-1][mu][DN][RECV][ bind[edge_num-1][mu][DN][RECV]++ ]=elex2olex[epnt];

	if((ecoord[mu] >= THICK) && (ecoord[mu] < 2*THICK)) 
	  g_BndInd[edge_num-1][mu][DN][SEND][ bind[edge_num-1][mu][DN][SEND]++ ]=elex2olex[epnt];

	if((ecoord[mu] >= g_el[mu]-2*THICK) && (ecoord[mu] < g_el[mu]-THICK)) 
	  g_BndInd[edge_num-1][mu][UP][SEND][ bind[edge_num-1][mu][UP][SEND]++ ]=elex2olex[epnt];

	if(ecoord[mu] >= g_el[mu]-THICK) 
	  g_BndInd[edge_num-1][mu][UP][RECV][ bind[edge_num-1][mu][UP][RECV]++ ]=elex2olex[epnt];
      }
    }
  }

  /*************/
  /* 8. Define the MPI_Datatypes for the send/recv buffers */

  if(g_npd>0){ /* skip everything if not parallel */
    MPI_Type_contiguous(DPS, MPI_DOUBLE, &g_type_dps);
    MPI_Type_commit(&g_type_dps);
    for(mu=0;mu<DIM;mu++){
      if(g_nprocl[mu]>1){
	for(en=0;en<MAXEDGES;en++){
	  MPI_Type_create_indexed_block(bind[en][mu][DN][RECV],1,g_BndInd[en][mu][DN][RECV],g_type_dps,
					&g_type_boundary[en][mu][DN][RECV]);
	  MPI_Type_commit(&g_type_boundary[en][mu][DN][RECV]);
	  
	  MPI_Type_create_indexed_block(bind[en][mu][DN][SEND],1,g_BndInd[en][mu][DN][SEND],g_type_dps,
					&g_type_boundary[en][mu][DN][SEND]);
	  MPI_Type_commit(&g_type_boundary[en][mu][DN][SEND]);
	  
	  MPI_Type_create_indexed_block(bind[en][mu][UP][RECV],1,g_BndInd[en][mu][UP][RECV],g_type_dps,
					&g_type_boundary[en][mu][UP][RECV]);
	  MPI_Type_commit(&g_type_boundary[en][mu][UP][RECV]);
	  
	  MPI_Type_create_indexed_block(bind[en][mu][UP][SEND],1,g_BndInd[en][mu][UP][SEND],g_type_dps,
					&g_type_boundary[en][mu][UP][SEND]);
	  MPI_Type_commit(&g_type_boundary[en][mu][UP][SEND]);
	}
      }
    }
  }

  /*************/
  MPI_Barrier(g_cart);
  if(g_proc_id==0){
    fprintf(stdout,"OK!\n");
    fflush(stdout);
  }
}
