/*****************************************************************************
 file: action_p4.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: December 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <complex.h>
#include "global.h"
#include "bicomplex.h"
#include "init_model.h"
#include "action.h"
#if (MODEL == P4)

dcomplex action_p4(double *phi){
  int j,k,mu;
  double dmh,lambdaf,shmu,chmu;
  dcomplex S,ctmp;
  bicomplex field,fieldup;
  
  dmh=DIM + g_mass*g_mass/2;
  lambdaf=g_lambda/4;

  S=0;
  for(j=0;j<g_vol;j++){
    field.br=phi[j*DPS]   + I * phi[j*DPS+1];
    field.bi=phi[j*DPS+2] + I * phi[j*DPS+3];
    ctmp=(field.br*field.br + field.bi*field.bi);
    S+=dmh*ctmp + lambdaf*ctmp*ctmp + g_h*field.br;

    for(mu=0;mu<DIM;mu++){
      k=g_ineigh[mu][UP][j];
      fieldup.br=phi[k*DPS]   + I * phi[k*DPS+1];
      fieldup.bi=phi[k*DPS+2] + I * phi[k*DPS+3];
      if(mu==g_timedir){
	chmu=cosh(g_chempot);
	shmu=sinh(g_chempot);
      }else{
	chmu=1;
	shmu=0;
      }
      S -= chmu*(field.br*fieldup.br + field.bi*fieldup.bi);
      S += I*shmu*(field.bi*fieldup.br - field.br*fieldup.bi);  /* epsilon={{0,-1;1,0}}} */
    }
  }
  MPI_Allreduce(&S,&S,1,MPI_DOUBLE_COMPLEX,MPI_SUM,g_sub_cart);

  return S;
}

#endif
