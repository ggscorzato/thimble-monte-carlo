/*****************************************************************************
 file: xchange_fields.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <complex.h>
#include <mpi.h>
#include "global.h"
#include "xchange_fields.h"

/* Note that this xchange function does not include a loop in the thimble direction. This should be done outside
   this function. */
void xchange_fields(double * const k){

  MPI_Request requests[4*DIM];
  MPI_Status status[4*DIM];
  int ireq,en,mu,ver;
  int reqcount = 4*g_npd;

  if(g_npd>0){ /* skip everything if not parallel */
    for(en=0;en<MAXEDGES;en++){
      ireq=0;
      for(mu=0;mu<DIM;mu++){
	if(g_nprocl[mu]>1){
	  for(ver=0;ver<2;ver++){
	    MPI_Isend(k,1,g_type_boundary[en][mu][ver][SEND],g_neigh_node[mu][ver],(5000+mu+ver*DIM),
		      g_sub_cart,&requests[ireq]);
	    MPI_Irecv(k,1,g_type_boundary[en][mu][1-ver][RECV],g_neigh_node[mu][1-ver],(5000+mu+ver*DIM),
		      g_sub_cart,&requests[ireq+1]);
	  }
	  ireq=ireq+4;
	}
      }
      MPI_Waitall(reqcount, requests, status);
    }
  }
}

