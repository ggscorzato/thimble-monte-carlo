/*****************************************************************************
 file: init_model_hub.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: August 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "global.h"
#include "ferm_cuda.h"
#include "init_model.h"
#if (MODEL == HUB)

void init_model_hub(){

  int deg=CPS;
#if (USEMAGMA == 1)
  int j,ud,mu;
#endif

  if(g_proc_id==0) fprintf(stdout,"Initializing model HUBBARD...\n");

  if(g_nprocl[g_timedir]>1){
    fprintf(stderr,"Can't parallelize the time direction in the Hubbard model (requires second neighbors)");
    exit(-3);
  }

  /*** Sizes ***/
  g_size=g_vol*DPS;
  g_esize=g_evol*DPS;
  g_csize=g_vol*CPS;
  g_cesize=g_evol*CPS;
  g_svol=g_vol/g_l[g_timedir];
  g_T=g_l[g_timedir];
  g_lwork=4*g_csize; // set with ilanev
#if (USEMAGMA == 1)
  g_lwork=g_csize*magma_get_zgetri_nb( g_csize );
#endif

  /*** various coefficients ***/

  g_Umu[0]=g_dbeta*(0.5*g_U+g_mu_plus);
  g_Umu[1]=g_dbeta*(0.5*g_U+g_mu_minus);

  if(g_U>0){ 
    g_udb  = I*sqrt(g_U)*g_dbeta;
  }else{ 
    g_udb = sqrt(-g_U) * g_dbeta;
  }
  g_tdb = g_t * g_dbeta;
  
  if(g_proc_id==0) fprintf(stdout,"model HUBBARD initialized\n");
  
  /*** init the FFT ***/
  if(g_proc_id==0) fprintf(stdout,"Initializing fft...");
  
  gg_init_plan(&g_plan, DIM, g_L, g_nprocl, g_indexord, deg, g_sub_cart);
  
  if(g_proc_id==0) fprintf(stdout,"OK!\n");

  /*** MAGMA stuff ***/
#if (USEMAGMA == 1)
  g_gpugrid=(g_csize+BLOCK_DIM-1)/BLOCK_DIM;
  cudaMalloc((void **)&gpu_res, g_gpugrid*sizeof(dcomplex));
  g_res=(dcomplex*)malloc(g_gpugrid*sizeof(dcomplex));
  
  cudaMemcpyToSymbol(&gpu_csize,&g_csize,sizeof(int),0,cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol(&gpu_NF,&g_NF,sizeof(int),0,cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol(&gpu_timedir,&g_timedir,sizeof(int),0,cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol(gpu_Umu,g_Umu,2*sizeof(double),0,cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol(&gpu_tdb,&g_tdb,sizeof(double),0,cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol(&gpu_udb,&g_udb,sizeof(dcomplex),0,cudaMemcpyHostToDevice);

  cudaMalloc((void **)&gpu_ineightu, g_csize*sizeof(int));
  cudaMalloc((void **)&gpu_ineigh, DIM*2*g_csize*sizeof(int));
  cudaMemcpy(gpu_ineightu,g_ineigh[g_timedir][UP],g_csize,cudaMemcpyHostToDevice);
  for(mu=0;mu<DIM;mu++){
    for(ud=0;ud<2;ud++){
      j=(mu*2+ud)*g_csize;
      cudaMemcpy(gpu_ineigh+j,g_ineigh[mu][ud],g_csize,cudaMemcpyHostToDevice);
    }
  }
  cudaMalloc((void **)&gpu_s, g_csize*sizeof(int));
#endif

  /**********************/
  MPI_Barrier(g_cart);
}

void finalize_model_hub(){
  /* finalize fourier */
  gg_destroy_plan(&g_plan);
}
#endif
