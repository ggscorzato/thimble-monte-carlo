/*****************************************************************************
 file: langevin_step.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "global.h"
#include "force.h"
#include "langevin_step.h"

void langevin_step(double *phi_new, double **phi, double **noise){

  int j,dumm=0,acc=0;
  double s2dt;
  s2dt=sqrt(2*g_dt);

  /* I can't use phi[1] since it is computed with rk4, and here (and in accept_reject) I use Euler. 
     So I recompute the force */

  if(g_proc_coords[g_flowdir] == 0){
    force_model(g_ff,phi[0],NULL,0,dumm,acc,1);
    for(j=0;j<g_size;j++) phi_new[j] = phi[0][j] + g_dt * g_ff[j] + s2dt * noise[0][j];
  }
}
