/*****************************************************************************
 file: ferm_mhessian_exact.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "ferm_matrices.h"
#include "ferm_cuda.h"
#include "ferm_mhessian.h"
#ifdef WITHFERM

/*
  Compute (via lapack inversions) the fermionic part of the -hessian acting on eta, accumulate the result on etap.
  etap += - conj(Hess_F[M,phi] eta )
  M is the conjugate fermion matrix FM
  Mt is the transposed fermion matrix FM
  phi is the field
 */

void ferm_mhessian_exact(double * etap, double * eta, double * phi){

  int fl,j;
  int info,m,n,n2,lda;

  n=g_csize; m=n; lda=n;
  n2=n*n;
    
  /* update fermion matrix g_M */
  ferm_update_full_mat_model(phi);

  for(fl=0;fl<g_NF;fl++){

    /* init the full matrix to be inverted */
    #if (USEMAGMA == 1)
    magma_zcopymatrix(n,n,(magmaDoubleComplex *)gpu_M+fl*n2,n,(magmaDoubleComplex *)gpu_A,n);
    #else
    for(j=0;j<n2;j++) g_A[j] = g_M[fl][j];
    #endif

    /* LU factorize and invert */
    #if (USEMAGMA == 1)
    magma_zgetrf_gpu(n,n,(magmaDoubleComplex *)gpu_A,n,g_ipiv,&info);
    magma_zgetri_gpu(n,(magmaDoubleComplex *)gpu_A,n,g_ipiv,(magmaDoubleComplex *)gpu_work,g_lwork,&info);
    #else
    zgetrf_(&m,&n,g_A,&lda,g_ipiv,&info);
    zgetri_(&n,g_A,&lda,g_ipiv,g_work,&g_lwork,&info);
    #endif

    if(info!=0){
      if(g_proc_id==0) fprintf(stderr,"ferm hessian zgetrf/i info=%d\n",info); 
      fflush(stdout); exit(-58);
    }

    /* etap_k -= Tr[M^-1 d_k M M^-1 d_h M] conj( eta_h ) */
    #if (USEMAGMA == 1)
    ferm_TrdMiMdMiM_exact_model(etap,eta,gpu_A);
    #else
    ferm_TrdMiMdMiM_exact_model(etap,eta,g_A);
    #endif

    /* etap_k += Tr[M^-1 d_k d_h M] conj( eta_h ) */
    #if (USEMAGMA == 1)
    ferm_TrddMiM_exact_model(etap,eta,gpu_A);
    #else
    ferm_TrddMiM_exact_model(etap,eta,g_A);
    #endif

  }
}
#endif
