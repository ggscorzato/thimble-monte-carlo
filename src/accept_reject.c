/*****************************************************************************
 file: accept_reject.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include "global.h"
#include "ranlxd.h"
#include "action.h"
#include "force.h"
#include "accept_reject.h"

int accept_reject(double *phi_new, double *phi){

  int j,dumm=0,nt=0,accept;
  dcomplex act;
  double act_old,act_new,mom_old,mom_new,tmp,h_old,h_new,yy[1],dh,expmdh,dt;

 if(g_proc_coords[g_flowdir] == 0){
   dt=sqrt(2.0*g_dt); /* HMC dt corresponds to sqrt(2 * Lang_dt)*/
  
   if(g_proc_id==0) ranlxd(yy,1);
   MPI_Bcast(&yy[0], 1, MPI_DOUBLE, 0, g_sub_cart);

   act=action_model(phi);
   act_old=creal(act);

   act=action_model(phi_new);
   act_new=creal(act);

   mom_old=0;
   for(j=0;j<g_size;j++){
     tmp=g_noise[0][j];
     mom_old+= 0.5 * tmp*tmp;
   }
   MPI_Allreduce(&mom_old,&mom_old,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);

   force_model(g_ff,phi,NULL,nt,dumm,0,1);
   force_model(g_ff,phi_new,NULL,nt,dumm,1,1);
   
   mom_new=0;
   for(j=0;j<g_size;j++){
     tmp= g_noise[0][j]+0.5*dt*g_ff[j];
     mom_new+=0.5*tmp*tmp;
   }
   MPI_Allreduce(&mom_new,&mom_new,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);

   h_old=act_old+mom_old;
   h_new=act_new+mom_new;
   dh=h_new-h_old;
   expmdh=exp(-dh);
   accept=(expmdh > yy[0]);
 }
 MPI_Bcast(&accept, 1, MPI_INT, 0, g_cart);
   
 if(g_proc_id==0) fprintf(stdout,"Accepted: %d, act_old=%g, act_new=%g,"
			  "mom_old=%g, mom_new=%g, h_old=%g, h_new=%g, exp(-dh)=%g\n",
			  accept,act_old,act_new,mom_old,mom_new,h_old,h_new,expmdh);
 
 return accept;
}
