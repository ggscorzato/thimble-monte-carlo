/*****************************************************************************
 file: source_noise_IMR.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include "global.h"
#include "mhessian.h"
#include "source_noise_IMR.h"

/* 
   This routine computes the b term that is needed for the linear system Ax-b=0 whose solution defines the
   evolution of the noise (x). Although the size of the system is n=g_vol*g_nt_tot, (size(A)= n x n,
   size(x)=size(b)=n), the only non-zero component in b is the last one. For this reason, this routine produces
   only that component. For the same reason, only the last component of the noise contributes (g_noise[g_nt_tot]).

   Typical call: 
     source_noise_IMR_FE(g_b, g_noise, phi,dtau,0,g_nt_loc) 
     source_noise_IMR_FB(g_b, g_noise, phi,dtau,0,g_nt_loc) 
   b_: (output) b term 
   noise_: (input) noise 
   phi: (input) fields
   dtau (input) discretization step in time.
*/

void source_noise_IMR_FE(double **b, double **noise, double **phi, double dtau, int nb, int ne){

  int j,h,acc=0,iwa=0;
  double dtau2;
  dtau2=dtau/2;

  for(h=nb;h<ne;h++) for(j=0;j<g_size;j++) b[h][j]=0.0;
  if(g_proc_coords[g_flowdir]==g_np_ntau-1){
    mhessian_model(b[ne-1],noise[ne],phi,ne-1,PLUSHALF,acc,iwa);
    for(j=0;j<g_size;j++) b[ne-1][j] = noise[ne][j] - dtau2 * b[ne-1][j];
  }
}

void source_noise_IMR_FB(double **b, double **noise, double **phi, double dtau, int nb, int ne){

  int j,h,acc=0,iwa=0;
  double dtau2;
  dtau2=dtau/2;

  for(h=nb;h<ne;h++) for(j=0;j<g_size;j++) b[h][j]=0.0;
  if(g_proc_coords[g_flowdir]==0){
    mhessian_model(b[nb],noise[nb],phi,nb,PLUSHALF,acc,iwa);
    for(j=0;j<g_size;j++) b[nb][j] = -noise[nb][j] - dtau2 * b[nb][j];
  }
}
