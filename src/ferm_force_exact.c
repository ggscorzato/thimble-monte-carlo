/*****************************************************************************
 file: ferm_force_exact.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <complex.h>
#include "global.h"
#include "ferm_matrices.h"
#include "ferm_cuda.h"
#include "ferm_force.h"
#ifdef WITHFERM

/*
  Accumulate in ff the fermion force: ff_j += - conj (d_j S_F ) = Tr[M^-1 d_j M].
  (where M is the conjugate fermion matrix)
*/

void ferm_force_exact(double * ff, double * phi){

  int fl,j;
  int info,n,m,lda,n2;

  n2=g_csize*g_csize;
  n=g_csize; m=n; lda=n;

  /* update the conj fermion matrix g_M */
  ferm_update_full_mat_model(phi);

  for(fl=0;fl<g_NF;fl++){

    /* init the full matrix to be inverted */
    #if (USEMAGMA == 1)
    magma_zcopymatrix(n,n,(magmaDoubleComplex *)gpu_M+fl*n2,n,(magmaDoubleComplex *)gpu_A,n);
    #else
    for(j=0;j<n2;j++) g_A[j] = g_M[fl][j];
    #endif

    /* LU factorize and invert */
    #if (USEMAGMA == 1)
    magma_zgetrf_gpu(n,n,(magmaDoubleComplex *)gpu_A,n,g_ipiv,&info);
    magma_zgetri_gpu(n,(magmaDoubleComplex *)gpu_A,n,g_ipiv,(magmaDoubleComplex *)gpu_work,g_lwork,&info);
    #else
    zgetrf_(&m,&n,g_A,&lda,g_ipiv,&info);
    zgetri_(&n,g_A,&lda,g_ipiv,g_work,&g_lwork,&info);
    #endif

    if(info!=0){
      if(g_proc_id==0) fprintf(stderr,"ferm force zgetrf/i info=%d; lwork=%d\n",info,g_lwork); 
      fflush(stdout); exit(-58);
    }

    /* ff_k = Tr[d_k M * M^-1] */
    #if (USEMAGMA == 1)
    ferm_TrdMiM_exact_model(ff,gpu_A);
    #else
    ferm_TrdMiM_exact_model(ff,g_A);
    #endif
  }
}
#endif
