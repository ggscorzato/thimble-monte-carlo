/*****************************************************************************
 file: ferm_extract_noise.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
 *******************************************************************************/

#ifndef _FERM_EXTRACT_NOISE_H
#define _FERM_EXTRACT_NOISE_H

void ferm_extract_noise();
void ferm_extract_basis();

#endif
