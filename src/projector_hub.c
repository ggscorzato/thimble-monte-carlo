/***********************************************************************
 file: projector_hub.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: August 2013
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <math.h>
#include <mpi.h>
#include <ggfft.h>
#include "global.h"
#include "init_geometry.h"
#include "init_model.h"
#include "mhessian.h"
#include "projector.h"
#if (MODEL == HUB)

/*
  Initialization of the projector at a constant field. See Formulae.pdf and Hessian_analytic_hubbard.pdf
  proj projects on the POSITIVE autospace of d^2 S_R (negative eigenvalues of mhessian).
*/
 
void init_projector_hub(double *** proj){

  int i,j,h,l,m,mu,fl,mnq[DIM],nq[DIM],nk[DIM],nkq[DIM],sm,bi[DPMP];
  double q[DIM],k[DIM],kq[DIM],twopi=2*3.141592653589793,ph,itvol;
  double A,B,mcof,ev_dm[DPMP][DPMP],ev_sm[DPS][DPS],norm,gammak,gammakq;
  dcomplex coeff,alpha,sk,skq,denk,denkq,pd;

  if(g_proc_id==g_nproc-1) fprintf(stdout,"Initializing projector...");

  itvol=1.0/((double)g_tvol);

  /* set to zero all the entries of the basis vectors that will not be touched */
  for(i=0;i<DPMP;i++){
    for(j=0;j<DPMP;j++){
      ev_dm[i][j]=0.0;
    }
  }
  for(i=0;i<DPS;i++){
    for(j=0;j<DPS;j++){
      ev_sm[i][j]=0.0;
    }
  }

  /* main loop over momenta j (-> mom q) */
  for(j=0;j<g_vol;j++){
    sm=1;
    for(mu=0;mu<DIM;mu++){
      nq[mu]=g_coord[mu][j];
      mnq[mu]=(-nq[mu]+g_L[mu])%g_L[mu];
      q[mu]= (double)(twopi*nq[mu])/g_L[mu];
      if(nq[mu]!=mnq[mu]) sm*=0;
    }
    g_samemom[j]=sm;
    coeff=0.0;
    /* loop over summed sigma and momenta i(-> mom k) */
    for(fl=0;fl<g_NF;fl++){
      alpha=1.0 + g_udb * g_phi0 + g_Umu[fl];
      for(i=0;i<g_tvol;i++){ /* i need to loop over the whole lattice */
	sk =0.0;
	skq=0.0;
	index2coord(nk,i,g_gcbasis,g_L);
	for(mu=0;mu<DIM;mu++){
	  ph=0.0;
	  if(mu==g_timedir) ph=0.5;
	  nkq[mu]=(nk[mu]-nq[mu])%g_L[mu];
	  k[mu]  = (double)(twopi*( nk[mu]+ph))/g_L[mu];
	  kq[mu] = (double)(twopi*(nkq[mu]+ph))/g_L[mu];
	  if(mu!=g_timedir){
	    sk+=sin(k[mu]);
	    skq+=sin(kq[mu]);
	  }
	}
	gammak  = -2.0*g_tdb*sk;
	gammakq = -2.0*g_tdb*skq;
	denk  = cexp(-I* k[g_timedir]) - (alpha + I * gammak);
	denkq = cexp(-I*kq[g_timedir]) - (alpha + I * gammakq);
	pd=denk*denkq;
	if(!(cabs(pd)>1e-14)){
	  if(g_proc_id==g_nproc-1) fprintf(stdout,"ERROR!! zero denominator at (j,i)==(%d,%d)\n",j,i);
	  exit(-55);
	}
	coeff += 1.0/(pd);
      } 
    } /* end of loop over summed sigma and momenta i(-> mom k) */

    coeff *= (g_udb * g_udb) * itvol;
    coeff += g_dbeta; /* add the bosonic part */
    mcof = cabs(coeff);

    if(mcof > 1e-14){ 
      coeff /= mcof;
    } else {
      if(g_proc_id==g_nproc-1) fprintf(stdout,"warning!! zero eigenvalue at j=%d\n",j);
    }
    A = creal(coeff);
    B = cimag(coeff);

    if(!sm){
      
      ev_dm[0][0]= B; ev_dm[0][1]= A; ev_dm[0][2]= 0; ev_dm[0][3]= 1; // ev: -sqrt(A^2+B^2)
      ev_dm[1][0]=-A; ev_dm[1][1]= B; ev_dm[1][2]= 1; ev_dm[1][3]= 0; // ev: -sqrt(A^2+B^2)
      ev_dm[2][0]=-B; ev_dm[2][1]=-A; ev_dm[2][2]= 0; ev_dm[2][3]= 1; // ev:  sqrt(A^2+B^2)
      ev_dm[3][0]= A; ev_dm[3][1]=-B; ev_dm[3][2]= 1; ev_dm[3][3]= 0; // ev:  sqrt(A^2+B^2)
      bi[0]=2; bi[1]=3; // positive eigenvectors

      /* normalize the ev_dm */
      for(l=0;l<DPS;l++){
        norm=0.0;
        for(m=0;m<DPMP;m++) norm+=ev_dm[bi[l]][m]*ev_dm[bi[l]][m];
        norm=1.0/sqrt(norm);
        for(m=0;m<DPMP;m++) ev_dm[bi[l]][m] *= norm;
      }

      /* compute the projector */
      for(h=0;h<DPS;h++){  /* here I project only on mom q (not -q) */
        for(m=0;m<DPMP;m++){
          proj[j][h][m]=0;
          for(l=0;l<DPS;l++) proj[j][h][m]+=ev_dm[bi[l]][h]*ev_dm[bi[l]][m];
        }
      }
      
    } else if(sm){

      ev_sm[0][0]= -A+1; ev_sm[0][1]= B; // ev: -sqrt(A^2+B^2)
      ev_sm[1][0]= -A-1; ev_sm[1][1]= B; // ev:  sqrt(A^2+B^2)

      bi[0]=1; // positive eigenvector

      if(fabs(B)<1e-15 && A<0){
	bi[0]=0;
      }

      /* normalize the ev_sm */
      for(l=0;l<1;l++){
        norm=0.0;
        for(m=0;m<DPS;m++) norm+=ev_sm[bi[l]][m]*ev_sm[bi[l]][m];
        norm=1.0/sqrt(norm);
        for(m=0;m<DPS;m++) ev_sm[bi[l]][m] *= norm;
      }

      /* compute the projector */
      for(h=0;h<DPS;h++){
        for(m=0;m<DPS;m++){
          proj[j][h][m]=0;
          for(l=0;l<1;l++) proj[j][h][m]+=ev_sm[bi[l]][h]*ev_sm[bi[l]][m];
        }
      }

    } // sm
  } // j
  if(g_proc_id==g_nproc-1) fprintf(stdout,"OK!\n");
}
#endif
