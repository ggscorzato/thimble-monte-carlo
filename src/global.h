/***********************************************************************
 file: global.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 ***********************************************************************/
#ifndef _GLOBAL_H
#define _GLOBAL_H

/****************** COMPILATION OPTIONS ********************************/
#define MODEL HUB    /* options: P4 HUB  */
#define DIM 2        /* DIM does not include the dimension along the thimble (tau) */
#define MAXEDGES 2   /* Maximal number of directions outside the bulk that are included in the borders */
#define THICK 1      /* Thickness of the borders. Currently, only THICK=1 is fully supported.  */
#define EXTRACOND 1  /* At the moment up to 2 extra conditions are coded */
#define USEMAGMA  1  /* Use Magma library: 1=yes, 0=no */
#define BLOCK_DIM 16 /* active threads per block in GPU (conditions ?!?) */

/****************** INCLUDES *******************************************/
#include <stdlib.h>
#include <complex.h>
#include <mpi.h>
#include <ggfft.h>
#define dcomplex _Complex double
#if (USEMAGMA==1)  /* global variables only needed with magma in some routines are in ferm_cuda.h */
 #include <cuda_runtime_api.h>
 #include <cublas.h>
 #include <magma.h>
#endif

/***********************************************************************/
#if defined MAIN_PROGRAM
 #define EXTERN
#else
 #define EXTERN extern
#endif

/****************** CONVENTIONAL VALUE FOR LABELS **********************/
#define SEND 1
#define RECV 0
#define UP 1
#define DN 0
#define FW 1
#define BW -1
#define SD 1
#define SA -1
#define RANDOM 0
#define READIN 1
#define TESTONLY 3
#define MEASUREONLY 4
#define LOAD 0
#define STORE 1
#define PLUSZERO 0
#define PLUSHALF 1
#define MINUSHALF -1
#define FORSD 1
#define FORNOISESA 2
#define FORNOISESD 3
#define P4 1
#define HUB 2

/****************** REDEFINE MODEL DEPENDENT FUNCTIONS NAMES ***********/
#if (MODEL==P4)
 #include "redefine_p4.h"
 #undef WITHFERM
#endif
#if (MODEL==HUB)
 #include "redefine_hub.h"
 #define WITHFERM
#endif

/****************** MACROS *********************************************/
#define  MAX(A, B)  ((A) > (B) ? (A) : (B))
#define  MIN(A, B)  ((A) < (B) ? (A) : (B))

/****************** LAPACK ROUTINES ************************************/
/* LU factorization */
extern int zgetrf_(int *m, int *n, dcomplex *a, int *lda, int *ipiv, int *info);

/* inverse */
extern void zgetri_(int *n, dcomplex *A, int *lda, int *ipiv, dcomplex *work, int *lwork, int *info);

/* real eigenvalues & eigenvectors */
extern void dsyev_(char* JOBZ, char* UPLO, int* NN,  double *AA, int* LDA, double* WW,
		   double* WORK, int* LWORK, int *INFO, int len_jobz, int len_uplo);

/* complex eigenvalues (for res phase) */
extern void zgeev_(char* jobvl, char* jobvr, 
		   int* N, dcomplex *A, int* lda, dcomplex *W, dcomplex *vl,
		   int* ldvl, dcomplex *vr, int* ldvr, 
		   dcomplex *work, int* lwork, double *work2, int* info);

/****************** GLOBAL VARIABLES ***********************************/
/* Geometry */
EXTERN int g_indexord[DIM], g_L[DIM+1], g_l[DIM+1], g_el[DIM],*g_coord[DIM], ***g_ineigh, **g_ineigh_, *g_ineigh__;
EXTERN int g_vol, g_svol, g_evol, g_tvol, g_sub_print_proc;
EXTERN int g_proc_id, g_nproc, g_nprocl[DIM+1], g_proc_coords[DIM+1], g_neigh_node[DIM+1][2], g_npd, *g_lid;
EXTERN int * g_BndInd[MAXEDGES][DIM][2][2], g_cbasis[DIM], g_ecbasis[DIM], g_ncbasis[DIM], g_gcbasis[DIM];
EXTERN MPI_Datatype g_type_dps;
EXTERN MPI_Datatype g_type_boundary[MAXEDGES][DIM][2][2];
EXTERN MPI_Comm g_cart, g_sub_cart, g_print_comm;

/* Starting parameters */
EXTERN int g_seed, g_start, g_start_config_num,g_with_residual_phase,g_with_projected_mhessian;
EXTERN double g_init_rescale;

/* Thimble parameters */
EXTERN int g_timedir, g_flowdir, g_size, g_esize, g_csize, g_cesize, g_T, g_NF, g_NR, g_pc_flow_1st,g_pc_flow_last;
EXTERN int g_np_ntau, g_nt_loc, g_nt_1st, g_nt_loc_first, g_nt_loc_last, g_nt_tot, g_nsub, *g_ntloc_list,*g_ntdisp_list;
EXTERN int g_sd_cg_itermax, g_sd_nr_itermax, g_noise_cg_itermax, g_noise_cgf_itermax;
EXTERN double g_dtau, g_noise_cg_tol, g_noise_cgf_tol, g_sd_cg_tol, g_sd_nr_tol, g_weight_act_const, g_si_tol;

/* Langevin parameters */
EXTERN double g_dt;
EXTERN int g_niter, g_nskip, g_ntherm;

/* Action and model specific parameters */
EXTERN double g_chempot;
EXTERN double g_lambda, g_mass, g_h; /* phi4 */
EXTERN double g_t, g_U, g_dbeta, g_mu_plus, g_mu_minus, g_Umu[2], g_tdb;  /* hubbard */
EXTERN dcomplex  g_udb, g_phi0, g_S0;

/* Projector */
EXTERN int  *g_samemom;
EXTERN double ***g_proj, **g_proj_, *g_proj__;

/* main fields */
EXTERN double **g_field,*g_field_,**g_field_new,*g_field_new_,**g_noise,*g_noise_,*g_stat_conf;
EXTERN double ***g_thimb_tan_basis,**g_thimb_tan_basis_,*g_thimb_tan_basis__;

/* exact phase */
EXTERN dcomplex *g_B, *g_evalues,*g_evwork;
EXTERN double *g_rwork;
EXTERN int g_lwfact;

/* fermions */
EXTERN int g_ferm_stoch;

/* fermions (exact) */
EXTERN int g_lwork, *g_ipiv;
#if (USEMAGMA==0)
EXTERN dcomplex *g_work, *g_A, *g_M_, **g_M;
#endif

/* fermions (stochastic) */
EXTERN double *g_phi_mid;
EXTERN dcomplex **g_xi, *g_xi_, *g_aux, *g_cr, *g_cp, *g_cap, *g_car;
EXTERN dcomplex ****g_pb0, ****g_pb2,***g_pb0_, ***g_pb2_,**g_pb0__, **g_pb2__,*g_pb0___, *g_pb2___;

/* service fields (ode, cgne,...) */
EXTERN double *g_ff, *g_kk, *g_hh, *g_ll;
EXTERN double **g_yy, *g_yy_, **g_r, *g_r_, **g_p, *g_p_, **g_ap, *g_ap_, **g_ar, *g_ar_, **g_b, *g_b_;
EXTERN double **g_cnstr, *g_cnstr_,**g_diff,*g_diff_;

/* fft */
EXTERN gg_plan g_plan;
EXTERN dcomplex *g_fftf, *g_fftb; 

/* debug */
EXTERN int g_debug_level;
EXTERN double **g_debug,*g_debug_;
EXTERN double ***g_projdb, **g_projdb_, *g_projdb__;

#endif

