/*****************************************************************************
 file: steepest_descent.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include "global.h"
#include "ode_rk4.h"
#include "ode_euler.h"
#include "force.h"
#include "projector.h"
#include "constraints.h"
#include "cgne.h"
#include "matrices_SD_IMR.h"
#include "observables.h"
#include "steepest_descent.h"

/* Compute phi[0-g_nt_tot] from phi[0], using a Newton-Raphson iteration of the linear system defined
   by the Steepest Descent Eq. discretised with the Implicit Midpont Rule (see notes) */

void steepest_descent(double ** phi, double *phi0old, double act, double n2n){

  int it,j,h,sz[2],iter=0,check_after_proj=1,Nt_con,Nt_deg;
  double res,diff,deltact;
  
  sz[0]=g_size;
  sz[1]=EXTRACOND;

  Nt_con=g_nt_loc;
  Nt_deg=g_nt_loc;
  if(g_proc_coords[g_flowdir]==0) Nt_con=g_nt_loc+1;
  if(g_proc_coords[g_flowdir]==g_np_ntau-1) Nt_deg=g_nt_loc+1;

  /* Compute the first estimate of phi[0...g_nt_tot] from phi[0] with the rk4 explicit integrator */
  ode_rk4(phi,&force_model,0,g_nt_loc,g_nsub,g_dtau,1,NULL,FW);

  /* Project phi[g_nt_tot] here for the first time, and check that it is not too bad */

  if(g_proc_coords[g_flowdir]==g_np_ntau-1){
    if(check_after_proj){
      projector(g_kk,phi[g_nt_loc],g_proj,1);
      diff=0.0;
      for(j=0;j<g_size;j++){
	diff+=fabs(g_kk[j]-phi[g_nt_loc][j]);
	phi[g_nt_loc][j]=g_kk[j];
      }
      MPI_Allreduce(&diff,&diff,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
      if(g_debug_level>0) if(g_proc_id==0) fprintf(stdout,"Projection of phi[Nt] differs by:%g\n",diff);
    } else {
      projector(phi[g_nt_loc],phi[g_nt_loc],g_proj,1);
    }
  }

  /*  Newton-Raphson iteration to compute phi[0...g_nt_tot] properly with IMR integrator and CGNE */

  if( g_nt_tot > 0 ){
    res=1.0;
    for(it=0;it<g_sd_nr_itermax;it++){
      
      constraints(g_cnstr,phi,phi0old,n2n,act,g_dtau,g_nt_loc);
      
      res=0.0;
      for(h=0;h<Nt_con;h++) for(j=0;j<sz[h/g_nt_loc];j++) res+=g_cnstr[h][j]*g_cnstr[h][j];
      MPI_Allreduce(&res,&res,1,MPI_DOUBLE,MPI_SUM,g_cart);
      if(g_proc_coords[g_flowdir]==0){
	deltact=g_cnstr[g_nt_loc][0]/g_weight_act_const;
	if(g_debug_level>0) if(g_proc_id==0) fprintf(stdout,"SD-CG-iter: %d, SD-NR-iter: %d,"
						     "Constraints: %g, delta Action: %g\n",
						     iter,it,res,deltact);
      }
      
      if(res<g_sd_nr_tol){
	if(g_debug_level>0) if(g_proc_id==0) fprintf(stdout,"CG-NR converged!\n");
	break;
      }
      
      for(h=0;h<Nt_deg;h++) for(j=0;j<sz[0];j++) g_diff[h][j]=0.0;
      iter=cgne(g_diff,&matrix_SD_IMR,&matrixt_SD_IMR,g_cnstr,g_sd_cg_itermax,g_sd_cg_tol,phi,
		0,Nt_deg,0,g_nt_loc,EXTRACOND);
      for(h=0;h<g_nt_loc+1;h++) for(j=0;j<sz[0];j++) phi[h][j]+=g_diff[h][j]; /* this copies the borders along the flow */
      if(g_proc_coords[g_flowdir]==g_np_ntau-1) projector(phi[g_nt_loc],phi[g_nt_loc],g_proj,1);
    }
    if((it==g_sd_nr_itermax) && (g_proc_id==0)) fprintf(stdout,"Newton-Raphson WARNING: max iterations"
							" %d exceeded !!\n",g_sd_nr_itermax);
  }
}
