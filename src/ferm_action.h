/*****************************************************************************
 file: ferm_action.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
 *******************************************************************************/

#ifndef _FERM_ACTION_H
#define _FERM_ACTION_H

#include "matrix_mult_typedef.h"
dcomplex ferm_action_stoch(matrix_mult_ferm M, double * phi);
dcomplex ferm_action_exact(double * phi);

#endif
