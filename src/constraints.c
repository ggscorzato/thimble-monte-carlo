/*****************************************************************************
 file: constraints.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "global.h"
#include "force.h"
#include "projector.h"
#include "action.h"
#include "constraints.h"

/* 
   Cnstr = -F in the notes, Cnstr = b term in CGNE 
   It is assumed that phi[g_nt_tot] is projected.
*/
void constraints(double **Cnstr, double **phi, double *phi0old, double norm2_noise, double act_old, double dtau,
		 int nt){

  int h,j,dumm=0;
  double act,dif,dif2;

  /* Force: g_size*nt conditions */
  for(h=0;h<nt;h++){
    for(j=0;j<g_size;j++) g_kk[j] = 0.5*(phi[h+1][j]+phi[h][j]);
    force_model(g_ff,g_kk,NULL,h,dumm,0,1);
    for(j=0;j<g_size;j++){
      Cnstr[h][j]=-(phi[h+1][j] - phi[h][j] - dtau*g_ff[j]);
    }
  }

  if(g_proc_coords[g_flowdir] == g_pc_flow_1st){
    if(EXTRACOND>0){ /* Extra cond (**) */
      act = creal(action_model(phi[0]));
      Cnstr[nt][0] = -g_weight_act_const*(act - act_old);
    }

    if(EXTRACOND>1){ /* Extra cond (*) */
      dif2 = 0;
      for(j=0;j<g_size;j++){
	dif=phi[0][j]-phi0old[j];
	dif2+=dif*dif;
      }
      MPI_Allreduce(&dif2,&dif2,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
      Cnstr[nt][1] = -(dif2-norm2_noise);
    }
  }
}
