/*****************************************************************************
 file: ferm_TrddMiM_exact_hub.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
 *******************************************************************************/

#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "init_model.h"
#include "ferm_matrices.h"
#if (MODEL == HUB)

/* 
   Compute etap_k += - conj(H_{F,d2} eta_h) = + conj( Tr[d_k d_h M * M^-1] * eta_h) 
   (assume that mat = conj(M^-1) )
*/

#if (USEMAGMA == 0)
void ferm_TrddMiM_exact_hub(double *etap, double *eta, dcomplex *mat){

  /* does nothing, this term is 0 for the Hubbard model */

}
#endif  // USEMAGMA
#endif
