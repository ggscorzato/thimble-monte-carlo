/*****************************************************************************
 file: ferm_cuda_hub.cu
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2014
 *******************************************************************************/
#define CUDA_FILE

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cuComplex.h>
#include "global.h"
#include "ferm_matrices.h"
#include "init_model.h"
#include "ferm_cuda.h"

#if (MODEL == HUB)

#if (USEMAGMA == 1)

extern "C" void ferm_init_full_mat_hub(){
  cuda_ferm_init_full_mat_hub<<<1,1>>>(gpu_M,gpu_s,gpu_ineightu,gpu_ineigh);
}
__global__ void cuda_ferm_init_full_mat_hub(cuDoubleComplex *_M, int *_s, int *_ineightu, int *_ineigh){

  int fl,k,kr,kr1,n,n2,rho;
  cuDoubleComplex cone, ctmp;
  cone = make_cuDoubleComplex(1.0,0.0);

  /*** init Conjugate Fermion Matrix ***/
  /* MAGMA version. Don't bother to parallelize this initialization.  */
  n=gpu_csize;
  n2=n*n;
  for(fl=0;fl<(gpu_NF);fl++){
    for(k=0;k<n;k++){
      _M[fl*n2 + k*n + k] = cone;
      _s[k]=1;
      kr=_ineightu[k];
      if(kr<k) _s[k]=-1;  /* this happens when k is in the last timeslice */
      _M[fl*n2 + k*n + kr] = make_cuDoubleComplex(-_s[k]*(1.0 + gpu_Umu[fl]), 0.0);
      for(rho=0;rho<DIM;rho++){
	if(rho!=(gpu_timedir)){
	  kr1=_ineigh[(rho*2 + UP)*n + kr];
	  ctmp = make_cuDoubleComplex(_s[k]*(gpu_tdb), 0.0);
	  _M[fl*n2 + k*n + kr1]=cuCadd(_M[fl*n2 + k*n + kr1], ctmp);
	  kr1=_ineigh[(rho*2 + DN)*n + kr];
	  ctmp = make_cuDoubleComplex(-_s[k]*(gpu_tdb), 0.0);
	  _M[fl*n2 + k*n + kr1]=cuCadd(_M[fl*n2 + k*n + kr1], ctmp);
	}
      }
    }
  }
}

extern "C" void ferm_update_full_mat_hub(double *phi){
  cudaMemcpy(gpu_field,phi,g_size,cudaMemcpyHostToDevice);
  cuda_ferm_update_full_mat_hub<<<g_gpugrid,BLOCK_DIM>>>(gpu_field,gpu_M,gpu_s,gpu_ineightu);
}

__global__ void cuda_ferm_update_full_mat_hub(double *_field, cuDoubleComplex *_M, int *_s, int *_ineightu){

  int fl,k,kr,n,n2;
  cuDoubleComplex ctmp, ctmp1, ctmp2;

  n=gpu_csize;
  n2=n*n;
  for(fl=0;fl<(gpu_NF);fl++){
    k=threadIdx.x + blockIdx.x * blockDim.x;
    if(k<n){
      kr=_ineightu[k];
      // _M[fl][k*n + kr] = -_s[k]*(1.0 + conj(g_udb*(_field[2*k]+I*_field[2*k+1])) + g_Umu[fl]);
      ctmp = make_cuDoubleComplex(_field[2*k], _field[2*k+1]);
      ctmp = cuCmul(gpu_udb,ctmp);
      ctmp = cuConj(ctmp);
      ctmp1 = make_cuDoubleComplex(1.0 + gpu_Umu[fl], 0.0);
      ctmp = cuCadd(ctmp,ctmp1);
      ctmp2 = make_cuDoubleComplex(-_s[k], 0.0);
      _M[fl*n2 + k*n + kr] = cuCmul(ctmp2,ctmp);
    }
  }
}

    
extern "C" void ferm_TrLog_exact_hub(dcomplex *pfact,cuDoubleComplex *mat){
  cuda_ferm_TrLog_exact_hub<<<g_gpugrid,BLOCK_DIM>>>(gpu_res,mat);
  cudaMemcpy(pfact,gpu_res,1,cudaMemcpyDeviceToHost);
}

__global__ void cuda_ferm_TrLog_exact_hub(cuDoubleComplex *res, cuDoubleComplex *mat){

  extern __shared__ cuDoubleComplex sdata[];
  cuDoubleComplex ctmp;
  double mtmp,atmp;
  int n=gpu_csize;
  unsigned int tid=threadIdx.x;
  unsigned int j=threadIdx.x + blockIdx.x * blockDim.x;

  ctmp = cuConj(mat[j*n+j]);
  mtmp = log(cuCabs(ctmp));
  atmp = atan2(cuCreal(ctmp),cuCimag(ctmp));
  sdata[tid] = make_cuDoubleComplex(-mtmp,-atmp);  //  sdata[tid] = - clog(cuConj(mat[j*n+j]));

  __syncthreads(); 

  for (unsigned int s=blockDim.x/2; s>0; s>>=1) { 
    if (tid < s) {
      sdata[tid] = cuCadd(sdata[tid], sdata[tid + s]);
    }
    __syncthreads(); 
  }

  if (tid == 0) res[blockIdx.x] = sdata[0];

}


extern "C" void ferm_TrdMiM_exact_hub(double *f, cuDoubleComplex *mat){
  cudaMemcpy(gpu_ff,f,g_csize,cudaMemcpyHostToDevice);
  cuda_ferm_TrdMiM_exact_hub<<<g_gpugrid,BLOCK_DIM>>>(gpu_ff,mat,gpu_s,gpu_ineightu);
  cudaMemcpy(f,gpu_ff,g_csize,cudaMemcpyDeviceToHost);
}

__global__ void cuda_ferm_TrdMiM_exact_hub(double *_ff, cuDoubleComplex *mat, int *_s, int *_ineightu){

  int n=gpu_csize;
  cuDoubleComplex ctmp, ctmp1;
  unsigned int jc,j=threadIdx.x + blockIdx.x * blockDim.x;

  if(j<n){
    jc=_ineightu[j];
    ctmp1 = make_cuDoubleComplex(-_s[j],0.0);
    ctmp = cuCmul(ctmp1, gpu_udb);
    ctmp = cuCmul(cuConj(ctmp), mat[jc*n + j]);
    _ff[2*j]  += cuCreal(ctmp);
    _ff[2*j+1]+= cuCimag(ctmp);
  }
}

extern "C" void ferm_TrdMiMdMiM_exact_hub(double *etap,double *eta, cuDoubleComplex *mat){
  dim3 grid(g_gpugrid,g_gpugrid),block(BLOCK_DIM,BLOCK_DIM);
  cudaMemcpy(gpu_ff,eta ,g_size,cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_gg,etap,g_size,cudaMemcpyHostToDevice);
  cuda_ferm_TrdMiMdMiM_exact_hub<<<grid,block>>>(gpu_gg,gpu_ff,mat,gpu_s,gpu_ineightu);
  cudaMemcpy(etap,gpu_gg,g_size,cudaMemcpyDeviceToHost);
}

__global__ void cuda_ferm_TrdMiMdMiM_exact_hub(double *etap, double *eta, cuDoubleComplex *mat, int *_s, int *_ineightu){

  int li,mj,lip,mjp,n;
  cuDoubleComplex ctmp, ctmp1, ceta, cetap, udb2;
  udb2=cuCmul(gpu_udb, gpu_udb);
  n=gpu_csize;
  li=threadIdx.x + blockIdx.x * blockDim.x;
  mj=threadIdx.y + blockIdx.y * blockDim.y;

  if(mj<n && li<n){
    mjp=_ineightu[mj];
    lip=_ineightu[li];
    ctmp1 = make_cuDoubleComplex(_s[mj], _s[li]);
    ctmp = cuCmul(udb2, cuCmul( mat[li+n*mjp], cuCmul(mat[mj+n*lip], ctmp1)));
    ceta = make_cuDoubleComplex(eta[2*li], - eta[2*li+1]);
    cetap = cuCmul(ctmp, ceta);
    etap[mj*2  ] -= cuCreal(cetap);
    etap[mj*2+1] -= cuCimag(cetap);
  }
}

extern "C" void ferm_TrddMiM_exact_hub(double *etap, double *eta, cuDoubleComplex *mat){

  /* does nothing, this term is 0 for the Hubbard model */

}

#endif // USEMAGMA
#endif
