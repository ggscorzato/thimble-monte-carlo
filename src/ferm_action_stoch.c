/*****************************************************************************
 file: ferm_action_stoch.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
*******************************************************************************/

#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "ferm_matrices.h"
#include "ferm_action.h"
#ifdef WITHFERM
/*
  Compute (stochastically) the fermionic part of the action.  This is realized as:
  -Tr [Log M] ~ -sum_r xi^(r) [Log M] xi^(r) ~ sum_r xi^(r) [sum_k (-1)^k (M-1)^k/k ] xi^(r)
*/

dcomplex ferm_action_stoch(matrix_mult_ferm A, double * phi){

  int r,fl,j,k,kmaxlog=60,sign,with_id=0;
  double iNR=1./((double)g_NR),coef;
  dcomplex fact=0.0;

  for(fl=0;fl<g_NF;fl++){
    for(r=0;r<g_NR;r++){
      
      /* First step in Taylor iteration (use temporary vectors: g_cr, g_car, g_cap) */
      A(g_cr,g_xi[r],fl,with_id,phi);      
      for(j=0;j<g_csize;j++) g_cap[j] = g_cr[j];

      /* Taylor iteration for the logarithm.   I use: log(1+A)b ~ (((((...)A-1/4)A+1/3)A-1/2)A+1)Ax */
      sign=1;
      for(k=1;k<kmaxlog;k++){
	A(g_car,g_cr,fl,with_id,phi);
	for(j=0;j<g_csize;j++) g_cr[j] = g_car[j];
	sign*=-1;
	coef=sign*1./((double)(k+1));
	for(j=0;j<g_csize;j++) g_cap[j] += coef*g_cr[j];
      }
      /* accumulate the result in the action (remember that M is the conjugate fermion matrix) */
      for(j=0;j<g_csize;j++) fact -= iNR*conj(g_xi[r][j]*g_cap[j]);
    }
  }
  return fact;
}
#endif
