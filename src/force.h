/*****************************************************************************
 file: force.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _FORCE_H
#define _FORCE_H

void force_model(double *ff, double *phi, double **nullpt, int nt, int dumm_ph, int acc, int sign);

#endif
