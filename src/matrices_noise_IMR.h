/*****************************************************************************
 file: matrices_noise_IMR.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _MATRICES_NOISE_IMR_H
#define _MATRICES_NOISE_IMR_H

void matrix_noise_IMR_FE( double **out, double **in, double dtau, double **phi, int nts, int nte);
void matrix_noise_IMR_FB( double **out, double **in, double dtau, double **phi, int nts, int nte);
void matrixt_noise_IMR_FE(double **out, double **in, double dtau, double **phi, int nts, int nte);
void matrixt_noise_IMR_FB(double **out, double **in, double dtau, double **phi, int nts, int nte);

#endif
