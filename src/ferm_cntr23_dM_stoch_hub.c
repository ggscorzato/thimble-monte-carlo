/*****************************************************************************
 file: ferm_cntr23_dM_stoch_hub.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
 *******************************************************************************/

#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "init_model.h"
#include "ferm_matrices.h"
#if (MODEL == HUB)

/* 
   Compute res_k += alpha * conj( x_i * d_k FM_ij ) * y_j 
   (accumulating on res)
   (y is real in ferm_force_stoch and already conjugate in ferm_mhessian_stoch)
*/

void ferm_cntr23_dM_stoch_hub(double *res, dcomplex *x, dcomplex *y, double alpha){

  int j,jc,s;
  dcomplex ctmp;

  for(j=0;j<g_csize;j++){
    jc=g_ineigh[g_timedir][UP][j];
    s=1;
    if(jc<j) s=-1;
    ctmp = -s * alpha * conj(g_udb * x[jc]) * y[j]; 
    res[2*j]  += creal(ctmp);
    res[2*j+1]+= cimag(ctmp);
  }
}
#endif
