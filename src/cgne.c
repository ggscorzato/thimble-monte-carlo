/*****************************************************************************
 file: cgne.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include "global.h"
#include "matrix_mult_typedef.h"
#include "xchange_fields.h"
#include "cgne.h"

/* 
  Solve with conjugate gradient normal equation - with error minimization (CGNE) the system Ax=b.  i.e. solve AA^t
  y = b and then set x=A^t y.  The code assumes to find the initial guess for the solution in x, where the result
  is also written.  Note that x,g_p,g_ar have length ndof, while b,g_r,g_ap have length ncon.  See Saad Algorithm
  8.5 (Craig's Method) in which y never appear.  Note that CGNE is well suited to find the solution x with minimal
  norm in under-determined systems.  Underdetermined systems have ncon < ndof, if A is full rank. But, we may also
  have under-determined systems with ncon > ndof, if A is not full rank.
  typical use:
  NOISE SA (FE): ns_p=0; ne_p=ntau; ns_r=0; ne_r=ntau; extra_cond=0;
  NOISE SD (FB): ns_p=1; ne_p=ntau+1; ns_r=0; ne_r=ntau; extra_cond=0;
  SD:  ns_p=0; ne_p=ntau+1; ns_r=0; ne_r=ntau; extra_cond=EXTRACOND;
*/

int cgne(double ** x, matrix_mult_cgne A, matrix_mult_cgne At, double ** b, int itermax, double tol, 
	 double **phi, int ns_p, int ne_p, int ns_r, int ne_r, int extra_cond){
  double alpha,beta,r2,p2,r2old,rAAtr;
  int it,i,n,sz[2],ext_ne_r;
  
  sz[0]=g_size;
  sz[1]=extra_cond;

  ext_ne_r=ne_r;
  if((extra_cond > 0) && (g_proc_coords[g_flowdir] == g_pc_flow_1st)) ext_ne_r++;

  /* 1. */
  A(g_r,x,g_dtau,phi,ns_r,ne_r);

  for(n=ns_r;n<ext_ne_r;n++) for(i=0;i<sz[n/ne_r];i++) g_r[n][i]=b[n][i]-g_r[n][i];

  r2=0.0;
  for(n=ns_r;n<ext_ne_r;n++) for(i=0;i<sz[n/ne_r];i++) r2+=g_r[n][i]*g_r[n][i];
  MPI_Allreduce(&r2,&r2,1,MPI_DOUBLE,MPI_SUM,g_cart);
  if(g_debug_level>1) if(g_proc_id==0) fprintf(stdout,"CG-res^2:%g\n",r2);
  if(r2 < tol) return 0;

  At(g_p,g_r,g_dtau,phi,ns_r,ne_r);

  /* 2. */
  for(it=0;it<itermax;it++){
    p2=0.0;
    for(n=ns_p;n<ne_p;n++) for(i=0;i<sz[n/ne_p];i++) p2+=g_p[n][i]*g_p[n][i];
    MPI_Allreduce(&p2,&p2,1,MPI_DOUBLE,MPI_SUM,g_cart);

    /* 3. */
    alpha=r2/p2;

    /* 4. */
    for(n=ns_p;n<ne_p;n++) for(i=0;i<sz[n/ne_p];i++) x[n][i] += alpha*g_p[n][i];

    /* 5. */
    A(g_ap,g_p,g_dtau,phi,ns_r,ne_r);
    for(n=ns_r;n<ext_ne_r;n++) for(i=0;i<sz[n/ne_r];i++) g_r[n][i] -= alpha*g_ap[n][i];

    /* 6. */
    r2old=r2;
    r2=0.0;
    for(n=ns_r;n<ext_ne_r;n++) for(i=0;i<sz[n/ne_r];i++) r2+=g_r[n][i]*g_r[n][i];
    MPI_Allreduce(&r2,&r2,1,MPI_DOUBLE,MPI_SUM,g_cart);
    beta=r2/r2old;

    if(r2 < tol) break;
    
    /* 7. */
    At(g_ar,g_r,g_dtau,phi,ns_r,ne_r);
    for(n=ns_p;n<ne_p;n++) for(i=0;i<sz[n/ne_p];i++) g_p[n][i] = g_ar[n][i] + beta*g_p[n][i];

    /* DEBUG: check rAAtr norm */
    rAAtr=0.0;
    for(n=ns_p;n<ne_p;n++) for(i=0;i<sz[n/ne_p];i++) rAAtr += g_ar[n][i]*g_ar[n][i];
    MPI_Allreduce(&rAAtr,&rAAtr,1,MPI_DOUBLE,MPI_SUM,g_cart);

    if(g_debug_level>1) if(g_proc_id==0) fprintf(stdout,"CG-res^2:%g, A-norm:%g\n",r2,rAAtr);
  }
  if(g_np_ntau>1) xchange_fields_flow_up(x);
  if((it==itermax) && (g_proc_id==0)) fprintf(stdout,"CGNE WARNING: max iterations %d exceeded !!\n",itermax);
  return it;

}
