/***********************************************************************
 file: matrix_mult_typedef.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
*******************************************************************************/


/* Type definition of the pointer to the function which contains the matrix multiplication. */

#ifndef _MATRIX_MULT_TYPEDEF_H
#define _MATRIX_MULT_TYPEDEF_H

typedef void (*matrix_mult_ode)(double *, double *, double **, int, int, int, int);
typedef void (*matrix_mult_cgne)(double **, double **, double, double **, int, int);
typedef void (*matrix_mult_ferm)(dcomplex *, dcomplex *, int, int, double *);

#endif
