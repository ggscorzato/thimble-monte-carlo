/*****************************************************************************
 file: force_hub.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: August 2013
 *******************************************************************************/

#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "xchange_fields.h"
#include "init_model.h"
#include "ferm_matrices.h"
#include "ferm_force.h"
#include "force.h"
#if (MODEL == HUB)

/* 
   force = - sign dS_R/dphi (note the minus sign!)
*/

void force_hub(double *ff, double *phi, double ** nullpt, int nt, int dumm_ph, int acc, int sign){

  int j;

  /*** Exchange the boundaries (always at the start of the routine that uses them). Needed for fermionic part ***/
  xchange_fields(phi);

  /*** set the output to zero, if accumulation is not wanted ***/
  if(!acc) for(j=0;j<g_size;j++) ff[j]=0;

  /* bosonic part of the force */
  for(j=0;j<g_vol;j++){
    ff[2*j  ] -= sign*g_dbeta * phi[2*j  ];
    ff[2*j+1] += sign*g_dbeta * phi[2*j+1];
  }
  /* fermionic part of the force */
  if(g_ferm_stoch==1) {
    ferm_force_stoch(ff,phi,nt,&ferm_matrix_hub,&ferm_matrixt_hub);
  } else {
    ferm_force_exact(ff,phi);
  }
}

#endif
