/*****************************************************************************
 file: ferm_TrLog_exact_hub.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
 *******************************************************************************/

#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "init_model.h"
#include "ferm_matrices.h"
#if (MODEL == HUB)

/* 
   Compute res = - Tr log[conj[M]]
*/

#if (USEMAGMA == 1)
#endif // MAGMA
#endif



