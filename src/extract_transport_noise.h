/*****************************************************************************
 file: extract_transport_noise.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _EXTRACT_TRANSPORT_NOISE_H
#define _EXTRACT_TRANSPORT_NOISE_H

void extract_transport_noise(double **noise, double **phi, double *n2n, int Nstart, int Nend);

#endif
