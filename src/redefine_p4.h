/*****************************************************************************
 file: redefine_p4.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: December 2013
*******************************************************************************/

#ifndef _REDEFINE_P4_H
#define _REDEFINE_P4_H

#define init_model_model init_model_p4
#define finalize_model_model finalize_model_p4
#define init_projector_model init_projector_p4
#define search_min_model search_min_p4
#define search_min_lapack_model search_min_lapack_p4
#define action_model action_p4
#define force_model force_p4
#define mhessian_model mhessian_p4
#define density_model density_p4
#define phi2_model phi2_p4
#define tests_model tests_p4
#define online_measurements_model online_measurements_p4
#endif
