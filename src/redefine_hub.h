/*****************************************************************************
 file: redefine_hub.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: December 2013
*******************************************************************************/

#ifndef _REDEFINE_HUB_H
#define _REDEFINE_HUB_H

#define init_model_model init_model_hub
#define finalize_model_model finalize_model_hub
#define init_projector_model init_projector_hub
#define search_min_model search_min_hub
#define search_min_lapack_model search_min_lapack_hub
#define action_model action_hub
#define force_model force_hub
#define mhessian_model mhessian_hub
#define ferm_init_full_mat_model ferm_init_full_mat_hub
#define ferm_update_full_mat_model ferm_update_full_mat_hub
#define ferm_print_full_mat_model ferm_print_full_mat_hub
#define ferm_matrix_model ferm_matrix_hub
#define ferm_matrixt_model ferm_matrixt_hub
#define ferm_TrLog_exact_model ferm_TrLog_exact_hub
#define ferm_TrdMiM_exact_model ferm_TrdMiM_exact_hub
#define ferm_TrddMiM_exact_model ferm_TrddMiM_exact_hub
#define ferm_TrdMiMdMiM_exact_model ferm_TrdMiMdMiM_exact_hub
#define ferm_cntr13_dM_stoch_model ferm_cntr13_dM_stoch_hub
#define ferm_cntr23_dM_stoch_model ferm_cntr23_dM_stoch_hub
#define ferm_cntr_ddM_stoch_model ferm_cntr_ddM_stoch_hub
#define tests_model tests_hub
#define online_measurements_model online_measurements_hub

#endif
