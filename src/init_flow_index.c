/*****************************************************************************
 file: init_flow_index.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: February 2014
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <complex.h>
#include "global.h"
#include "init_flow_index.h"

/*
  This routine sets the parallelization along the flow direction.
 */

void init_flow_index(){
  int i,ntll;

  if(g_proc_id==0) fprintf(stdout,"Initializing flow index ...");

  /* proc coordinate along flow dir corresponding to fist/last active node (used in residual phase) */
  g_pc_flow_1st=0; 
  g_pc_flow_last=g_np_ntau-1; 

  if(g_proc_coords[g_flowdir]==0){
    if(g_np_ntau==1){
      ntll = g_nt_tot;
      if(ntll != g_nt_loc_first) fprintf(stdout,"resetting g_nt_loc_first -> %d ...",ntll);
      g_nt_loc_first = ntll;
    }

    g_nt_loc=g_nt_loc_first;
    g_nt_1st=0;

  } else if(g_proc_coords[g_flowdir]==g_np_ntau-1){
    if(g_np_ntau==2){
      ntll = ( g_nt_tot - g_nt_loc_first);
      if(ntll != g_nt_loc_last) fprintf(stdout,"resetting g_nt_loc_last -> %d ...",ntll);
      g_nt_loc_last = ntll;
    }
    g_nt_loc=g_nt_loc_last;
    g_nt_1st=g_nt_tot - g_nt_loc;

  } else {

    g_nt_loc = ( g_nt_tot - g_nt_loc_first - g_nt_loc_last )/(g_np_ntau-2);
    g_nt_1st = g_proc_coords[g_flowdir] * g_nt_loc;
    if(g_nt_loc * (g_np_ntau-2) != ( g_nt_tot - g_nt_loc_first - g_nt_loc_last )){
      fprintf(stderr,"ERROR: mismatch between g_np_ntau =%d, g_nt_tot=%d, g_nt_loc_first=%d, g_nt_loc_last=%d !!\n",
	      g_np_ntau,g_nt_tot,g_nt_loc_first,g_nt_loc_last);
      exit(-4);
    }
  }

  /* create in proc_id=0 a list of all nt_loc and their starting indices */
  if(g_proc_id==0){
    g_ntloc_list = (int*) calloc(g_np_ntau,sizeof(int));
    g_ntdisp_list = (int*) calloc(g_np_ntau,sizeof(int));
  }

  MPI_Gather(&g_nt_loc,1,MPI_INT,g_ntloc_list,1,MPI_INT,0,g_cart);

  if(g_proc_id==0){
    g_ntloc_list[0]++; /* first slice has g_nt_loc+1 points*/
    g_ntdisp_list[0]=0;
    for(i=1;i<g_np_ntau;i++){
      g_ntdisp_list[i]=g_ntdisp_list[i-1]+g_ntloc_list[i-1];
    }
  }

  MPI_Barrier(g_cart);
  if(g_proc_id==0) fprintf(stdout,"OK!\n");
}

