/*****************************************************************************
 file: ferm_cuda.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2014
 *******************************************************************************/

#ifndef _FERM_CUDA_H
#define _FERM_CUDA_H

#if defined MAIN_PROGRAM
 #define EXTERN
#else
 #define EXTERN extern
#endif

#if defined CUDA_FILE
 #define EXTC extern "C"
#else
 #define EXTC
#endif


#if (MODEL == HUB)
#if (USEMAGMA == 1)

EXTERN  cuDoubleComplex *gpu_work, *gpu_A, *gpu_M, *gpu_res;
EXTERN  dcomplex *g_res;
EXTERN  __constant__ cuDoubleComplex gpu_udb;
EXTERN  double *gpu_field, *gpu_ff, *gpu_gg;
EXTERN  __constant__ double gpu_tdb, gpu_Umu[2];
EXTERN  int *gpu_ineightu, *gpu_ineigh, *gpu_s, g_gpugrid;
EXTERN  __constant__ int gpu_csize, gpu_NF, gpu_timedir;

EXTC void ferm_init_full_mat_hub();
EXTC void ferm_update_full_mat_hub(double *phi);
EXTC void ferm_TrLog_exact_hub(dcomplex *pfact,cuDoubleComplex *mat);
EXTC void ferm_TrdMiM_exact_hub(double *f, cuDoubleComplex *mat);
EXTC void ferm_TrdMiMdMiM_exact_hub(double *etap,double *eta, cuDoubleComplex *mat);
EXTC void ferm_TrddMiM_exact_model(double *etap, double *eta, cuDoubleComplex *mat);

__global__ void cuda_ferm_init_full_mat_hub(cuDoubleComplex *M, int *s, int *intu, int *in);
__global__ void cuda_ferm_update_full_mat_hub(double *_field, cuDoubleComplex *_M, int *_s, int *_ineightu);
__global__ void cuda_ferm_TrLog_exact_hub(cuDoubleComplex *res, cuDoubleComplex *mat);
__global__ void cuda_ferm_TrdMiM_exact_hub(double *_ff, cuDoubleComplex *mat, int *_s, int *_ineightu);
__global__ void cuda_ferm_TrdMiMdMiM_exact_hub(double *etap, double *eta, cuDoubleComplex *mat, int *_s, int *_ineightu);

#endif // USEMAGMA
#endif // MODEL HUB
#endif // _FERM_CUDA_H
