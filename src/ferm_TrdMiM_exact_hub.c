/*****************************************************************************
 file: ferm_TrdMiM_exact_hub.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
 *******************************************************************************/

#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "init_model.h"
#include "ferm_matrices.h"
#if (MODEL == HUB)

/* 
   Compute f_k += - conj( d_k S_F ) =  conj( +Tr[d_k M * M^-1] )
   (assume that mat = conj(M^-1) )
*/

#if (USEMAGMA == 0)
void ferm_TrdMiM_exact_hub(double *f, dcomplex *mat){

  int j,jc,s;
  dcomplex ctmp;
    
  for(j=0;j<g_csize;j++){
    jc=g_ineigh[g_timedir][UP][j];
    s=1;
    if(jc<j) s=-1;
    ctmp = mat[jc*g_csize+j];
    ctmp *= conj(-s * g_udb);
    f[2*j]  += creal(ctmp);
    f[2*j+1]+= cimag(ctmp);
  }
}
#endif  // USEMAGMA
#endif
