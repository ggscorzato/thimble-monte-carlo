/*****************************************************************************
 file: ferm_matrices.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
 *******************************************************************************/

#ifndef _FERM_MATRICES_H
#define _FERM_MATRICES_H

#if (USEMAGMA == 0)
void ferm_init_full_mat_model();
void ferm_update_full_mat_model(double *phi);
void ferm_TrLog_exact_model(dcomplex *res, dcomplex *mat);
void ferm_TrdMiM_exact_model(double *f, dcomplex *mat);
void ferm_TrddMiM_exact_model(double *etap, double *eta, dcomplex *mat);
void ferm_TrdMiMdMiM_exact_model(double *etap, double *eta, dcomplex *mat);
#endif

void ferm_print_full_mat_model(int fl);
void ferm_matrix_model(dcomplex *etap, dcomplex *eta, int fl, int id, double *phi);
void ferm_matrixt_model(dcomplex *etap, dcomplex *eta, int fl, int id, double *phi);
void ferm_cntr13_dM_stoch_model(dcomplex *res, double *eta, dcomplex *xi, double alpha);
void ferm_cntr23_dM_stoch_model(double *res, dcomplex *x, dcomplex *y, double alpha);
void ferm_cntr_ddM_stoch_model(double *res, dcomplex *x, dcomplex *y, double *eta, double alpha);
#endif


