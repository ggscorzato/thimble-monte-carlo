/*****************************************************************************
 file: accept_reject.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _ACCEPT_REJECT_H
#define _ACCEPT_REJECT_H

int accept_reject(double *phi_new, double *phi);

#endif
