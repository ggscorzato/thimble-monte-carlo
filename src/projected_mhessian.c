/*****************************************************************************
 file: projected_mhessian.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: February 2014
 *******************************************************************************/

#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "init_model.h"
#include "mhessian.h"

/* 
   -Hessian matrix H projected with P = (JV)(JV)^T 
   etap  = acc*eta + P H(phi[n_tau],(phi[n_tau +/-1])) * eta
   ( Iwa is currently not used. Keep it as dumb index to match the same function prototype of as 
   force_model() and mhessian_model() )
*/

void projected_mhessian(double *etap, double *eta, double **phi, int n_tau, int pm_half, int acc, int Iwa){

  int j,k;
  double c_j,*etaq;

  mhessian_model(etap,eta,phi,n_tau,pm_half,0,Iwa);

  if(g_with_projected_mhessian){
    
    etaq=(double *)calloc(g_size,sizeof(double));
    
    for(k=0;k<g_size;k++) etaq[k]=etap[k];
    for(k=0;k<g_size;k++) etap[k]=0.0;
    
    for(j=0;j<g_tvol*CPS;j++){
      
      /*c_j = <(J bas^j )^T,etaq>  */
      c_j=0.0;
      for(k=0;k<g_csize;k++){
  	c_j += g_thimb_tan_basis[j][n_tau][2*k  ] * etaq[2*k+1];
  	c_j -= g_thimb_tan_basis[j][n_tau][2*k+1] * etaq[2*k  ];
      }
      MPI_Allreduce(&c_j,&c_j,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
      
      /* etap = \sum_k c_j (J bas^j)_k   */      
      for(k=0;k<g_csize;k++){
  	etap[2*k+1] += c_j * g_thimb_tan_basis[j][n_tau][2*k  ];
  	etap[2*k  ] -= c_j * g_thimb_tan_basis[j][n_tau][2*k+1];
      }
    }
    free(etaq);
  }

  if(acc) for(k=0;k<g_size;k++) etap[k]+=eta[k];

}
