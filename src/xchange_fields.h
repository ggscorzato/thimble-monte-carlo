/*****************************************************************************
 file: xchange_fields.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _XCHANGE_FIELDS_H
#define _XCHANGE_FIELDS_H

void xchange_fields(double *);
void xchange_fields_flow_up(double **);
void xchange_fields_flow_dn(double **);

#endif
