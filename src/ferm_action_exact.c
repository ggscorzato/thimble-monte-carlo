/*****************************************************************************
 file: ferm_action_exact.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <complex.h>
#include "global.h"
#include "ferm_matrices.h"
#include "ferm_cuda.h"
#include "ferm_action.h"
#ifdef WITHFERM
/*
  Compute the fermion action
*/

dcomplex ferm_action_exact(double * phi){

  int fl,j;
  int info,n,m,lda,n2;
  dcomplex fact=0.0;

  n2=g_csize*g_csize;
  n=g_csize; m=n; lda=n;

  /* update the conj fermion matrix g_M */
  ferm_update_full_mat_model(phi);

  for(fl=0;fl<g_NF;fl++){

    /* init the full matrix to be factorized */
    #if (USEMAGMA == 1)
    magma_zcopymatrix(n,n,(magmaDoubleComplex *)gpu_M+fl*n2,n,(magmaDoubleComplex *)gpu_A,n);
    #else
    for(j=0;j<n2;j++) g_A[j] = g_M[fl][j];
    #endif

    /* LU factorize  */
    #if (USEMAGMA == 1)
    magma_zgetrf_gpu(n,n,(magmaDoubleComplex *)gpu_A,n,g_ipiv,&info);
    #else
    zgetrf_(&m,&n,g_A,&lda,g_ipiv,&info);
    #endif

    if(info!=0){
      if(g_proc_id==0) fprintf(stderr,"ferm action zgetrf info=%d\n",info); 
      fflush(stdout); exit(-58);
    }

    /* fact = - Tr log [FM] = - Tr log[conj(M)] */
    #if (USEMAGMA == 1)
    ferm_TrLog_exact_model(&fact,gpu_A);
    #else
    for(j=0;j<g_csize;j++){
      fact-=clog(conj(g_A[j*n+j]));
    }
    #endif
  }
  return fact;
}
#endif
