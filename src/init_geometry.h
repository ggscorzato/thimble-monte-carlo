/***********************************************************************
 file: init_geometry.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 ***********************************************************************/

#ifndef _INIT_GEOMETRY_H
#define _INIT_GEOMETRY_H

void init_geometry();
void index2coord(int * coord, int index, int *cbasis, int *l);
int coord2index(int *coord, int *cbasis);

#endif
