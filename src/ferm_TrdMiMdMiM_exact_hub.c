/*****************************************************************************
 file: ferm_TrdMiMdMiM_exact_hub.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
 *******************************************************************************/

#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "init_model.h"
#include "ferm_matrices.h"
#if (MODEL == HUB)

/* 
   Compute etap_k += - conj(H_{F,d d}) = - conj( Tr[FM^-1 d_k FM FM^-1 d_h FM] eta_h )
   (assume that mat = conj(FM^-1) )
*/

#if (USEMAGMA == 0)
void ferm_TrdMiMdMiM_exact_hub(double *etap, double *eta, dcomplex *mat){

  int li,mj,lip,mjp,sl,sm;
  dcomplex ctmp, ceta, cetap, udb2;

  udb2=g_udb*g_udb;

  for(mj=0;mj<g_csize;mj++){
    mjp=g_ineigh[g_timedir][UP][mj];
    sm=1;
    if(mjp<mj) sm=-1;
    for(li=0;li<g_csize;li++){
      lip=g_ineigh[g_timedir][UP][li];
      sl=1;
      if(lip<li) sl=-1;
      ctmp = udb2 * mat[li+g_csize*mjp] * mat[mj+g_csize*lip] * (sm * sl);
      ceta = conj(eta[2*li] + I*eta[2*li+1]);
      cetap=ctmp * ceta;
      etap[mj*2  ] -= creal(cetap);
      etap[mj*2+1] -= cimag(cetap);
    }
  }
}
#endif  // USEMAGMA
#endif
