/*****************************************************************************
 file: steepest_descent.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _STEEPEST_DESCENT_H
#define _STEEPEST_DESCENT_H

void steepest_descent(double **phi, double *phi0old, double act, double n2n);

#endif
