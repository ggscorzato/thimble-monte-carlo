/*****************************************************************************
 file: source_noise_IMR.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _SOURCE_NOISE_IMR_H
#define _SOURCE_NOISE_IMR_H

void source_noise_IMR_FE(double **b, double **noise, double **phi, double dtau, int nb, int ne);
void source_noise_IMR_FB(double **b, double **noise, double **phi, double dtau, int nb, int ne);

#endif
