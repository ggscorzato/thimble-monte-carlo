/*****************************************************************************
 file: ferm_cntr_ddM_stoch_hub.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
 *******************************************************************************/

#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "init_model.h"
#include "ferm_matrices.h"
#if (MODEL == HUB)

/* 
   Compute       res_k += alpha [conj( x_i d_k d_h FM_ij)  y_j] conj(eta_h) 
   (y happens to be real where this routine is used (ferm_mhessian_stoch))
*/

void ferm_cntr_ddM_stoch_hub(double *res, dcomplex *x, dcomplex *y, double *eta, double alpha){

  /* do nothing, since ddFM=0  for the Hubbard model*/

}
#endif
