/*****************************************************************************
 file: ferm_extract_noise.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "global.h"
#include "ranlxd.h"
#include "ferm_extract_noise.h"
#ifdef WITHFERM

void ferm_extract_noise(){
  int r,fl,j,k;

  for(r=0;r<g_NR;r++){
    gauss_vectorz(g_xi[r],g_csize);
    for(fl=0;fl<g_NF;fl++){
      for(k=0;k<g_nt_loc+1;k++){
	for(j=0;j<g_csize;j++){
	  g_pb0[fl][r][k][j]=0.0;
	  g_pb2[fl][r][k][j]=0.0;
	}
      }
    }
  }
}

void ferm_extract_basis(){
  int r,fl,j,k;
  
  if(g_NR!=g_csize){
    fprintf(stderr,"To do this test, we need g_NR=g_csize. Instead we have g_NR=%d and g_csize=%d",
	    g_NR,g_csize);
    exit(-3);
  }

  for(r=0;r<g_NR;r++){
    for(j=0;j<g_csize;j++){
      g_xi[r][j]=0.0;
    }
    g_xi[r][r]=sqrt((double)g_NR);
  }

  for(r=0;r<g_NR;r++){
    for(fl=0;fl<g_NF;fl++){
      for(k=0;k<g_nt_loc+1;k++){
	for(j=0;j<g_csize;j++){
	  g_pb0[fl][r][k][j]=0.0;
	  g_pb2[fl][r][k][j]=0.0;
	}
      }
    }
  }
}
#endif
