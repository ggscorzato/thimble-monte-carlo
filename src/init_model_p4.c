/*****************************************************************************
 file: init_model_p4.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <ggfft.h>
#include "global.h"
#include "init_model.h"
#if (MODEL == P4)

/* See minimum_phi4_source.nb */

void init_model_p4(){

  int deg=CPS;
  
  if(g_proc_id==0) fprintf(stdout,"Initializing model P4...");

  /*** Sizes ***/
  g_size=g_vol*DPS;
  g_esize=g_evol*DPS;
  g_csize=g_vol*CPS;
  g_cesize=g_evol*CPS;
  g_svol=g_vol/g_l[g_timedir];
  g_T=g_l[g_timedir];

  if(g_proc_id==0) fprintf(stdout,"OK!\n");

  /*** Init the FFT ***/
  if(g_proc_id==0) fprintf(stdout,"Initializing fft...");

  gg_init_plan(&g_plan, DIM, g_L, g_nprocl, g_indexord, deg, g_sub_cart);

  if(g_proc_id==0) fprintf(stdout,"OK!\n");

  /*************/
  MPI_Barrier(g_cart);
}

void finalize_model_p4(){
  /*** finalize fourier ***/
  gg_destroy_plan(&g_plan);
}
#endif
