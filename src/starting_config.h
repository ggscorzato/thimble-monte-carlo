/*****************************************************************************
 file: starting_config.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _STARTING_CONFIG_H
#define _STARTING_CONFIG_H

void starting_config(double **phi);

#endif
