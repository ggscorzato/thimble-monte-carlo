/*****************************************************************************
 file: projector.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _PROJECTOR_H
#define _PROJECTOR_H

void projector(double * out, double * in, double *** proj, int field_flag);
void init_projector_from_hessian(double **phi, double *** proj);
void init_projector_model(double *** proj);

#endif
