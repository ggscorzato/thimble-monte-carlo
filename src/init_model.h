/*****************************************************************************
 file: init_model.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _INIT_MODEL_H
#define _INIT_MODEL_H

#if (MODEL==P4)
 #define CPS 2  /* complex per site */
#endif

#if (MODEL==HUB)
 #define CPS 1  /* complex per site */
#endif

#define DPS (2*CPS)   /* double per site */
#define DPMP (4*CPS)  /* double per pair of momenta {p,-p} */

void init_model_model();
void finalize_model_model();

#endif
