/*****************************************************************************
 file: matrices_noise_IMR.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include "global.h"
#include "mhessian.h"
#include "xchange_fields.h"
#include "matrices_noise_IMR.h"

/*
  See Formulae.pdf
  Normally nts=0, nte=g_nt_tot (except in residual phase)
*/

void matrix_noise_IMR_FE(double **out, double **in, double dtau, double **phi, int nts, int nte){

  int h,j,acc=1,iwa=0;
  double dtau2;
  dtau2=0.5*dtau;

  if(g_np_ntau>1) xchange_fields_flow_up(in); /* recv in[g_nt_loc] if not last; send in[0] if not first; */

  /* part O(1) */
  for(h=nts;h<nte-1;h++) for(j=0;j<g_size;j++) out[h][j] = in[h][j] - in[h+1][j];
  h=nte-1;
  if(g_proc_coords[g_flowdir]==g_pc_flow_last){
    for(j=0;j<g_size;j++) out[h][j] = in[h][j];
  } else {
    for(j=0;j<g_size;j++) out[h][j] = in[h][j] - in[h+1][j];
  }

  /* part O(dtau) */
  for(h=nts;h<nte-1;h++){
    for(j=0;j<g_size;j++) g_kk[j] = dtau2*(in[h][j]+in[h+1][j]);
    mhessian_model(out[h],g_kk,phi,h,PLUSHALF,acc,iwa);
  }
  h=nte-1;
  if(g_proc_coords[g_flowdir]==g_pc_flow_last){
    for(j=0;j<g_size;j++) g_kk[j] = dtau2*(in[h][j]);
  } else {
    for(j=0;j<g_size;j++) g_kk[j] = dtau2*(in[h][j]+in[h+1][j]);
  }
  mhessian_model(out[h],g_kk,phi,h,PLUSHALF,acc,iwa);
}

void matrix_noise_IMR_FB(double **out, double **in, double dtau, double **phi, int nts, int nte){

  int h,j,acc=1,iwa=0;
  double dtau2;
  dtau2=0.5*dtau;

  if(g_np_ntau>1) xchange_fields_flow_up(in); /* recv in[g_nt_loc] if not last; send in[0] if not first; */

  /* part O(1) */
  h=nts;
  if(g_proc_coords[g_flowdir]==g_pc_flow_1st){
    for(j=0;j<g_size;j++) out[h][j] = -in[h+1][j];
  } else {
    for(j=0;j<g_size;j++) out[h][j] = in[h][j] - in[h+1][j];
  }
  for(h=nts+1;h<nte;h++) for(j=0;j<g_size;j++) out[h][j] = in[h][j] - in[h+1][j];

  /* part O(dtau) */
  h=nts;
  if(g_proc_coords[g_flowdir]==g_pc_flow_1st){
    for(j=0;j<g_size;j++) g_kk[j] = dtau2*(in[h+1][j]);
  } else {
    for(j=0;j<g_size;j++) g_kk[j] = dtau2*(in[h][j]+in[h+1][j]);
  }
  mhessian_model(out[h],g_kk,phi,h,PLUSHALF,acc,iwa);
  for(h=nts+1;h<nte;h++){
    for(j=0;j<g_size;j++) g_kk[j] = dtau2*(in[h][j]+in[h+1][j]);
    mhessian_model(out[h],g_kk,phi,h,PLUSHALF,acc,iwa);
  }
}

void matrixt_noise_IMR_FE(double **out, double **in, double dtau, double **phi, int nts, int nte){

  int k,j,p=0,acc=0,iwa=0,ntsm1;
  double dtau2;
  MPI_Status status[2];
  MPI_Request req[2];
  dtau2=0.5*dtau;
  ntsm1=g_nt_loc; /* if nts>0 it should not be accessed */

  if(g_np_ntau>1) xchange_fields_flow_dn(in); /* recv in[-1=g_nt_loc] if not first; send in[g_nt_loc-1] if not last; */

  /* part O(1) */
  k=nts;
  if(g_proc_coords[g_flowdir]>g_pc_flow_1st){
    for(j=0;j<g_size;j++) out[k][j] = in[k][j] - in[ntsm1][j];
  } else {
    for(j=0;j<g_size;j++) out[k][j] = in[k][j];
  }
  for(k=nts+1;k<nte;k++) for(j=0;j<g_size;j++) out[k][j] = in[k][j] - in[k-1][j];

  /* part O(dtau) */
  k=nte-1; /* do last term first, in order to send it */
  mhessian_model(g_kk,in[k],phi,k,PLUSHALF,acc,iwa);
  for(j=0;j<g_size;j++) out[k][j] += dtau2*g_kk[j];
  if((g_proc_coords[g_flowdir] < g_pc_flow_last) && (nte==g_nt_loc)){
    MPI_Isend(g_kk,g_size,MPI_DOUBLE,g_neigh_node[g_flowdir][UP],7877,g_cart,&req[p]);
    p++;
  }
  if((g_proc_coords[g_flowdir] > g_pc_flow_1st) && (nts==0)){ /* recv the other contribution to the first term */
    MPI_Irecv(g_ff,g_size,MPI_DOUBLE,g_neigh_node[g_flowdir][DN],7877,g_cart,&req[p]);
    p++;
  }

  for(k=nts;k<nte-1;k++){
    mhessian_model(g_kk,in[k],phi,k,PLUSHALF,acc,iwa);
    for(j=0;j<g_size;j++) out[k][j] += dtau2*g_kk[j];
    for(j=0;j<g_size;j++) out[k+1][j] += dtau2*g_kk[j];
  }

  if(p>0) MPI_Waitall(p,req,status);

  if((g_proc_coords[g_flowdir] > g_pc_flow_1st) && (nts==0)){
    for(j=0;j<g_size;j++) out[0][j] += dtau2*g_ff[j];
  }
}

void matrixt_noise_IMR_FB(double **out, double **in, double dtau, double **phi, int nts, int nte){

  int k,j,p=0,acc=0,iwa=0,ntsm1;
  double dtau2;
  MPI_Status status[2];
  MPI_Request req[2];
  dtau2=0.5*dtau;
  ntsm1=g_nt_loc; /* if nts>0 it should not be accessed */

  if(g_np_ntau>1) xchange_fields_flow_dn(in); /* recv in[-1=g_nt_loc] if not first; send in[g_nt_loc-1] if not last; */

  /* part O(1) */
  k=nts;
  if(g_proc_coords[g_flowdir]>g_pc_flow_1st) for(j=0;j<g_size;j++) out[k][j] = in[k][j] - in[ntsm1][j];

  for(k=nts+1;k<nte;k++) for(j=0;j<g_size;j++) out[k][j] = in[k][j] - in[k-1][j];

  k=nte;
  if(g_proc_coords[g_flowdir]==g_pc_flow_last) for(j=0;j<g_size;j++) out[k][j] = - in[k-1][j];

  /* part O(dtau) */
  k=nte-1; /* do last term first, in order to send it */
  mhessian_model(g_kk,in[k],phi,k,PLUSHALF,acc,iwa);
  for(j=0;j<g_size;j++) out[k][j] += dtau2*g_kk[j];
  if(g_proc_coords[g_flowdir] < g_pc_flow_last){ 
    if(nte==g_nt_loc){
      MPI_Isend(g_kk,g_size,MPI_DOUBLE,g_neigh_node[g_flowdir][UP],7879,g_cart,&req[p]);
      p++;
    }
  } else {
    for(j=0;j<g_size;j++) out[k+1][j] += dtau2*g_kk[j];
  }
  if((g_proc_coords[g_flowdir] > g_pc_flow_1st) && (nts==0)){ /* recv the other contribution to the first term */
    MPI_Irecv(g_ff,g_size,MPI_DOUBLE,g_neigh_node[g_flowdir][DN],7879,g_cart,&req[p]);
    p++;
  }

  for(k=nts;k<nte-1;k++){
    mhessian_model(g_kk,in[k],phi,k,PLUSHALF,acc,iwa);
    if((k>nts) || (g_proc_coords[g_flowdir]>g_pc_flow_1st)) for(j=0;j<g_size;j++) out[k][j] += dtau2*g_kk[j];
    for(j=0;j<g_size;j++) out[k+1][j] += dtau2*g_kk[j];
  }

  if(p>0) MPI_Waitall(p,req,status);

  if((g_proc_coords[g_flowdir] > g_pc_flow_1st) && (nts==0)){
    for(j=0;j<g_size;j++) out[0][j] += dtau2*g_ff[j];
  }
}
