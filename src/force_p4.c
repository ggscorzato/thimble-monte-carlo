/*****************************************************************************
 file: force_p4.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "xchange_fields.h"
#include "init_model.h"
#include "force.h"
#if (MODEL == P4)

/* 
   force = - sign dS_R/dphi (note the minus sign!)
   (see Eq.(2.13-2.14) of 0902.4686, with opposite sign for K^I)
*/

void force_p4(double *ff, double *phi, double ** nullpt, int nt, int dumm_ph, int acc, int sign){

  int j,aj,bj,k,ak,bk,a,b,mu,ver;
  double dm,chmu,shmu;

  dm=2*DIM + g_mass*g_mass;

  /*** Exchange the boundaries (always at the start of the routine that uses them) ***/
  xchange_fields(phi);

  /*** set the output to zero, if accumulation is not wanted ***/
  if(!acc) for(j=0;j<g_size;j++) ff[j]=0;

  /*** main loop (see Eq.(2.13-2.14) of 0902.4686, with opposite sign for K^I) ***/
  for(j=0;j<g_vol;j++){

    /** source term **/
    ff[j*DPS] -= sign*g_h;

    for(a=0;a<CPS;a++){
      aj=j*DPS+a*CPS;

      /** mass term **/
      ff[aj  ] -= sign*dm*phi[aj  ];
      ff[aj+1] += sign*dm*phi[aj+1];

      /** Interacting term **/
      for(b=0;b<CPS;b++){
	bj=j*DPS+b*CPS;
	ff[aj  ] += -sign*g_lambda*phi[aj  ]*(phi[bj]*phi[bj] - phi[bj+1]*phi[bj+1]);
	ff[aj  ] +=  sign*2*g_lambda*phi[aj+1]*(phi[bj]*phi[bj+1]);
	ff[aj+1] +=  sign*g_lambda*phi[aj+1]*(phi[bj]*phi[bj] - phi[bj+1]*phi[bj+1]);
	ff[aj+1] +=  sign*2*g_lambda*phi[aj  ]*(phi[bj]*phi[bj+1]);
      }

      /** derivative term **/
      for(mu=0;mu<DIM;mu++){
	if(mu==g_timedir){
	  chmu=cosh(g_chempot);
	  shmu=sinh(g_chempot);
	}else{
	  chmu=1;
	  shmu=0;
	}
	for(ver=0;ver<2;ver++){
	  k=g_ineigh[mu][ver][j];
	  ak=k*DPS+a*CPS;
	  bk=k*DPS+(1-a)*CPS;
	  /* epsilon_{a,b}=(a-b)=(2a-1) [if b=1-a]={{0,-1;1,0}}} */
	  ff[aj  ]+= sign*(  chmu*phi[ak  ] + (2*a-1)*(2*ver-1)*shmu*phi[bk+1]);
	  ff[aj+1]+= sign*( -chmu*phi[ak+1] + (2*a-1)*(2*ver-1)*shmu*phi[bk  ]);
	}
      }
    }
  }
}
#endif
