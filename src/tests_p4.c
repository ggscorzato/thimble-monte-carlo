/***********************************************************************
 file: tests_p4.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 ***********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <math.h>
#include "global.h"
#include "ranlxd.h"
#include "init_model.h"
#include "projector.h"
#include "mhessian.h"
#include "force.h"
#include "action.h"
#include "matrices_noise_IMR.h"
#include "matrices_SD_IMR.h"
#include "ode_rk4.h"
#include "ode_euler.h"
#include "observables.h"
#include "steepest_descent.h"
#include "starting_config.h"
#include "cgne.h"
#include "constraints.h"
#include "source_noise_IMR.h"
#include "init_geometry.h"
#include "load_store_config.h"
#include "tests.h"
#if (MODEL == P4)

void tests_p4(){

  int i,j,h,k,dd,dum=0, acc=0, iwa, iter, all_AAT=0, sz[2], coords[DIM];
  int ks,ke,jmax, Nt_con, Nt_deg, pe, ext_ne_r;
  double rAp,pAtr,rHp,pHr,delta,vecti[8],vecto1[4],vecto2[4];
  double lastn2n,lastact,matdtau=0.4;
  MPI_Status stat;

  if(g_vol != g_tvol){ 
    if(g_proc_id==0) fprintf(stdout,"Domain Decomposed version not tested yet. Exiting\n");
    MPI_Abort(MPI_COMM_WORLD,-1);
    exit(-111);
  }
  if(g_nt_loc != (g_nt_loc/g_nsub)*g_nsub){
    if(g_proc_id==0) fprintf(stdout,"g_nt_loc =%d must be multiple of g_nsub =%d. Exiting\n",g_nt_loc,g_nsub);
    MPI_Abort(MPI_COMM_WORLD,-1);
    exit(-112);
  }

  MPI_Barrier(g_cart);
  /*** new fields test load store ***/
  if(g_np_ntau==1){
    load_store_toy(111,STORE);
    load_store_toy(111,LOAD);
  } else{
    load_store_toy(111,LOAD);
  }


  if(g_np_ntau==1){
    //    for(h=0;h<g_nt_loc+1;h++) for(j=0;j<g_size;j++)  g_field[h][j] = (double)(h*1000 + j);
    for(h=0;h<g_nt_loc+1;h++) gauss_vectord(g_field[h],g_size);
    load_store_config(0,g_field,STORE);
  } else {
    load_store_config(0,g_field,LOAD);
  }

  for(pe=0; pe<g_np_ntau; pe++){
    MPI_Barrier(g_cart);
    if(g_proc_coords[g_flowdir]==pe){
      for(h=0;h<g_nt_loc;h++){
	for(j=0;j<g_size;j++){
	  //	  fprintf(stdout,"field[%d][%d][%d]=%g\n",pe,h,j,g_field[h][j]);
	}
      }
    }
  }

  MPI_Barrier(g_cart);

  /***** some variables *****/
  Nt_con=g_nt_loc;
  Nt_deg=g_nt_loc;
  if(g_proc_coords[g_flowdir]==0) Nt_con=g_nt_loc+1;
  if(g_proc_coords[g_flowdir]==g_np_ntau-1) Nt_deg=g_nt_loc+1;
  sz[0]=g_size;
  sz[1]=EXTRACOND;
  ext_ne_r=g_nt_loc;
  if((EXTRACOND > 0) && (g_proc_coords[g_flowdir] == g_pc_flow_1st)) ext_ne_r++;

  MPI_Barrier(g_cart);

  /***** test of projector and hessian *****/
  if(g_proc_id==0) fprintf(stdout,"\n\nGG test starts.\n");

  MPI_Barrier(g_cart);

  /*** restart random number generator with the reference seed ***/
  g_seed=826697020;
  rlxd_init(1,g_seed+g_proc_id);

  if(g_proc_id==0) fprintf(stdout,"\n***\n Restart random number generator with the reference seed: %d.\n",g_seed);

  MPI_Barrier(g_cart);
  if(g_proc_coords[g_flowdir]==0){
    /*** set field to zero ***/
    for(j=0;j<g_size;j++) g_field[0][j]=0;

    /*** init random field ***/
    for(h=0;h<DPS;h++){
      gauss_vectord(g_ff,g_vol);
      for(j=0;j<g_vol;j++) g_field[0][j*DPS+h]=g_ff[j];
    }
    
    /*** print random field ***/
    for(j=0;j<g_size;j++) fprintf(stdout,"phi: %g\n",g_field[0][j]);

    /*** extract noise (ordered like Marco) ***/
    for(h=0;h<DPS;h++){
      gauss_vectord(g_ff,g_vol);
      for(j=0;j<g_vol;j++) g_noise[0][j*DPS+h]=g_ff[j];
    }

    /*** print-out for check ***/
    for(j=0;j<g_size;j++) fprintf(stdout,"eta: %g\n",g_noise[0][j]);

    /*** apply hessian (symmetric) ***/
    iwa=0;
    mhessian_model(g_kk,g_noise[0],g_field,0,0,acc,iwa);

    /*** print result of hessian ***/
    for(j=0;j<g_size;j++) fprintf(stdout,"etahs: %g\n",-g_kk[j]);

    /*** apply projector computed from hessian ***/
    for(j=0;j<g_vol;j++){
      g_field_new[0][j*DPS]=creal(g_phi0);
      g_field_new[0][j*DPS+1]=cimag(g_phi0);
      g_field_new[0][j*DPS+2]=0.;
      g_field_new[0][j*DPS+3]=0.;
    }
    init_projector_from_hessian(g_field_new,g_projdb);
    for(j=0;j<g_vol;j++){
      index2coord(coords,j,g_cbasis,g_l);
      if(g_samemom[j]) dd=DPS;
      if(!g_samemom[j]) dd=DPMP;
    
      gauss_vectord(vecti,DPMP);
      fprintf(stdout,"dd=%d\n",dd);
      fprintf(stdout,"Proj[%d]: ",j);
      for(h=0;h<DPS;h++){
	vecto1[h]=0.;
	for(k=0;k<dd;k++){
	  vecto1[h]+=g_proj[j][h][k] * vecti[k];
	  fprintf(stdout,"%g, ",g_proj[j][h][k]);
	}
      }
      fprintf(stdout,"\n");
      fprintf(stdout,"Proj from H[%d]: ",j);
      for(h=0;h<DPS;h++){
	vecto2[h]=0.;
	for(k=0;k<dd;k++){
	  vecto2[h]+=g_projdb[j][h][k] * vecti[k];
	  fprintf(stdout,"%g, ",g_projdb[j][h][k]);
	}
      }
      fprintf(stdout,"\n");
      delta=0.;
      for(k=0;k<DPS;k++) delta+=fabs(vecto1[k]-vecto2[k]);
      fprintf(stdout,"Diff P1v-P2v[%d,%d,(%d,%d,%d,%d)]: %g\n",j,g_samemom[j],coords[0],coords[1],coords[2],coords[3],delta);
    }
    projector(g_kk,g_noise[0],g_projdb,0);
  
    /*** print result of projection from hessian ***/
    for(j=0;j<g_size;j++) fprintf(stdout,"etapfh: %g\n",g_kk[j]);

    /*** apply projector  ***/
    projector(g_noise[0],g_noise[0],g_proj,0);
  
    /*** print result of projection ***/
    for(j=0;j<g_size;j++) fprintf(stdout,"etap: %g\n",g_noise[0][j]);
    delta=0.;
    for(j=0;j<g_size;j++) delta+=fabs(g_noise[0][j]-g_kk[j]);
    fprintf(stdout,"difference between the two projections: %g\n",delta);
  
    /*** compute observables on it ***/
    online_measurements_model(g_field[0]);
    
    /*** compute the gradient = - force ***/
    force_model(g_ff,g_field[0],NULL,dum,dum,acc,-1);
  
    /*** print result of gradient = - force ***/
    for(j=0;j<g_size;j++) fprintf(stdout,"FF: %g\n",g_ff[j]);
  } //  end of if(g_proc_coords[g_flowdir]==0)

  MPI_Barrier(g_cart);

  /*** test hessian symmetric ***/

  if(g_proc_id==0) fprintf(stdout,"\n***\n test hessian symmetric.\n");
  acc=0;
  iwa=0;

  if(g_np_ntau==1){
    for(h=0;h<g_nt_loc;h++) gauss_vectord(g_debug[h],g_size);
    load_store_config(1,g_debug,STORE);
  } else {
    load_store_config(1,g_debug,LOAD);
  }
  for(h=0;h<g_nt_loc;h++) for(i=0;i<g_size;i++) g_r[h][i]=g_debug[h][i];
  
  if(g_np_ntau==1){
    for(h=0;h<g_nt_loc;h++) gauss_vectord(g_debug[h],g_size);
    load_store_config(2,g_debug,STORE);
  } else {
    load_store_config(2,g_debug,LOAD);
  }
  for(h=0;h<g_nt_loc;h++) for(i=0;i<g_size;i++) g_p[h][i]=g_debug[h][i];
  
  for(pe=0; pe<g_np_ntau; pe++){
    MPI_Barrier(g_cart);
    if(g_proc_coords[g_flowdir]==pe){
      for(h=0;h<g_nt_loc;h++){
	mhessian_model(g_ap[h],g_p[h],g_field,h,0,acc,iwa);
	mhessian_model(g_ar[h],g_r[h],g_field,h,0,acc,iwa);
	rHp=0.0;
	for(i=0;i<g_size;i++) rHp += g_ap[h][i]*g_r[h][i];
	pHr=0.0;
	for(i=0;i<g_size;i++) pHr += g_ar[h][i]*g_p[h][i];
	if(g_sub_print_proc==1) fprintf(stdout,"Symmetric Hessian [tau=%d,%d]:"
					" rHp = %15.14f, pHr=%15.14f\n",pe,h,rHp,pHr);
      }
    }
  }

  MPI_Barrier(g_cart);

  /*** restart random number generator with the reference seed ***/
  g_seed=826697020;
  rlxd_init(1,g_seed+g_proc_id);

  if(g_proc_id==0) fprintf(stdout,"\n***\n Restart random number generator with the reference seed: %d.\n",g_seed);

  MPI_Barrier(g_cart);

  /*** test ODE: rk4 vs euler on field ***/
  if(g_proc_id==0) fprintf(stdout,"\n***\n test ODE: rk4 vs euler on field.\n");

  starting_config(g_field);

  /* select the highest one as starting point for BW int. */
  if(g_np_ntau==1){
    for(i=0;i<g_size;i++) g_field[g_nt_tot][i]=g_field[0][i];
  } else {
    if(g_proc_coords[g_flowdir]==0){
      MPI_Send(g_field[0], g_size, MPI_DOUBLE, g_neigh_node[g_flowdir][DN], 33, g_cart);
    }
    if(g_proc_coords[g_flowdir]==g_np_ntau-1){
      MPI_Recv(g_field[g_nt_loc], g_size, MPI_DOUBLE, g_neigh_node[g_flowdir][UP], 33, g_cart, &stat);
    }
  }
  ode_rk4(g_field,&force_model,0,g_nt_loc,1,g_dtau,-1,NULL,BW); /* SA rk4 */
  if(g_proc_coords[g_flowdir]==0){
    for(i=0;i<g_size;i++) g_field_new[0][i]=g_field[0][i];  /* set a common starting point */
    for(i=0;i<g_size;i++) g_diff[0][i]=g_field[0][i]; /* save it  */
  }

  ode_euler(g_field,&force_model,0,g_nt_loc,1,g_dtau,1,NULL,FW);
  ode_rk4(g_field_new,&force_model,0,g_nt_loc,1,g_dtau,1,NULL,FW);

  for(pe=0; pe<g_np_ntau; pe++){
    if(g_proc_coords[g_flowdir]==pe){
      for(h=1;h<g_nt_loc+1;h+=1){
	delta=0.0;
	for(i=0;i<g_size;i++) delta+=fabs(g_field_new[h][i]-g_field[h][i]);
	MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
	if(g_sub_print_proc==1) fprintf(stdout,"ODE difference Euler-RK4: %15.14f, after %d steps (in flow ind %d), with dt=%g.\n",
					delta,h,pe,g_dtau);
	delta=0.0;
	for(i=0;i<g_size;i++) delta+=fabs(g_field[h][i]-g_field[0][i]);
	MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
	if(g_sub_print_proc==1) fprintf(stdout,"ODE distance covered (per proc) with Euler, after %d steps (in proc %d):"
					" %15.14f.\n",h,pe,delta);
	delta=0.0;
	for(i=0;i<g_size;i++) delta+=fabs(g_field_new[h][i]-g_field_new[0][i]);
	MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
	if(g_sub_print_proc==1) fprintf(stdout,"ODE distance covered (per proc) with RK4, after %d steps (in proc %d):"
					" %15.14f.\n",h,pe,delta);
      }
    }
  }

  fflush(stdout);
  MPI_Barrier(g_cart);

  if(g_proc_id==0) fprintf(stdout,"test ODE rk4 and euler: return check on field.\n");

  ode_euler(g_field,&force_model,0,g_nt_loc,1,g_dtau,1,NULL,BW);
  if(g_proc_coords[g_flowdir]==0){
    delta=0.0;
    for(i=0;i<g_size;i++) delta+=fabs(g_diff[0][i]-g_field[0][i]);
    MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
    if(g_proc_id==0) fprintf(stdout,"ODE Euler return check. Nt=%d, dt=%g, delta=%15.14f\n",g_nt_loc,g_dtau,delta);
  }
  ode_rk4(g_field_new,&force_model,0,g_nt_loc,1,g_dtau,1,NULL,BW);

  if(g_proc_coords[g_flowdir]==0){
    delta=0.0;
    for(i=0;i<g_size;i++) delta+=fabs(g_diff[0][i]-g_field_new[0][i]);
    MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
    if(g_proc_id==0) fprintf(stdout,"ODE rk4 return check. Nt=%d, dt=%g, delta=%15.14f\n",g_nt_loc,g_dtau,delta);
  }

  MPI_Barrier(g_cart);

  /*** test ODE: rk4 vs euler on noise ***/

  if(g_proc_id==0) fprintf(stdout,"\n***\n test ODE: rk4 vs euler on noise.\n");

  if(g_proc_coords[g_flowdir]==0){
    for(i=0;i<g_size;i++) g_field_new[0][i]=g_noise[0][i];
    for(i=0;i<g_size;i++) g_diff[0][i]=g_noise[0][i]; /* save it  */
  }
  ode_rk4(g_noise,&mhessian_model,0,g_nt_loc,1,g_dtau,0,g_field,FW); 
  ode_euler(g_field_new,&mhessian_model,0,g_nt_loc,1,g_dtau,0,g_field,FW); 

  for(pe=0; pe<g_np_ntau; pe++){
    if(g_proc_coords[g_flowdir]==pe){
      for(h=1;h<g_nt_loc+1;h+=1){
	delta=0.0;
	for(i=0;i<g_size;i++) delta+=fabs(g_field_new[h][i]-g_noise[h][i]);
	MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
	if(g_sub_print_proc==1) fprintf(stdout,"ODE difference Euler-RK4: %15.14f, after %d steps (in flow ind %d), with dt=%g.\n",
					delta,h,pe,g_dtau);
	delta=0.0;
	for(i=0;i<g_size;i++) delta+=fabs(g_noise[h][i]-g_noise[0][i]);
	MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
	if(g_sub_print_proc==1) fprintf(stdout,"ODE distance covered (per proc) with RK4, after %d steps (in proc %d):"
					" %15.14f.\n",h,pe,delta);
      }
    }
  }

  fflush(stdout);
  MPI_Barrier(g_cart);

  if(g_proc_id==0) fprintf(stdout,"test ODE rk4 and euler: return check on noise.\n");

  ode_euler(g_field_new,&mhessian_model,0,g_nt_loc,1,g_dtau,0,g_field,BW);
  if(g_proc_coords[g_flowdir]==0){
    delta=0.0;
    for(i=0;i<g_size;i++) delta+=fabs(g_diff[0][i]-g_field_new[0][i]);
    MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
    if(g_proc_id==0) fprintf(stdout,"ODE Euler return check. Nt=%d, dt=%g, delta=%15.14f\n",g_nt_loc,g_dtau,delta);
  }
  ode_rk4(g_noise,&mhessian_model,0,g_nt_loc,1,g_dtau,0,g_field,BW);
  if(g_proc_coords[g_flowdir]==0){
    delta=0.0;
    for(i=0;i<g_size;i++) delta+=fabs(g_diff[0][i]-g_noise[0][i]);
    MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
    if(g_proc_id==0) fprintf(stdout,"ODE rk4 return check. Nt=%d, dt=%g, delta=%15.14f\n",g_nt_loc,g_dtau,delta);
  }

  MPI_Barrier(g_cart);

  /*** test ODE with subdivision of intervals ***/

  if(g_proc_id==0) fprintf(stdout,"\n***\n test ODE with subdivision of intervals (meaningful only in proc 0).\n");

  if(g_proc_coords[g_flowdir]==0){
    for(i=0;i<g_size;i++) g_field_new[0][i]=g_field[0][i];   /* set a common starting point */
  }
  ode_rk4(g_field,&force_model,0,g_nt_loc,1,g_dtau,1,NULL,FW);
  ode_rk4(g_field_new,&force_model,0,g_nt_loc/g_nsub,g_nsub,g_dtau*g_nsub,1,NULL,FW);

  if(g_proc_coords[g_flowdir]==0){
    for(h=0;h<g_nt_loc/g_nsub;h++){
      delta=0.0;
      for(i=0;i<g_size;i++) delta+=fabs(g_field_new[h][i]-g_field[h*g_nsub][i]);
      MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
      if(g_sub_print_proc==1){
	fprintf(stdout,"ODE (rk4) check w/o subintervals f[%d...][0]=%15.14f,"
		" -- f_sub[%d][0]=%15.14f, -- diff=%15.14f,\n",
		h*g_nsub,g_field[h*g_nsub][0],h,g_field_new[h][0],delta);
      }
    }
  }

  ode_euler(g_field,&force_model,0,g_nt_loc,1,g_dtau,1,NULL,FW);
  ode_euler(g_field_new,&force_model,0,g_nt_loc/g_nsub,g_nsub,g_dtau*g_nsub,1,NULL,FW);

  if(g_proc_coords[g_flowdir]==0){
    for(h=0;h<g_nt_loc/g_nsub;h++){
      delta=0.0;
      for(i=0;i<g_size;i++) delta+=fabs(g_field_new[h][i]-g_field[h*g_nsub][i]);
      MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
      if(g_sub_print_proc==1){ 
	fprintf(stdout,"ODE (euler) check w/o subintervals f[%d...][0]=%15.14f,"
		"-- f_sub[%d][0]=%15.14f, -- diff=%15.14f,\n",
		h*g_nsub,g_field[h*g_nsub][0],h,g_field_new[h][0],delta);
      }
    }
  }

  MPI_Barrier(g_cart);

  /*** test At = transpose[A] for noise ***/

  if(g_proc_id==0) fprintf(stdout,"\n***\n test At = transpose[A] for noise.\n");

  /* extremes for FB^t */
  ks=0;
  ke=g_nt_loc;
  if(g_proc_coords[g_flowdir]==0) ks=1;
  if(g_proc_coords[g_flowdir]==g_np_ntau-1) ke=g_nt_loc+1;

  starting_config(g_field);

  for(k=0;k<g_nt_loc+1;k++) for(i=0;i<g_size;i++) g_p[k][i]=0.0;
  for(k=0;k<g_nt_loc  ;k++) for(i=0;i<g_size;i++) g_r[k][i]=0.0;
  for(k=0;k<g_nt_loc  ;k++) for(i=0;i<g_size;i++) g_ap[k][i]=0.0;
  for(k=0;k<g_nt_loc+1;k++) for(i=0;i<g_size;i++) g_ar[k][i]=0.0;
  
  if(g_proc_coords[g_flowdir]==g_np_ntau-1)  projector(g_field[g_nt_loc],g_field[g_nt_loc],g_proj,0);

  if(g_np_ntau==1){
    for(h=0;h<g_nt_loc;h++) gauss_vectord(g_debug[h],g_size);
    load_store_config(3,g_debug,STORE);
  } else {
    load_store_config(3,g_debug,LOAD);
  }
  for(h=0;h<g_nt_loc;h++) for(i=0;i<g_size;i++) g_r[h][i]=g_debug[h][i];

  if(g_np_ntau==1){
    for(h=0;h<g_nt_loc+1;h++) gauss_vectord(g_debug[h],g_size);
    load_store_config(4,g_debug,STORE);
  } else {
    load_store_config(4,g_debug,LOAD);
  }
  for(h=0;h<g_nt_loc+1;h++) for(i=0;i<g_size;i++) g_p[h][i]=g_debug[h][i];

  matrix_noise_IMR_FE(g_ap,g_p,matdtau,g_field,0,g_nt_loc);
  matrixt_noise_IMR_FE(g_ar,g_r,matdtau,g_field,0,g_nt_loc);

  rAp=0.0;
  for(k=0;k<g_nt_loc;k++) for(i=0;i<g_size;i++) rAp += g_ap[k][i]*g_r[k][i];
  MPI_Allreduce(&rAp,&rAp,1,MPI_DOUBLE,MPI_SUM,g_cart);
  pAtr=0.0;
  for(k=0;k<g_nt_loc;k++) for(i=0;i<g_size;i++) pAtr += g_ar[k][i]*g_p[k][i];
  MPI_Allreduce(&pAtr,&pAtr,1,MPI_DOUBLE,MPI_SUM,g_cart);
  if(g_proc_id==0) fprintf(stdout,"NOISE FE: rAp = %15.14f, pAtr=%15.14f\n",rAp,pAtr);

  matrix_noise_IMR_FB(g_ap,g_p,matdtau,g_field,0,g_nt_loc);
  matrixt_noise_IMR_FB(g_ar,g_r,matdtau,g_field,0,g_nt_loc);

  jmax=2;
  for(pe=0; pe<g_np_ntau; pe++){
    MPI_Barrier(g_cart);
    if(g_proc_coords[g_flowdir]==pe){
      for(h=0;h<g_nt_loc;h++){
	for(j=0;j<jmax;j++){
	  fprintf(stdout,"r[%d][%d][%d]=%g\n",pe,h,j,g_r[h][j]);
	}
      }
    }
  }
  for(pe=0; pe<g_np_ntau; pe++){
    MPI_Barrier(g_cart);
    if(g_proc_coords[g_flowdir]==pe){
      for(h=0;h<g_nt_loc;h++){
	for(j=0;j<jmax;j++){
	  fprintf(stdout,"ap[%d][%d][%d]=%g\n",pe,h,j,g_ap[h][j]);
	}
      }
    }
  }
  for(pe=0; pe<g_np_ntau; pe++){
    MPI_Barrier(g_cart);
    if(g_proc_coords[g_flowdir]==pe){
      for(h=ks;h<ke;h++){
	for(j=0;j<jmax;j++){
	  fprintf(stdout,"p[%d][%d][%d]=%g\n",pe,h,j,g_p[h][j]);
	}
      }
    }
  }
  for(pe=0; pe<g_np_ntau; pe++){
    MPI_Barrier(g_cart);
    if(g_proc_coords[g_flowdir]==pe){
      for(h=ks;h<ke;h++){
	for(j=0;j<jmax;j++){
	  fprintf(stdout,"ar[%d][%d][%d]=%g\n",pe,h,j,g_ar[h][j]);
	}
      }
    }
  }

  
  rAp=0.0;
  for(k=0;k<g_nt_loc;k++) for(i=0;i<g_size;i++) rAp += g_ap[k][i]*g_r[k][i];
  MPI_Allreduce(&rAp,&rAp,1,MPI_DOUBLE,MPI_SUM,g_cart);
  pAtr=0.0;
  for(k=ks;k<ke;k++) for(i=0;i<g_size;i++) pAtr += g_ar[k][i]*g_p[k][i];
  MPI_Allreduce(&pAtr,&pAtr,1,MPI_DOUBLE,MPI_SUM,g_cart);
  if(g_proc_id==0) fprintf(stdout,"NOISE FB: rAp = %15.14f, pAtr=%15.14f\n",rAp,pAtr);
  
  MPI_Barrier(g_cart);

  /*** test At = transpose[A] for SD ***/

  if(g_proc_id==0) fprintf(stdout,"\n***\n test At = transpose[A] for SD.\n");

  for(k=0;k<g_nt_loc+1;k++) for(i=0;i<g_size;i++) g_p[k][i]=0.0;
  for(k=0;k<ext_ne_r  ;k++) for(i=0;i<sz[k/g_nt_loc];i++) g_r[k][i]=0.0;
  for(k=0;k<ext_ne_r  ;k++) for(i=0;i<sz[k/g_nt_loc];i++) g_ap[k][i]=0.0;
  for(k=0;k<g_nt_loc+1;k++) for(i=0;i<g_size;i++) g_ar[k][i]=0.0;

  if(g_np_ntau==1){
    for(h=0;h<g_nt_loc+1;h++) gauss_vectord(g_field[h],g_size);
    load_store_config(5,g_field,STORE);
  } else {
    load_store_config(5,g_field,LOAD);
  }

  if(g_proc_coords[g_flowdir]==g_np_ntau-1) projector(g_field[g_nt_loc],g_field[g_nt_loc],g_proj,0);

  if(g_np_ntau==1){
    for(h=0;h<g_nt_loc;h++) gauss_vectord(g_debug[h],g_size);
    load_store_config(6,g_debug,STORE);
  } else {
    load_store_config(6,g_debug,LOAD);
  }
  for(h=0;h<g_nt_loc;h++) for(i=0;i<g_size;i++) g_r[h][i]=g_debug[h][i];
  
  if(g_np_ntau==1){
    for(h=0;h<g_nt_loc+1;h++) gauss_vectord(g_debug[h],g_size);
    load_store_config(7,g_debug,STORE);
  } else {
    load_store_config(7,g_debug,LOAD);
  }
  for(h=0;h<g_nt_loc+1;h++) for(i=0;i<g_size;i++) g_p[h][i]=g_debug[h][i];

  matrix_SD_IMR(g_ap,g_p,matdtau,g_field,0,g_nt_loc);  // OK
  matrixt_SD_IMR(g_ar,g_r,matdtau,g_field,0,g_nt_loc);  // wrong O(dtau)
  
  rAp=0.0;
  for(k=0;k<ext_ne_r;k++) for(i=0;i<sz[k/g_nt_loc];i++) rAp += g_ap[k][i]*g_r[k][i];
  MPI_Allreduce(&rAp,&rAp,1,MPI_DOUBLE,MPI_SUM,g_cart);
  pAtr=0.0;
  for(k=0;k<g_nt_loc+1;k++) for(i=0;i<g_size;i++) pAtr += g_ar[k][i]*g_p[k][i];
  MPI_Allreduce(&pAtr,&pAtr,1,MPI_DOUBLE,MPI_SUM,g_cart);
  if(g_proc_id==0) fprintf(stdout,"SD: rAp = %15.14f, pAtr=%15.14f\n",rAp,pAtr);
  
  MPI_Barrier(g_cart);

  /*** test At = transpose[A] for noise (element by element...O(V^3) calculation) ***/
  
  if(all_AAT==1){
    fprintf(stdout,"\n***\n test At = transpose[A] for noise (element by element).\n");
    for(k=0;k<g_nt_tot+1;k++) for(i=0;i<g_size;i++) g_p[k][i]=0.0;
    for(k=0;k<g_nt_tot+1;k++) for(i=0;i<g_size;i++) g_r[k][i]=0.0;
    
    for(h=0;h<g_nt_tot;h++){
      for(j=0;j<g_size;j++){
	g_r[h][j]=1.0;
	matrixt_noise_IMR_FE(g_ar,g_r,0.1,g_field,0,g_nt_tot);
	for(k=0;k<g_nt_tot;k++){
	  for(i=0;i<g_size;i++){
	    g_p[k][i]=1.0;
	    matrix_noise_IMR_FE(g_ap,g_p,0.1,g_field,0,g_nt_tot);
	    if(fabs(g_ap[h][j]-g_ar[k][i]) > 1e-14){
	      fprintf(stdout,"A At (noise) mismatch: A_(%d,%d;%d,%d)=%g!=At_(%d,%d;%d,%d)=%g\n",
		      h,j,k,i,g_ap[h][j],k,i,h,j,g_ar[k][i]);
	    }
	    g_p[k][i]=0.0;
	  }
	}
	g_r[h][j]=0.0;
	fprintf(stdout,"A (%d,%d;:,:) = At(:,:;%d,%d) checked\n",h,j,h,j);
      }
    }
  }
  
  MPI_Barrier(g_cart);

  /*** compare FIELD evolved with euler / rk4 / CGNE ***/

  if(g_proc_id==0) fprintf(stdout,"\n***\n compare FIELD evolved with euler / rk4 / CGNE.\n");

  sz[0]=g_size;
  sz[1]=EXTRACOND;

  starting_config(g_field);

  /* select the highest one as starting point for BW int. */
  if(g_np_ntau==1){
    for(i=0;i<g_size;i++) g_field[g_nt_tot][i]=g_field[0][i];
  } else {
    if(g_proc_coords[g_flowdir]==0){
      MPI_Send(g_field[0], g_size, MPI_DOUBLE, g_neigh_node[g_flowdir][DN], 33, g_cart);
    }
    if(g_proc_coords[g_flowdir]==g_np_ntau-1){
      MPI_Recv(g_field[g_nt_loc], g_size, MPI_DOUBLE, g_neigh_node[g_flowdir][UP], 33, g_cart, &stat);
    }
  }

  ode_rk4(g_field,&force_model,0,g_nt_loc,1,g_dtau,-1,NULL,BW); /* SA rk4 */
  ode_rk4(g_field,&force_model,0,g_nt_loc,1,g_dtau,1,NULL,FW); /* SD */

  lastn2n=0;
  lastact=0;
  if(g_proc_coords[g_flowdir]==0) lastact=creal(action_model(g_field[0]));
  constraints(g_cnstr,g_field,g_field[0],lastn2n,lastact,g_dtau,g_nt_loc);
  delta=0.0;
  for(h=0;h<Nt_con;h++) for(j=0;j<sz[h/g_nt_loc];j++) delta+=g_cnstr[h][j]*g_cnstr[h][j];
  MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_cart);
  if(g_proc_id==0) fprintf(stdout,"SD constraint F(phi):%g\n",delta);

  for(h=0;h<Nt_deg;h++) for(j=0;j<g_size;j++) g_diff[h][j]=0.0;
  iter=cgne(g_diff,&matrix_SD_IMR,&matrixt_SD_IMR,g_cnstr,g_sd_cg_itermax,g_sd_cg_tol,g_field,
	    0,Nt_deg,0,g_nt_loc,EXTRACOND);
  if(g_proc_id==0) fprintf(stdout,"CG-iter-SD:%d\n",iter);

  for(pe=0; pe<g_np_ntau; pe++){
    if(g_proc_coords[g_flowdir]==pe){
      for(h=0;h<g_nt_loc+1;h+=10){
	delta=0.0;
	for(i=0;i<g_size;i++) delta+=fabs(g_diff[h][i]);
	MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
	if(g_sub_print_proc==1){
	  fprintf(stdout,"ODE SD RK4-CGNE difference: %15.14f, after %d steps (tau slice: %d), with dt=%g.\n",delta,h,pe,g_dtau);
	}
	delta=0.0;
	for(i=0;i<g_size;i++) delta+=fabs(g_field[h][i]-g_field[0][i]);
	MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
	if(g_sub_print_proc==1) fprintf(stdout,"ODE SD distance covered (per proc) with RK4 (tau slice: %d):"
					" %15.14f.\n",pe,delta);
      }
    }
  }
  matrix_SD_IMR(g_r,g_diff,g_dtau,g_field,0,g_nt_loc);

  for(h=0;h<Nt_con;h++) for(j=0;j<sz[h/g_nt_loc];j++) g_r[h][j]=g_r[h][j]-g_cnstr[h][j];

  delta=0.0;
  for(h=0;h<Nt_con;h++) for(j=0;j<sz[h/g_nt_loc];j++) delta+=g_r[h][j]*g_r[h][j];
  MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_cart);
  if(g_proc_id==0) fprintf(stdout,"SD:  || F' xi - F(phi) ||^2:%g\n",delta);


  MPI_Barrier(g_cart);

  /*** compare NOISE evolved with euler / rk4 / CGNE ***/

  if(g_proc_id==0) fprintf(stdout,"\n***\n compare NOISE evolved with euler / rk4 / CGNE.\n");

  ode_rk4(g_noise,&mhessian_model,0,g_nt_loc,1,g_dtau,0,g_field,FW); 
  for(h=0;h<g_nt_loc+1;h++)  for(i=0;i<g_size;i++) g_debug[h][i]=g_noise[h][i]; /* save the result */

  source_noise_IMR_FB(g_b,g_noise,g_field,g_dtau,0,g_nt_loc);
  matrix_noise_IMR_FB(g_r,g_noise,g_dtau,g_field,0,g_nt_loc);
  for(h=0;h<g_nt_loc;h++) for(j=0;j<g_size;j++) g_r[h][j]=g_b[h][j]-g_r[h][j];

  delta=0.0;
  for(h=0;h<g_nt_loc;h++) for(j=0;j<g_size;j++) delta+=g_r[h][j]*g_r[h][j];
  MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_cart);
  if(g_proc_id==0) fprintf(stdout,"|| M eta-b ||^2 (FB) =  %g.\n",delta);

  source_noise_IMR_FE(g_b,g_noise,g_field,g_dtau,0,g_nt_loc);
  matrix_noise_IMR_FE(g_ap,g_noise,g_dtau,g_field,0,g_nt_loc);
  for(h=0;h<g_nt_loc;h++) for(j=0;j<g_size;j++) g_ap[h][j]=g_b[h][j]-g_ap[h][j];

  delta=0.0;
  for(h=0;h<g_nt_loc;h++) for(j=0;j<g_size;j++) delta+=g_ap[h][j]*g_ap[h][j];
  MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_cart);
  if(g_proc_id==0) fprintf(stdout,"|| M eta-b ||^2 (FE) =  %g.\n",delta);
  
  source_noise_IMR_FB(g_b,g_noise,g_field,g_dtau,0,g_nt_loc);
  iter=cgne(g_noise,&matrix_noise_IMR_FB,&matrixt_noise_IMR_FB,g_b,g_noise_cg_itermax,g_noise_cg_tol,
	    g_field,1,g_nt_loc+1,0,g_nt_loc,0);
  if(g_proc_id==0) fprintf(stdout,"CG-iter-dn-TN:%d\n",iter);

  source_noise_IMR_FE(g_b,g_noise,g_field,g_dtau,0,g_nt_loc);
  iter=cgne(g_noise,&matrix_noise_IMR_FE,&matrixt_noise_IMR_FE,g_b,g_noise_cg_itermax,g_noise_cg_tol,
	    g_field,0,g_nt_loc,0,g_nt_loc,0);
  if(g_proc_id==0) fprintf(stdout,"CG-iter-up-TN:%d\n",iter);
  
  for(pe=0; pe<g_np_ntau; pe++){
    if(g_proc_coords[g_flowdir]==pe){
      for(h=0;h<g_nt_loc+1;h+=10){
	delta=0.0;
	for(i=0;i<g_size;i++) delta+=fabs(g_debug[h][i]-g_noise[h][i]);
	MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
	if(g_sub_print_proc==1) fprintf(stdout,"ODE noise RK4-CGNE difference: %15.14f, after %d steps"
					" (tau slice: %d), with dt=%g.\n",delta,h,pe,g_dtau);
	delta=0.0;
	for(i=0;i<g_size;i++) delta+=fabs(g_debug[h][i]-g_debug[0][i]);
	MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
	if(g_sub_print_proc==1) fprintf(stdout,"ODE noise distance covered (per proc) with RK4 (tau slice: %d):"
					" %15.14f.\n",pe,delta);
	delta=0.0;
	for(i=0;i<g_size;i++) delta+=fabs(g_noise[h][i]-g_noise[0][i]);
	MPI_Allreduce(&delta,&delta,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
	if(g_sub_print_proc==1) fprintf(stdout,"ODE noise distance covered (per proc) with CGNE (tau slice: %d):"
					" %15.14f.\n",pe,delta);
      }
    }
  }

  /***** end of tests *****/
  if(g_proc_id==0) fprintf(stdout,"\n\n GG test ends.\n");
  
}
#endif
