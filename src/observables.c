/*****************************************************************************
 file: observables.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <complex.h>
#include "global.h"
#include "bicomplex.h"
#include "init_model.h"
#include "action.h"
#include "observables.h"

/* 
   In this file we put observables that are defined for all models (very few actually)
*/

void multi_action_monitor(double **phi){

  int h,Nt;
  dcomplex cact;
  double *ract,*iact,*fulllist;
  
  Nt=g_nt_loc;
  if(g_proc_coords[g_flowdir]==0) Nt=g_nt_loc+1;

  if(g_proc_id==0) fulllist = (double*) calloc(g_nt_tot+1,sizeof(double));
  ract=(double*)calloc(Nt,sizeof(double));
  iact=(double*)calloc(Nt,sizeof(double));
  
  for(h=0;h<Nt;h++){
    cact=action_model(phi[h]);
    ract[h]=creal(cact);
    iact[h]=cimag(cact);
  }

  MPI_Gatherv(ract,Nt,MPI_DOUBLE,fulllist,g_ntloc_list,g_ntdisp_list,MPI_DOUBLE,0,g_print_comm);
  if(g_proc_id==0){
    fprintf(stdout,"real action monitor: ");
    for(h=0;h<g_nt_tot+1;h++) fprintf(stdout,"%g, ",fulllist[h]);
    fprintf(stdout,"\n");
  }
  
  MPI_Gatherv(iact,Nt,MPI_DOUBLE,fulllist,g_ntloc_list,g_ntdisp_list,MPI_DOUBLE,0,g_print_comm);
  if(g_proc_id==0){
    fprintf(stdout,"imag action monitor: ");
    for(h=0;h<g_nt_tot+1;h++) fprintf(stdout,"%g, ",fulllist[h]);
    fprintf(stdout,"\n");
  }

  if(g_proc_id==0) free(fulllist);
  free(ract);
  free(iact);
}
