/*****************************************************************************
 file: ferm_mhessian.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
 *******************************************************************************/

#ifndef _FERM_MHESSIAN_H
#define _FERM_MHESSIAN_H

#include "matrix_mult_typedef.h"

void ferm_mhessian_stoch(double * etap, double * eta, double * phi, int nt, 
			 matrix_mult_ferm M, matrix_mult_ferm Mt);
void ferm_mhessian_exact(double * etap, double * eta, double * phi);
void ferm_hessian_debug(int fl, int r, int nt);

#endif
