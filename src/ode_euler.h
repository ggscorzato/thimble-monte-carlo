/*****************************************************************************
 file: ode_euler.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _ODE_EULER_H
#define _ODE_EULER_H

#include "matrix_mult_typedef.h"

void ode_euler(double ** y, matrix_mult_ode A, int ns, int ne, int Nsub, double dt ,int opt, double **phi, int dir_flag);

#endif
