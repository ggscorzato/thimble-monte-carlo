/*****************************************************************************
 file: init_flow_index.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: December 2013
 *******************************************************************************/

#ifndef _INIT_FLOW_INDEX_H
#define _INIT_FLOW_INDEX_H

void init_flow_index();

#endif
