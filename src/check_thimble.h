/*****************************************************************************
 file: check_thimble.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: December 2013
 *******************************************************************************/

#ifndef _CHECK_THIMBLE_H
#define _CHECK_THIMBLE_H

void check_thimble(double SI);

#endif
