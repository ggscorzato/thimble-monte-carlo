/*****************************************************************************
 file: ferm_cgne.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include "global.h"
#include "matrix_mult_typedef.h"
#include "cgne.h"
#ifdef WITHFERM

/* 
  Solve with conjugate gradient normal equation - with error minimization (CGNE) the system Ax=b.  i.e. solve AA^t
  y = b and then set x=A^t y.  The code assumes to find the initial guess for the solution in x, where the result
  is also written.  Note that x,g_p,g_ar and b,g_r,g_ap have all length N.  See Saad Algorithm 8.5 (Craig's Method)
  in which y never appear.
*/

int ferm_cgne(dcomplex * x, matrix_mult_ferm A, matrix_mult_ferm At, dcomplex * b, int itermax, 
	      double tol, double *phi, int N, int fl){
  double alpha,beta,r2,p2,r2old;
  int it,i;
  
  /* 1. */
  A(g_cr,x,fl,1,phi);

  for(i=0;i<N;i++) g_cr[i]=b[i]-g_cr[i];

  r2=0.0;
  for(i=0;i<N;i++) r2+=g_cr[i]*conj(g_cr[i]);
  MPI_Allreduce(&r2,&r2,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
  if(g_debug_level>1) if(g_proc_id==0) fprintf(stdout,"CGF-res^2:%g\n",r2);
  if(r2 < tol) return 0;

  At(g_cp,g_cr,fl,1,phi);

  /* 2. */
  for(it=0;it<itermax;it++){
    p2=0.0;
    for(i=0;i<N;i++) p2+=g_cp[i]*conj(g_cp[i]);
    MPI_Allreduce(&p2,&p2,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);

    /* 3. */
    alpha=r2/p2;

    /* 4. */
    for(i=0;i<N;i++) x[i] += alpha*g_cp[i];

    /* 5. */
    A(g_cap,g_cp,fl,1,phi);
    for(i=0;i<N;i++) g_cr[i] -= alpha*g_cap[i];

    /* 6. */
    r2old=r2;
    r2=0.0;
    for(i=0;i<N;i++) r2+=g_cr[i]*conj(g_cr[i]);
    MPI_Allreduce(&r2,&r2,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
    beta=r2/r2old;

    if(r2 < tol) break;
    
    /* 7. */
    At(g_car,g_cr,fl,1,phi);
    for(i=0;i<N;i++) g_cp[i] = g_car[i] + beta*g_cp[i];

    if(g_debug_level>1) if(g_proc_id==0) fprintf(stdout,"CGF-res^2:%g\n",r2);
  }
  if((it==itermax) && (g_proc_id==0)) fprintf(stdout,"CGNEF WARNING: max iterations %d exceeded !!\n",itermax);
  return it;

}
#endif
