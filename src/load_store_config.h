/*****************************************************************************
 file: load_store_config.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _LOAD_STORE_CONFIG_H
#define _LOAD_STORE_CONFIG_H

void load_store_config(int cnum, double **phi, int ls_flag);
void load_store_test();
void load_store_toy(int cnum,  int ls_flag);

#endif
