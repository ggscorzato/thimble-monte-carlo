/*****************************************************************************
 file: ferm_fullmatrix_hub.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <complex.h>
#include "global.h"
#include "ferm_matrices.h"
#include "init_model.h"
#if (MODEL == HUB)

/* 
   Initialize the full CONJUGATE fermion matrix for the Hubbard model 
   (used only in case of exact inversion)
*/

#if (USEMAGMA == 0)
void ferm_init_full_mat_hub(){

  if(g_proc_id==0) fprintf(stdout,"Initializing the conjugate full fermion matrix for  HUBBARD...");

  int fl,k,kr,kr1,n,n2,s,rho;

  /*** init Conjugate Fermion Matrix ***/
  n=g_csize;
  n2=n*n;
  for(fl=0;fl<g_NF;fl++){
    for(k=0;k<n;k++){
      g_M[fl][k*n + k]=1.0;
      s=1;
      kr=g_ineigh[g_timedir][UP][k];
      if(kr<k) s=-1;  /* this happens when k is in the last timeslice */
      g_M[fl][k*n + kr] = -s*(1.0 + conj(g_udb*g_phi0) + g_Umu[fl]);
      for(rho=0;rho<DIM;rho++){
	if(rho!=g_timedir){
	  kr1=g_ineigh[rho][UP][kr];
	  g_M[fl][k*n + kr1]=+s*g_tdb;
	  kr1=g_ineigh[rho][DN][kr];
	  g_M[fl][k*n + kr1]=-s*g_tdb;
	}
      }
    }
  }
  if(g_proc_id==0) fprintf(stdout,"OK!\n");
}
#endif

/* 
   Update the full CONJUGATE fermion matrix for the Hubbard model 
   (used only in case of exact inversion)
*/

#if (USEMAGMA == 0)
void ferm_update_full_mat_hub(double *phi){

  int fl,k,kr,n,n2,s;

  n=g_csize;
  n2=n*n;
  for(fl=0;fl<g_NF;fl++){
    for(k=0;k<n;k++){
      kr=g_ineigh[g_timedir][UP][k];
      s=1;
      if(kr<k) s=-1;
      g_M[fl][k*n + kr] = -s*(1.0 + conj(g_udb*(phi[2*k]+I*phi[2*k+1])) + g_Umu[fl]);
    }
  }
}
#endif

/* 
   Print Matrix for testing purposes
*/

#if (USEMAGMA == 0)
void ferm_print_full_mat_hub(int fl){

  int h,k,n2;
  dcomplex ctmp;

  n2=g_csize*g_csize;

  fprintf(stdout,"full Matrix[fl=%d]:\n",fl);
  for(k=0;k<g_csize;k++){
    for(h=0;h<g_csize;h++){
      if(fl==-1){
	ctmp=g_A[g_csize*h+k];
      } else {
	ctmp=g_M[fl][g_csize*h+k];
      }
      // fprintf(stdout,"(%7.5f,%7.5f), ",creal(ctmp),cimag(ctmp));
      fprintf(stdout,"%16.14f + %16.14f*i, ",creal(ctmp),cimag(ctmp));
    }
    fprintf(stdout,";\n");
  }
}
#endif
#endif
