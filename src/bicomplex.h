/***********************************************************************
 file: bicomplex.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
*******************************************************************************/

#ifndef _BICOMPLEX_H
#define _BICOMPLEX_H

#include <complex.h>

typedef struct 
{
  dcomplex br, bi;
} bicomplex;

// r,s,t bicomplex; a,b,c complex; R real
#define _bic_assign(r,s)  \
  (r).br = (s).br;        \
  (r).bi = (s).bi;

#define _bic_add(r,s,t)	     \
  (r).br = (s).br + (t).br;  \
  (r).bi = (s).bi + (t).bi;

#define _bic_acc(r,s)  \
  (r).br += (s).br;        \
  (r).bi += (s).bi;

#define _bic_mul(r,s,t)	                        \
  (r).br = (s).br * (t).br - (s).bi * (t).bi;	\
  (r).bi = (s).bi * (t).br + (s).br * (t).bi;

#define _bic_smul(r,c,t)        \
  (r).br = (c) * (t).br;	\
  (r).bi = (c) * (t).bi;

#define _bic_scal_prod(c,r,s)               \
  (c)= (r).br * (s).br + (r).bi * (s).bi;

#define _bic_eps_prod(c,r,s)                \
  (c)= (r).br * (s).bi - (r).bi * (s).br;

#define _bic_norm_sq(R,r)                                        \
  (R)= cabs((r).br) * cabs((r).br)  + cabs((r).bi) * cabs((r).bi);


#endif
