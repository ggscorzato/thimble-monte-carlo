/*****************************************************************************
 file: residual_phase.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: December 2013
 *******************************************************************************/

#ifndef _RESIDUAL_PHASE_H
#define _RESIDUAL_PHASE_H

void residual_phase_stoch(int it);
void residual_phase_exact(int it);
void gen_thimb_tan_basis();

#endif
