/***********************************************************************
 file: thimble_monte_carlo.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
*******************************************************************************/

#define MAIN_PROGRAM
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <mpi.h>
#include <complex.h>
#include "global.h"
#include "ranlxd.h"
#include "read_input.h"
#include "init_geometry.h"
#include "init_flow_index.h"
#include "alloc_fields.h"
#include "init_model.h"
#include "search_min.h"
#include "projector.h"
#include "ferm_matrices.h"
#include "starting_config.h"
#include "steepest_descent.h"
#include "extract_transport_noise.h"
#include "ferm_extract_noise.h"
#include "langevin_step.h"
#include "accept_reject.h"
#include "action.h"
#include "observables.h"
#include "check_thimble.h"
#include "load_store_config.h"
#include "residual_phase.h"
#include "tests.h"
#if (USEMAGMA == 1)
#include "ferm_cuda.h"
#endif

int main(int argc,char **argv){
  int it,i,h,accept;
  double diff,iact,ract,n2n;
  dcomplex cact;
  
  /*** Initializations ***/

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &g_proc_id);
  MPI_Comm_size(MPI_COMM_WORLD, &g_nproc);

  /* magma */
#if (USEMAGMA ==1)
  magma_init();
#endif
  
  /* Read the input file */
  fflush(stdout); MPI_Barrier(MPI_COMM_WORLD);
  read_input("thimble.input");
  
  /* Initialize the random number generator */
  fflush(stdout); MPI_Barrier(MPI_COMM_WORLD);
  rlxd_init(1,g_seed+g_proc_id);
  
  /* Initialize the lattice indices */
  fflush(stdout); MPI_Barrier(MPI_COMM_WORLD);
  init_geometry();

  /* Initialize the parallelization along the flow direction */
  fflush(stdout); MPI_Barrier(g_cart);
  init_flow_index();

  /* Initialize model specific stuff (including setting the minimum) */
  fflush(stdout); MPI_Barrier(g_cart);
  init_model_model();

  /* Allocate the fields */
  fflush(stdout); MPI_Barrier(g_cart);
  alloc_fields(g_nt_loc,1);

  /* Initialize the fermion matrix */
#ifdef WITHFERM
  fflush(stdout); MPI_Barrier(g_cart);
  g_phi0=0.0;
  if(g_ferm_stoch==0) ferm_init_full_mat_model();
#endif

  /* set the minimum */
  fflush(stdout); MPI_Barrier(g_cart);
  g_phi0=0.0;
  g_phi0=search_min_model(g_phi0);

  /* Initialize the projector (currently done in all tau slices. 
     To save memory consider doing it only in the last) */
  fflush(stdout); MPI_Barrier(g_cart);
  init_projector_model(g_proj);

  /* Compute the action at the stationary point */
  fflush(stdout); MPI_Barrier(g_cart);
  if(g_proc_coords[g_flowdir] == 0){
    for(i=0;i<g_csize;i++){
      g_field[0][2*i  ]=creal(g_phi0);
      g_field[0][2*i+1]=cimag(g_phi0);
    }    
    g_S0=action_model(g_field[0]);
    if(g_proc_id==0) fprintf(stdout,"Value of the action at the stationary point S_0=(%g,%g)\n",
			     creal(g_S0),cimag(g_S0));
  }

  /* If required, perform some tests and exit */
  if(g_start==TESTONLY){
    fflush(stdout); MPI_Barrier(g_cart);
    tests_model();
    MPI_Finalize();
    return(0);
    exit(-3);
  }

  /* Initialize the fields */
  fflush(stdout); MPI_Barrier(g_cart);
  if(g_start==READIN){
    load_store_config(g_start_config_num,g_field,LOAD);
    if(g_proc_id==0) fprintf(stdout,"Configuration # %d loaded.\n",g_start_config_num);
  } else if(g_start==RANDOM){
    starting_config(g_field);
    if(g_proc_id==0) fprintf(stdout,"First configuration generated.\n");
  }

  /* construct the thimble tangent basis, if necessary */
  if(g_with_residual_phase && g_with_projected_mhessian && g_vol==g_tvol){
    gen_thimb_tan_basis();
  }

  /*** Main loop ***/

  fflush(stdout); MPI_Barrier(g_cart);
  if(g_proc_id==0) fprintf(stdout,"Main loop starts.\n");
  for(it = g_start_config_num + 1; it <= g_start_config_num + g_niter; it++){
    if(g_proc_id==0) fprintf(stdout,"Iteration %d starts.\n",it);

    if(g_start==MEASUREONLY){

      fflush(stdout); MPI_Barrier(g_cart);
      if(it % g_nskip == 0) load_store_config(it,g_field,LOAD);

    } else {
      
      /* extract noises */
#ifdef WITHFERM
      fflush(stdout); MPI_Barrier(g_cart);
      if(g_ferm_stoch==1) ferm_extract_noise();
#endif
      n2n=0;
      fflush(stdout); MPI_Barrier(g_cart);
      extract_transport_noise(g_noise, g_field, &n2n, 0, g_nt_loc);
      
      /* langevin step */
      fflush(stdout); MPI_Barrier(g_cart);
      langevin_step(g_field_new[0],g_field,g_noise);
      if(g_debug_level>1){
	  if(g_proc_coords[g_flowdir] == 0){
	    for(i=0;i<g_size;i++) g_ll[i]=g_field_new[0][i];
	  }
      }

      /* accept reject step */
      fflush(stdout); MPI_Barrier(g_cart);
      accept=accept_reject(g_field_new[0], g_field[0]);
      
      if(accept){
	
	/* correct the accepted new configuration to stay in the thimble */
	fflush(stdout); MPI_Barrier(g_cart);
	if(g_proc_coords[g_flowdir] == 0){
	  cact=action_model(g_field_new[0]);
	  ract=creal(cact);
	  iact=cimag(cact);
	  if(g_proc_id==0) fprintf(stdout,"action before correction : %g, %g.\n",ract,iact);
	}
	fflush(stdout); MPI_Barrier(g_cart);
	steepest_descent(g_field_new, g_field[0],ract,n2n);
	
	/* debug stuff */
	if(g_debug_level>1){
	  fflush(stdout); MPI_Barrier(g_cart);
	  if(g_proc_coords[g_flowdir] == 0){
	    diff=0.0;
	    for(i=0;i<g_size;i++) diff+=fabs(g_field[0][i]-g_field_new[0][i]);
	    if(g_proc_id==0) fprintf(stdout,"diff configs old-new (norm1)= %g; ",diff);
	    diff=0.0;
	    for(i=0;i<g_size;i++) diff+=fabs(g_ll[i]-g_field_new[0][i]);
	    if(g_proc_id==0) fprintf(stdout,"size CGNE-SD adjustment (norm1)= %g.\n",diff);
	  }
	  fflush(stdout); MPI_Barrier(g_cart);
	  multi_action_monitor(g_field_new);
	}
	
	/* copy back the new configuration (incl. borders along flow) */
	fflush(stdout); MPI_Barrier(g_cart);
	for(h=0;h<g_nt_loc+1;h++) for(i=0;i<g_size;i++) g_field[h][i]=g_field_new[h][i];
	
	/* extend the thimble if necessary */
	fflush(stdout); MPI_Barrier(g_cart);
	if(g_proc_coords[g_flowdir] == 0){
	  cact=action_model(g_field[0]);
	  ract=creal(cact);
	  iact=cimag(cact);
	  if(g_proc_id==0) fprintf(stdout,"action after correction : %g, %g.\n",ract,iact);
	}
	// check_thimble(iact); /* SEGNO: check thimble needs a better strategy with flow parallelization */
      }

      /* save the configuration */
      if( (it>g_ntherm) && (it % g_nskip == 0) ){
	fflush(stdout); MPI_Barrier(g_cart);
	load_store_config(it,g_field,STORE);
      }
    } // end of !MEASUREONLY

    /* measurements */
    fflush(stdout); MPI_Barrier(g_cart);
    if(g_proc_coords[g_flowdir] == 0){
      online_measurements_model(g_field[0]);
    }

    if((g_with_residual_phase) && (it>g_ntherm) && (it % g_nskip == 0)){
      if(g_with_projected_mhessian){
	residual_phase_exact(it);
      } else {
	residual_phase_stoch(it);
      }
    }
    fflush(stdout); MPI_Barrier(g_cart);
    if(g_proc_id==0) fprintf(stdout,"Iteration %d ends.\n",it);
  } // end of main loop

  /*** Finalize ***/
  finalize_model_model();
  MPI_Finalize();
  return(0);
}
