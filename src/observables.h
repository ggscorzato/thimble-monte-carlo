/*****************************************************************************
 file: observables.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _OBSERVABLES_H
#define _OBSERVABLES_H

dcomplex density_model(double *phi);
dcomplex phi2_model(double *phi);
void online_measurements_model(double *phi);
void multi_action_monitor(double **phi);

#endif
