/*****************************************************************************
 file: ferm_mhessian_stoch.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "cgne.h"
#include "ferm_matrices.h"
#include "ferm_mhessian.h"
#ifdef WITHFERM

/*
  Compute (stochastically) the fermionic part of the -hessian acting on eta, accumulate the result on etap.
  etap += - conj(Hess_F[M,phi] eta )
  M is the conjugate fermion matrix FM
  Mt is the transposed fermion matrix FM
  phi is the field
 */

void ferm_mhessian_stoch(double * etap, double * eta, double * phi, int nt, 
			 matrix_mult_ferm M, matrix_mult_ferm Mt){

  int r,fl,j,iter;
  double iNR=1./((double)g_NR);

  for(r=0;r<g_NR;r++){
    for(fl=0;fl<g_NF;fl++){

      /* find pb0 = xi FM^-1  (if no similar solution is available use the one at nt-1) */
      if(g_pb0[fl][r][nt][0]==0 && nt!=0) for(j=0;j<g_csize;j++) g_pb0[fl][r][nt][j] = g_pb0[fl][r][nt-1][j];
      iter=ferm_cgne(g_pb0[fl][r][nt],Mt,M,g_xi[r],g_noise_cgf_itermax,g_noise_cgf_tol,phi,g_csize,fl);

      /* aux_k = conj( eta_j (d_j FM)_kl )  xi_l */
      for(j=0;j<g_csize;j++) g_aux[j]=0.0;
      ferm_cntr13_dM_stoch_model(g_aux,eta,g_xi[r],1.0);

      //ferm_hessian_debug(fl,r,nt);

      /* find pb2 = conj(FM^-1) aux (if no similar solution is available use the one at nt-1) */
      if(g_pb2[fl][r][nt][0]==0 && nt!=0) for(j=0;j<g_csize;j++) g_pb2[fl][r][nt][j] = g_pb2[fl][r][nt-1][j];
      iter=ferm_cgne(g_pb2[fl][r][nt],M,Mt,g_aux,g_noise_cgf_itermax,g_noise_cgf_tol,phi,g_csize,fl);

      /* etap_k += - conj( [xi (FM^-1 d_k FM FM^-1 d_j FM) xi] eta_j ) = - conj(pb0 d_k FM FM^-1) aux = 
	 = - (conj( pb0 d_k FM) pb2) */
      ferm_cntr23_dM_stoch_model(etap,g_pb0[fl][r][nt],g_pb2[fl][r][nt],-iNR);
    
      /* etap_k += conj( [xi (FM^-1 d_k d_h FM) xi] eta_h ) = [conj(pb0 d_k d_h FM) xi] conj(eta_h) */
      ferm_cntr_ddM_stoch_model(etap,g_pb0[fl][r][nt],g_xi[r],eta,iNR);
    }
  }
}

#if (USEMAGMA == 0)
void ferm_hessian_debug(int fl, int r, int nt){
  int i;
  double delta;
  dcomplex ctmp;

  fprintf(stdout,"cfr inv Mat[%d,%d,%d]\n",fl,r,nt);
  for(i=0;i<g_csize;i++){
    ctmp=conj(g_pb0[fl][r][nt][i])/sqrt((double)g_NR);
    delta=cabs(ctmp-g_A[i*g_csize+r]);
    if(delta>1e-15) fprintf(stdout,"diff cgne vs full inv Mat[%d,%d]=%g, [(%g+i*%g)!=(%g+i*%g)]\n",
			    r,i,delta,creal(ctmp),cimag(ctmp),creal(g_A[i*g_csize+r]),cimag(g_A[i*g_csize+r]));
  }
  for(i=0;i<g_csize;i++) fprintf(stdout,"aux[%d]=(%g,%g)\n",i,creal(g_aux[i]),cimag(g_aux[i]));

}
#endif
#endif
