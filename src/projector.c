/******************************************************************************
 file: projector.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: August 2013
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <math.h>
#include <mpi.h>
#include <ggfft.h>
#include "global.h"
#include "init_geometry.h"
#include "init_model.h"
#include "mhessian.h"
#include "projector.h"

/* 
   This routine applies the projector defined by positive eigenvalues of the Hessian computed in the costant field
   phi=g_phi0. It is model independent, because the model dependence is in the initialization of proj.
   Input: proj (contains the projector, assumed to be diagonal in momentum space)
   Input: in   (vector of size: g_size reals),
   Output: out (vector of size: g_size reals).
   in and out can be the same pointer, to overwrite in.

*/

void projector(double * out, double * in, double *** proj, int field_flag){

  int j,h,k,p,sm;
  double psi_in[DPMP], psi_out[DPS], invvol;
  invvol=(double)(1.0/g_vol);

  /*** If 'in' is a field (rather than a vector) subtract the constant component ***/
  if(field_flag){
    for(j=0;j<g_size;j++) in[j] -= g_stat_conf[j];
  }


  /*** Put the content of the "in" variable (without the borders) in the input for the fft and execute the forward
     FFT (this is done twice to have the momenta -p on the same process as p.  I should modify the way in which the
     fftw-transpose distributes the data to avoid this double fft) ***/

  for(j=0;j<g_vol*CPS;j++){
    g_fftf[j] = in[j*2] + I * in[j*2+1];
    g_fftb[j] = g_fftf[j];
  }  

  gg_distributed_multidim_fft(&g_plan,-1,g_fftf);
  gg_distributed_multidim_fft(&g_plan, 1,g_fftb);

  /*** Compute the projection (almost diagonal in momenta) ***/
  for(p=0;p<g_vol;p++){

    sm=g_samemom[p];
    
    if(!sm){  /* p!=-p */
      for(h=0;h<CPS;h++){
	psi_in[2*h  ]=creal(g_fftf[p*CPS+h]);
	psi_in[2*h+1]=cimag(g_fftf[p*CPS+h]);
	psi_in[DPS+2*h  ]=creal(g_fftb[p*CPS+h]);
	psi_in[DPS+2*h+1]=cimag(g_fftb[p*CPS+h]);
      }
      
      for(h=0;h<DPS;h++){ /* dim-domain: 2mom*4bic. dim-image: 1mom*4bic. overall rank: 4 */
	psi_out[h]=0;
	for(k=0;k<DPMP;k++){
	  psi_out[h]+=proj[p][h][k]*psi_in[k];
	}
      }
      
    } else {  /* p=-p */
      for(h=0;h<CPS;h++){
	psi_in[2*h  ]=creal(g_fftf[p*CPS+h]);
	psi_in[2*h+1]=cimag(g_fftf[p*CPS+h]);
      }
      
      for(h=0;h<DPS;h++){ /* dim-domain: 1mom*4bic. dim-image: 1mom*4bic. rank: 2 */
	psi_out[h]=0;
	for(k=0;k<DPS;k++){
	  psi_out[h]+=proj[p][h][k]*psi_in[k];
	}
      }	  
    }
    for(h=0;h<CPS;h++){
      g_fftf[p*CPS+h]=psi_out[2*h] + I * psi_out[2*h+1];
    }
  }
  
  /*** Backward FFT (one is sufficient) and put the result in the out variable ***/
  
  for(p=0;p<g_vol*CPS;p++) g_fftf[p]*=invvol;
  
  gg_distributed_multidim_fft(&g_plan,1,g_fftf);

  for(j=0;j<g_vol;j++){
    for(h=0;h<CPS;h++){
      out[j*DPS+2*h  ] = creal(g_fftf[j*CPS+h]);
      out[j*DPS+2*h+1] = cimag(g_fftf[j*CPS+h]);
    }
  }

  /*** If 'in & out' are fields (rather than vectors) add back their constant component ***/
  if(field_flag){
    for(j=0;j<g_size;j++) out[j] += g_stat_conf[j];
    if(in!=out) for(j=0;j<g_size;j++) in[j] += g_stat_conf[j];
  }

}

/* 
   This routine computes the projector by numerically diagonalizing the hessian in a given field configuation in
   momentum space.  It assumes that the hessian can be diagonalized in momentum space, apart from this, it is model
   independent.  This routines is more expensive and less precise than init_projector_model and it is currently
   used only as a test of the procedure based on init_projector_model, contained in projector_model.c.
   Moreover, IT IS CORRECT ONLY IN THE SERIAL CASE.
*/
void init_projector_from_hessian(double **phi, double *** proj){
  
  int j,h,k,l,p,a0,a1,a2,b0,b1,b2,sm;
  double invvol, tmp;

  /* Lapack variables */
  double AA[DPMP*DPMP], WORK[4*DPMP], WW[DPMP];
  int NND, NNS, LDAD, LDAS, LWORK, INFO, len_jobz, len_uplo;
  NND=DPMP; NNS=DPS; LDAD=DPMP; LDAS=DPS; LWORK=3*DPMP; len_jobz=1; len_uplo=1;

  invvol=(double)(1.0/g_vol);

  /* main loop over momenta */
  for(p=0;p<g_vol;p++){

    sm=g_samemom[p];

    if(!sm){ 	/* case p != -p */	
      for(a0=0;a0<2;a0++){ /* mom=p,-p */ 
	for(a1=0;a1<CPS;a1++){ /* complex per site */
	  for(a2=0;a2< 2;a2++){ /* reals per complex */

	    /* only momentum component p (resp.) -p is set non-zero */
	    for(j=0;j<g_vol*CPS;j++) g_fftf[j]=0.;
	    g_fftf[p*CPS+a1]=(dcomplex) ((1-a2)*1. + I*a2);
	  
	    /* anti-fft */
	    gg_distributed_multidim_fft(&g_plan,(1-2*a0),g_fftf);

	    /* copy to kk vector */
	    for(j=0;j<g_vol;j++){
	      for(h=0;h<CPS;h++){
		g_kk[j*DPS+2*h  ]=creal(g_fftf[j*CPS+h]);
		g_kk[j*DPS+2*h+1]=cimag(g_fftf[j*CPS+h]);
	      }
	    }

	    /* apply Hessian */
	    mhessian_model(g_ff,g_kk,phi,0,0,0,0);

	    for(b0=0;b0<2;b0++){ /* mom=p,-p */

	      /* copy back to fftf vector */
	      for(j=0;j<g_vol*CPS;j++){
		g_fftf[j]=g_ff[j*2] + I * g_ff[j*2+1];
	      }

	      /* fft */
	      gg_distributed_multidim_fft(&g_plan,(2*b0-1),g_fftf);
	      for(j=0;j<g_vol*CPS;j++) g_fftf[j]*=invvol;

	      for(b1=0;b1<CPS;b1++){ /* complex per site */
		for(b2=0;b2<2;b2++){ /* doubles per complex */
		  tmp=(1-b2)*creal(g_fftf[p*CPS+b1]) + b2*cimag(g_fftf[p*CPS+b1]);
		  AA[((((a0*CPS+a1)*2+a2)*2+b0)*CPS+b1)*2+b2]=tmp;
		} // b2
	      } // b1
	    } // b0
	  } // a2
	} // a1
      } // a0

      dsyev_("V", "U", &NND, AA, &LDAD, WW, WORK, &LWORK, &INFO, len_jobz, len_uplo);
      if(INFO!=0){
	if(g_proc_id==0) fprintf(stderr,"dsyev ERROR");
	MPI_Abort(MPI_COMM_WORLD,-13);
	exit(-14);
      }
      
      for(h=0;h<DPS;h++){
	for(k=0;k<DPMP;k++){
	  proj[p][h][k]=0.;
	  for(l=0;l<DPMP;l++){
	    if(WW[l]<0) proj[p][h][k]+=AA[l*DPMP+h]*AA[l*DPMP+k];
	  }
	}
      }
    } else if(sm){

      for(a1=0;a1<CPS;a1++){ /* complex per site */
	for(a2=0;a2<2;a2++){ /* doubles per complex */
	  
	  /* only momentum component p (resp.) -p is set non-zero */
	  for(j=0;j<g_vol*CPS;j++) g_fftf[j]=0.;
	  g_fftf[p*CPS+a1]=(dcomplex) ((1-a2)*1. + I*a2);
	    
	  /* anti-fft */
	  gg_distributed_multidim_fft(&g_plan, 1,g_fftf);

	  /* copy to kk vector */
	  for(j=0;j<g_vol;j++){
	    for(h=0;h<CPS;h++){
	      g_kk[j*DPS+2*h  ]=creal(g_fftf[j*CPS+h]);
	      g_kk[j*DPS+2*h+1]=cimag(g_fftf[j*CPS+h]);
	    }
	  }

	  /* apply Hessian */
	  mhessian_model(g_ff,g_kk,phi,0,0,0,0);

	  /* copy back to fftf vector */
	  for(j=0;j<g_vol;j++){
	    for(h=0;h<CPS;h++){
	      g_fftf[j*CPS+h]=g_ff[j*DPS+2*h] + I * g_ff[j*DPS+2*h+1];
	    }
	  }
	    
	  /* fft */
	  gg_distributed_multidim_fft(&g_plan,-1,g_fftf);
	  for(j=0;j<g_vol*CPS;j++) g_fftf[j]*=invvol;
	  
	  for(b1=0;b1<CPS;b1++){ /* complex per site */
	    for(b2=0;b2<2;b2++){ /* doubles per complex */
	      tmp=(1-b2)*creal(g_fftf[p*CPS+b1]) + b2*cimag(g_fftf[p*CPS+b1]);
	      AA[((a1*2+a2)*CPS+b1)*2+b2]=tmp;
	    } // b2
	  } // b1
	} // a2
      } // a1
      
      dsyev_("V", "U", &NNS, AA, &LDAS, WW, WORK, &LWORK, &INFO, len_jobz, len_uplo);
      if(INFO!=0){
	if(g_proc_id==0) fprintf(stderr,"dsyev ERROR");
	MPI_Abort(MPI_COMM_WORLD,-13);
	exit(-14);
      }

      for(h=0;h<DPS;h++){
	for(k=0;k<DPS;k++){
	  proj[p][h][k]=0.;
	  for(l=0;l<DPS;l++){
	    if(WW[l]<0) proj[p][h][k]+=AA[l*DPS+h]*AA[l*DPS+k];
	  }
	}
      }
    } // sm
  } // p
}
