/*****************************************************************************
 file: action_hub.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: December 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <complex.h>
#include "global.h"
#include "init_model.h"
#include "ferm_action.h"
#include "ferm_matrices.h"
#include "action.h"
#if (MODEL == HUB)

dcomplex action_hub(double *phi){

  int j;
  double twopi=2*3.141592653589793;
  dcomplex S,ctmp;
  
  S=0;
  for(j=0;j<g_vol;j++){
    ctmp=phi[j*2]   + I * phi[j*2+1];
    ctmp=ctmp*ctmp;
    S+=0.5*g_dbeta*ctmp;
  }

  if(g_ferm_stoch==1){
    ctmp = ferm_action_stoch(&ferm_matrix_hub,phi);
  } else {
    ctmp = ferm_action_exact(phi);
  }
  S += ctmp;

  MPI_Allreduce(&S,&S,1,MPI_DOUBLE_COMPLEX,MPI_SUM,g_sub_cart);

  S= creal(S) + I * fmod(cimag(g_S0),twopi);

  return S;
}
#endif
