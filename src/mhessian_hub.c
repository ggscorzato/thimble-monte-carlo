/*****************************************************************************
 file: mhessian_hub.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: August 2013
 *******************************************************************************/

#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "bicomplex.h"
#include "xchange_fields.h"
#include "init_model.h"
#include "ferm_matrices.h"
#include "ferm_mhessian.h"
#include "mhessian.h"
#if (MODEL == HUB)

/* 
   -Hessian matrix H = -ddS_R
   etap  = acc*etap + H(phi[n_tau],phi[n_tau pm_half]) * eta
   See file: ActionDerivatives.pdf 
   Iwa is currently not used. Keep it as dumb index to match the same function prototype of as force_model()
*/

void mhessian_hub(double *etap, double *eta, double **phi, int n_tau, int pm_half, int acc, int Iwa){

  int j;

  /*** Exchange the boundaries (always at the start of the routine that uses them). Needed in fermionic part. ***/
  xchange_fields(eta);

  if(pm_half==1){
    for(j=0;j<g_size;j++) g_phi_mid[j]=0.5*(phi[n_tau][j]+phi[n_tau+1][j]);
  } else if(pm_half==-1){
    for(j=0;j<g_size;j++) g_phi_mid[j]=0.5*(phi[n_tau][j]+phi[n_tau-1][j]);
  } else {
    for(j=0;j<g_size;j++) g_phi_mid[j]=phi[n_tau][j];
  }
  xchange_fields(g_phi_mid);

  /*** set the output to zero, if accumulation is not wanted ***/
  if(!acc) for(j=0;j<g_size;j++) etap[j]=0;

  /*** bosonic part ***/
  for(j=0;j<g_vol;j++){
    /* etap = - H eta = - [Re[H], 0 ; 0, -Re[H]] eta */  
    etap[2*j  ] -= g_dbeta*eta[2*j  ];
    etap[2*j+1] += g_dbeta*eta[2*j+1];
  }

  /*** fermionic part ***/
  if(g_ferm_stoch==1) {
    ferm_mhessian_stoch(etap,eta,g_phi_mid,n_tau,&ferm_matrix_hub,&ferm_matrixt_hub);
  } else {
    ferm_mhessian_exact(etap,eta,g_phi_mid);
  }
}
#endif
