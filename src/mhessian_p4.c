/*****************************************************************************
 file: mhessian_p4.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "bicomplex.h"
#include "xchange_fields.h"
#include "init_model.h"
#include "mhessian.h"
#if (MODEL == P4)

/* 
   -Hessian matrix H = -ddS_R
   etap  = acc*etap + H(phi[n_tau],(phi[n_tau +/-1])) * eta
   See file: ActionDerivatives.pdf 
   Iwa is currently not used. Keep it as dumb index to match the same function prototype of as force_model()
*/

void mhessian_p4(double *etap, double *eta, double **phi, int n_tau, int pm_half, int acc, int Iwa){

  int a,b,A,B,j,k,ej,ek,gj,gk,mu,ver;
  double dm,chmu,shmu,sign,Hessd[DPS][DPS], Hesspm[2][DIM][DPS][DPS];
  dcomplex cHess[CPS][CPS], c0, c1, ctmp;
  bicomplex field;

  dm=2*DIM + g_mass*g_mass; /* This does not contribute in the Iwasawa case */

  /*** Exchange the boundaries (always at the start of the routine that uses them) ***/
  xchange_fields(eta); /* boundaries of phi are not needed */
  
  /*** set the output to zero, if accumulation is not wanted ***/
  if(!acc) for(j=0;j<g_size;j++) etap[j]=0;

  /*** Compute the +/-mu off-diag Hessian terms (outside the main loop, since they don't depend on the fields) ***/
  for(mu=0;mu<DIM;mu++){
    if(mu==g_timedir){
      chmu=cosh(g_chempot);
      shmu=sinh(g_chempot);
    }else{
      chmu=1;
      shmu=0;
    }
    for(ver=0;ver<2;ver++){
      for(a=0;a<CPS;a++){
	for(b=0;b<CPS;b++){
	  if(a==b){
	    ctmp=-chmu;
	  } else {
	    ctmp=(a-b)*(2*ver-1)*I*shmu; /* epsilon_{a,b}=(a-b)={{0,-1;1,0}}} */
	  }
	  Hesspm[ver][mu][a*CPS  ][b*CPS  ] = creal(ctmp);
	  Hesspm[ver][mu][a*CPS  ][b*CPS+1] =-cimag(ctmp);
	  Hesspm[ver][mu][a*CPS+1][b*CPS  ] =-cimag(ctmp);
	  Hesspm[ver][mu][a*CPS+1][b*CPS+1] =-creal(ctmp);
	}
      }
    }
  }

  /**** main loop ****/
  for(j=0;j<g_vol;j++){
    gj=g_lid[j];

    /** select the fields that contribute **/
    field.br=phi[n_tau][j*DPS]   + I * phi[n_tau][j*DPS+1];
    field.bi=phi[n_tau][j*DPS+2] + I * phi[n_tau][j*DPS+3];
    if(pm_half==1){
      field.br = 0.5*(field.br + phi[n_tau+1][j*DPS]   + I * phi[n_tau+1][j*DPS+1]);
      field.bi = 0.5*(field.bi + phi[n_tau+1][j*DPS+2] + I * phi[n_tau+1][j*DPS+3]);
    }
    if(pm_half==-1){
      field.br = 0.5*(field.br + phi[n_tau-1][j*DPS]   + I * phi[n_tau-1][j*DPS+1]);
      field.bi = 0.5*(field.bi + phi[n_tau-1][j*DPS+2] + I * phi[n_tau-1][j*DPS+3]);
    }

    /** Build the xy-diagonal part of the hessian (the only one that depends on the fields) **/
    c0=field.br*field.br;
    c1=field.bi*field.bi;
    cHess[0][0]=g_lambda*(3*c0 + c1) + dm;
    cHess[1][1]=g_lambda*(c0 + 3*c1) + dm;
    cHess[0][1]=g_lambda*(2.0*field.br*field.bi);
    cHess[1][0]=cHess[0][1];
    for(a=0;a<CPS;a++){
      for(b=0;b<CPS;b++){
	Hessd[a*CPS  ][b*CPS  ]= creal(cHess[a][b]);
	Hessd[a*CPS  ][b*CPS+1]=-cimag(cHess[a][b]);
	Hessd[a*CPS+1][b*CPS  ]=-cimag(cHess[a][b]);
	Hessd[a*CPS+1][b*CPS+1]=-creal(cHess[a][b]);
      }
    }
    if(Iwa){ /* Iwasawa enters here: -1 if the input index (=column=B) is lower than the output index (=raw=A) */
      for(A=0;A<DPS;A++){
	Hessd[A][A]=0.0;
	for(B=0;B<A;B++){
	  Hessd[A][B]*=-1.0;
	}
      }
    }

    /** Compute output etap **/
    for(A=0;A<DPS;A++){
      ej=j*DPS+A;
      for(B=0;B<DPS;B++){
	ek=j*DPS+B;
	etap[ej]-=Hessd[A][B] * eta[ek];  /* Diagonal part in xy */ 
      }
    }
    for(A=0;A<DPS;A++){
      ej=j*DPS+A;
      gj=A*g_tvol+gj;   /* [debug] this is done only to reproduce Marco Iwasawa */
      for(mu=0;mu<DIM;mu++){
	for(ver=0;ver<2;ver++){
	  k=g_ineigh[mu][ver][j];
	  gk=g_lid[k];
	  for(B=0;B<DPS;B++){
	    ek=k*DPS+B;
	    gk=B*g_tvol+gk;    /* [debug] this is done only to reproduce Marco Iwasawa */
	    sign=1.0;
	    if(Iwa && (gk<gj)) sign=-1.0;   /* Iwasawa enters here (bring it outside this loop, if you suppress [debug]) */
	    etap[ej]-=sign*Hesspm[ver][mu][A][B] * eta[ek];  /* Off-diag part in xy */
	  }
	}
      }
    }
  }
}

#endif
