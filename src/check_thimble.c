/*****************************************************************************
 file: check_thimble.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <complex.h>
#include "global.h"
#include "alloc_fields.h"
#include "check_thimble.h"

/* 
   This routine checks the imaginary part of the action.
   If it is larger than a threshold, it increases g_nt_tot,
   and fills the last field slice with the stationary field.
*/

void check_thimble(double SI){
  
  int i,ntmin=10;

  if(fabs(SI-cimag(g_S0)) > g_si_tol){
    g_nt_tot++;
    alloc_fields(g_nt_tot,0); /* realloc */
    for(i=0;i<g_size;i++) g_field[g_nt_tot][i]=g_stat_conf[i];

    if(g_debug_level>0) if(g_proc_id==0) fprintf(stdout,"Updated N_tau=%d\n",g_nt_tot); 
  } else if((fabs(SI-cimag(g_S0)) < 0.0* g_si_tol) && (g_nt_tot > ntmin) ){
    g_nt_tot--;
    alloc_fields(g_nt_tot,0); /* realloc */
    if(g_debug_level>0) if(g_proc_id==0) fprintf(stdout,"Updated N_tau=%d\n",g_nt_tot); 
  }
}

