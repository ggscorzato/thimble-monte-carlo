/*****************************************************************************
 file: xchange_fields_flow.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: July 2014
 *******************************************************************************/

#include <stdlib.h>
#include <complex.h>
#include <mpi.h>
#include "global.h"
#include "xchange_fields.h"

/* exchanges the fields in the flow direction. 
   phi: fields to be exchanged
   everyone, but last proc, owns phi[0]...phi[g_nt_loc-1];
   last proc owns                phi[0]...phi[g_nt_loc];
   everyone needs                phi[0]...phi[g_nt_loc]; (where, phi[g_nt_loc] = phi[0]^(next) )
*/
void xchange_fields_flow_up(double ** const phi){

  MPI_Status status[2];
  MPI_Request req[2];
  int j=0;

  if(g_proc_coords[g_flowdir] > 0){
    MPI_Isend(phi[0],g_size,MPI_DOUBLE,g_neigh_node[g_flowdir][DN],1753,g_cart,&req[j]);
    j++;
  }
  if(g_proc_coords[g_flowdir] < g_np_ntau-1){
    MPI_Irecv(phi[g_nt_loc],g_size,MPI_DOUBLE,g_neigh_node[g_flowdir][UP],1753,g_cart,&req[j]);
    j++;
  }
  MPI_Waitall(j,req,status);
}

void xchange_fields_flow_dn(double ** const phi){

  MPI_Status status[2];
  MPI_Request req[2];
  int j=0,m1=g_nt_loc;

  if(g_proc_coords[g_flowdir] < g_np_ntau-1){
    MPI_Isend(phi[g_nt_loc-1],g_size,MPI_DOUBLE,g_neigh_node[g_flowdir][UP],1759,g_cart,&req[j]);
    j++;
  }
  if(g_proc_coords[g_flowdir] > 0){
    MPI_Irecv(phi[m1],g_size,MPI_DOUBLE,g_neigh_node[g_flowdir][DN],1759,g_cart,&req[j]);
    j++;
  }
  MPI_Waitall(j,req,status);
}
