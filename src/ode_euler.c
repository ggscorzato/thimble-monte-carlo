/*****************************************************************************
 file: ode_euler.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "global.h"
#include "matrix_mult_typedef.h"
#include "ode_euler.h"

/* 
   ODE integrator with Euler method.
   y[ns/ne][*]: (input) contains the inital data. 
   y[*][*]: (output) contains the solution.
   phi: (input) contains the fields that may enter in the matrix A. If they do not, use NULL pointer.
   A: (input) operator that produces the force for the ODE.
   ns: local lower index (starting if dir_flag=FW)
   ne: local higher index (starting if dir_flag=BW)
   (Nt=ne-ns: number of integration step. Should not exceed what is allocated for y (and phi, if needed).)
   dt: (input) integration step.
   opt: (input) parameter to be passed to A (e.g. Iwasawa for hessian or sign for force (A must accept one option)).
   dir_flag: (input) FW/BW direction of integration (FW:y[ns]->y[ne]; BW:y[ne]->y[ns]) (g_field[g_nt_tot] is closer to the min.)
*/
void ode_euler(double ** y, matrix_mult_ode A, int ns, int ne, int Nsub, double dt, int opt, double **phi, int dir_flag){

  int n,m,j,*q,sign,i0,i1,Nt;
  double dts;
  MPI_Status status;
  dts=dt/Nsub;

  /* if parallele along flow, do one by one ... */
  if(g_np_ntau>1){
    if(dir_flag==FW){
      if(g_proc_coords[g_flowdir] > g_pc_flow_1st){
	MPI_Recv(y[0], g_size, MPI_DOUBLE, g_neigh_node[g_flowdir][DN], 1103, g_cart, &status);
      }
    } else if (dir_flag==BW){
      if(g_proc_coords[g_flowdir] < g_pc_flow_last){
	MPI_Recv(y[g_nt_loc], g_size, MPI_DOUBLE, g_neigh_node[g_flowdir][UP], 1109, g_cart, &status);
      }
    }
  }

  if((Nsub != 1) && (phi != NULL)){
    if(g_proc_id==0) fprintf(stderr,"ODE ERROR: finer step possible only if phi==NULL\n");
    exit(-100);
  }
  
  /* set up indices for FW / BW */  
  Nt=ne-ns;
  q=malloc((Nt+1)*sizeof(int));
  if(dir_flag==FW){
    for(n=0;n<Nt+1;n++) q[n]=ns+n;
    sign=1;
    i0=0;i1=1;
  } else if(dir_flag==BW){
    for(n=0;n<Nt+1;n++) q[n]=ne-n;
    sign=-1;
    i0=1;i1=0;
  }

  if(Nsub==1){
    for(n=0;n<Nt;n++){
      /* kk = F(y_n, t_n ) */
      A(g_kk,y[q[n]],phi,q[n],PLUSZERO,0,opt);
      /* y_n+1 = y_n + kk dt */
      for(j=0;j<g_size;j++) y[q[n+1]][j]=y[q[n]][j] + sign*dts*g_kk[j];
    }
  } else {
    for(n=0;n<Nt;n++){
      for(j=0;j<g_size;j++) g_yy[i0][j] = y[q[n]][j];
      for(m=0;m<Nsub;m++){
	/* kk = F(y_n, t_n ) */
	A(g_kk,g_yy[i0],phi,i0,PLUSZERO,0,opt);
	/* y_n+1 = y_n + kk dt */
	for(j=0;j<g_size;j++) g_yy[i1][j] = g_yy[i0][j] + sign*dts*g_kk[j];
	/* new starting point */
	for(j=0;j<g_size;j++) g_yy[i0][j] = g_yy[i1][j];
      }
      for(j=0;j<g_size;j++) y[q[n+1]][j] = g_yy[i1][j];
    }
  }

  /* if parallele along flow, do one by one ... */
  if(g_np_ntau>1){
    if(dir_flag==FW){
      if(g_proc_coords[g_flowdir] < g_pc_flow_last){
	MPI_Send(y[g_nt_loc], g_size, MPI_DOUBLE, g_neigh_node[g_flowdir][UP], 1103, g_cart);
      }
    } else if (dir_flag==BW){
      if(g_proc_coords[g_flowdir] > g_pc_flow_1st){
	MPI_Send(y[0], g_size, MPI_DOUBLE, g_neigh_node[g_flowdir][DN], 1109, g_cart);
      }
    }
  }

}
