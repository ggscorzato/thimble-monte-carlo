/*****************************************************************************
 file: constraints.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _CONSTRAINTS_H
#define _CONSTRAINTS_H

void constraints(double **Cnstr, double **phi, double *phi0old, double norm2_noise, double act_old, double dtau, int nt);

#endif
