/*****************************************************************************
 file: alloc_fields.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _ALLOC_FIELDS_H
#define _ALLOC_FIELDS_H

void alloc_fields(int Nt, int first_alloc);

#endif
