/*****************************************************************************
 file: ferm_cntr13_dM_stoch_hub.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
 *******************************************************************************/

#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "init_model.h"
#include "ferm_matrices.h"
#if (MODEL == HUB)

/* 
   Compute res_k += alpha * conj( eta_j (d_j FM_kl)) xi_l
   (accumulating on res)                                                                                   
*/

void ferm_cntr13_dM_stoch_hub(dcomplex *res, double *eta, dcomplex *xi, double alpha){

  int j,jm,s;
  dcomplex ctmp,ceta;

  for(j=0;j<g_csize;j++){
    jm=g_ineigh[g_timedir][DN][j];
    ceta=eta[2*jm] + I * eta[2*jm+1];
    s=1;
    if(jm>j) s=-1;
    ctmp = alpha * conj(ceta * (-s * g_udb)) * xi[jm]; 
    res[j]  += ctmp;
  }
}
#endif
