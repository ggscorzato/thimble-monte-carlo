/*****************************************************************************
 file: extract_transport_noise.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "global.h"
#include "ranlxd.h"
#include "ode_rk4.h"
#include "cgne.h"
#include "mhessian.h"
#include "projector.h"
#include "source_noise_IMR.h"
#include "matrices_noise_IMR.h"
#include "extract_transport_noise.h"

/* 
   phi[Nstart...Nend]: (input)
   noise[Nstart...Nend]: (output)
*/

void extract_transport_noise(double **noise, double **phi, double * pn2n, int Nstart, int Nend){
  int j,iwa=0,iter;
  double norm2;

  /* extract a gaussian vector */
  if(g_proc_coords[g_flowdir]==g_pc_flow_1st){  /* Only if Nstart belongs to this proc */
    gauss_vectord(noise[Nstart],g_size);

    /* The norm of a vector with gaussian elements of size/2 is distributed like a Chi distribution of size/2. 
       This is the distribution that I need for the tangent space later */
    *pn2n = 0;
    for(j=0;j<g_size/2;j++) *pn2n += noise[Nstart][j]*noise[Nstart][j];
    MPI_Allreduce(pn2n,pn2n,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
  }

  MPI_Barrier(g_cart);
  if(g_nt_loc>0){
    /* Compute the first estimate of noise[Nstart...Nend] starting from g_noise[Nstart] with an explicit integrator */
    /* evolution along steepest descent dq=-dSr/dq => dn=-n ddS */
    ode_rk4(noise,&mhessian_model,Nstart,Nend,1,g_dtau,iwa,phi,FW);
    
    /* Refine noise[Nstart...Nend] with an implicit integrator and CGNE (g_noise[Nstart] fixed) */
    source_noise_IMR_FB(g_b,noise,phi,g_dtau,Nstart,Nend);
    iter=cgne(noise,&matrix_noise_IMR_FB,&matrixt_noise_IMR_FB,g_b,g_noise_cg_itermax,g_noise_cg_tol,phi,
	      Nstart+1,Nend+1,Nstart,Nend,0);

    if(g_debug_level>0) if(g_proc_id==0) fprintf(stdout,"CG-iter-dn-TN:%d\n",iter);
  }

  MPI_Barrier(g_cart);
  if(g_debug_level>1){   /* (using g_noise directly here for check) */    
    if(g_proc_coords[g_flowdir]==g_pc_flow_1st){  /* Only if Nstart belongs to this proc */
      norm2=0.0;
      for(j=0;j<g_size;j++) norm2+=g_noise[Nstart][j]*g_noise[Nstart][j];
      MPI_Allreduce(&norm2,&norm2,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
      if(g_proc_id==0) fprintf(stdout,"before proj. |noise[%d]|^2=%g, ",Nstart,norm2);
    }
    MPI_Barrier(g_cart);
    if(g_proc_coords[g_flowdir]==g_np_ntau-1){
      norm2=0.0;
      for(j=0;j<g_size;j++) norm2+=g_noise[Nend][j]*g_noise[Nend][j];
      MPI_Allreduce(&norm2,&norm2,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
      if(g_proc_id==g_np_ntau-1) fprintf(stdout,"|noise[%d]|^2 = %g\n",Nend,norm2);
    }
  }

  /* project the noise near the stationary point (noise[Nend]) */
  if(g_proc_coords[g_flowdir]==g_np_ntau-1)  projector(noise[Nend], noise[Nend], g_proj,0);
  
  MPI_Barrier(g_cart);
  if(g_nt_loc>0){
    /* Compute the first estimate of noise[Nstart...Nend] starting from g_noise[Nend] with an explicit integrator */
    /* evolution along steepest ascent dq = dSr/dq => dn = n ddS*/
    ode_rk4(noise,&mhessian_model,Nstart,Nend,1,g_dtau,iwa,phi,BW);

    /* Refine noise[Nstart...Nend] properly with an implicit integrator and CGNE (g_noise[Nend] fixed) */
    source_noise_IMR_FE(g_b,noise,phi,g_dtau,Nstart,Nend);
    iter=cgne(noise,&matrix_noise_IMR_FE,&matrixt_noise_IMR_FE,g_b,g_noise_cg_itermax,g_noise_cg_tol,phi,
	      Nstart,Nend,Nstart,Nend,0);

    if(g_debug_level>0) if(g_proc_id==0) fprintf(stdout,"CG-iter-up-TN:%d\n",iter);
  }

  MPI_Barrier(g_cart);
  if(g_debug_level>1){   /* (using g_noise directly here for check) */
    if(g_proc_coords[g_flowdir]==g_pc_flow_1st){  /* Only if Nstart belongs to this proc */
      norm2=0.0;
      for(j=0;j<g_size;j++) norm2+=g_noise[Nstart][j]*g_noise[Nstart][j];
      MPI_Allreduce(&norm2,&norm2,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
      if(g_proc_id==0) fprintf(stdout,"after proj. |noise[%d]|^2=%g, ",Nstart,norm2);
    }
    MPI_Barrier(g_cart);
    if(g_proc_coords[g_flowdir]==g_np_ntau-1){
      norm2=0.0;
      for(j=0;j<g_size;j++) norm2+=g_noise[Nend][j]*g_noise[Nend][j];
      MPI_Allreduce(&norm2,&norm2,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
      if(g_proc_id==g_np_ntau-1) fprintf(stdout,"|noise[%d]|^2 = %g\n",Nend,norm2);
    }
  }
  
  /* Resize the norm of the output to the desired size/2-dim Chi distribution */
  MPI_Barrier(g_cart);
  if(g_proc_coords[g_flowdir]==g_pc_flow_1st){  /* Only if Nstart belongs to this proc */
    norm2=0;
    for(j=0;j<g_size;j++) norm2+=noise[Nstart][j]*noise[Nstart][j];
    MPI_Allreduce(&norm2,&norm2,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
    for(j=0;j<g_size;j++) noise[Nstart][j] *= sqrt((*pn2n)/norm2);
  }
}
