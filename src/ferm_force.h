/*****************************************************************************
 file: ferm_force.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
 *******************************************************************************/

#ifndef _FERM_FORCE_H
#define _FERM_FORCE_H

#include "matrix_mult_typedef.h"

void ferm_force_stoch(double * f, double * phi, int nt, matrix_mult_ferm M, matrix_mult_ferm Mt);
void ferm_force_exact(double * f, double * phi);
void ferm_inverse_debug(int fl, int r, int nt);

#endif
