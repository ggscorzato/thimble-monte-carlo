/*****************************************************************************
 file: matrices_SD_IMR.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _MATRICES_SD_IMR_H
#define _MATRICES_SD_IMR_H

void matrix_SD_IMR(double **out, double **in, double dtau, double **phi, int nts, int nte);
void matrixt_SD_IMR(double **out, double **in, double dtau, double **phi, int nts, int nte);

#endif
