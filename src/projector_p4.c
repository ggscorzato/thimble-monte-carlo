/***********************************************************************
 file: projector_p4.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <math.h>
#include <mpi.h>
#include <ggfft.h>
#include "global.h"
#include "init_geometry.h"
#include "init_model.h"
#include "mhessian.h"
#include "projector.h"
#if (MODEL == P4)

/*
  Initialization of the projector at a constant field. See Formulae.pdf and Hessian_analytic_scalar_ssb.pdf
*/
 
void init_projector_p4(double *** proj){

  int j,k,h,l, mu, mnq[DIM], nq[DIM], sm, bi[8];
  double q[DIM],twopi=2*3.141592653589793,cosq[DIM],sinq0,scq,shmu,chmu,dm,norm;
  double A,B,C,A1C,A2C,A3C,BAC,cs,cd,cb,ES,ED; 
  double ev_dm[8][8], ev_sm[4][4] = {{1,0,0,0},  /* +A+3C */
				     {0,1,0,0},  /* -A-3C */
				     {0,0,1,0},  /* +A+C */
				     {0,0,0,1}}; /* -A-C */

  if(g_proc_id==g_nproc-1) fprintf(stdout,"Initializing projector...");
  
  /* reset service basis vectors */
  for(h=0;h<8;h++){
    for(k=0;k<8;k++){
      ev_dm[h][k]=0.0;
    }
  }

  /* set parameters */
  dm=2.0*DIM + g_mass*g_mass;
  shmu=sinh(g_chempot);
  chmu=cosh(g_chempot);
  C=g_lambda*creal(g_phi0)*creal(g_phi0);

  /* main loop over momenta j */
  for(j=0;j<g_vol;j++){
    /* compute momentum dependent variables */
    scq=0.0;
    sm=1;
    for(mu=0;mu<DIM;mu++){
      nq[mu]=g_coord[mu][j];
      mnq[mu]=(-nq[mu]+g_L[mu])%g_L[mu];
      if(nq[mu]!=mnq[mu]) sm*=0;
      q[mu]=(double)(twopi*nq[mu])/g_L[mu];
      cosq[mu] = cos(q[mu]);
      if(mu!=g_timedir) scq+=2.0*cosq[mu];
    }
    sinq0 = sin(q[g_timedir]);
    g_samemom[j]=sm;
    B=2.0*shmu*sinq0;
    A=dm - scq - 2.0*chmu*cosq[g_timedir];
    A1C = A + C;
    A2C = A + 2.0*C;
    A3C = A + 3.0*C;
    BAC = sqrt(B*B + A2C*A2C);
    cs = A2C + BAC;
    cd = A2C - BAC;
    cb = B;
    ES = C+BAC;
    ED = C-BAC;

    /* case p_j != -p_j */
    if(!sm){
      if(fabs(B) > 1e-15){
	/* eigenvectors: */                                                 /* eigenvalues:  */
	ev_dm[0][1]=-cs; ev_dm[0][3]=-cb; ev_dm[0][5]=-cs; ev_dm[0][7]= cb; /*  -ES=-C -BAC */
	ev_dm[1][0]= cs; ev_dm[1][2]= cb; ev_dm[1][4]=-cs; ev_dm[1][6]= cb; /*  -ES=-C -BAC */
	ev_dm[2][1]= cd; ev_dm[2][3]= cb; ev_dm[2][5]=-cd; ev_dm[2][7]= cb; /*   ED=+C -BAC */
	ev_dm[3][0]=-cd; ev_dm[3][2]=-cb; ev_dm[3][4]=-cd; ev_dm[3][6]= cb; /*   ED=+C -BAC */
	
	ev_dm[4][1]=-cd; ev_dm[4][3]=-cb; ev_dm[4][5]=-cd; ev_dm[4][7]= cb; /*  -ED=-C +BAC */
	ev_dm[5][0]= cd; ev_dm[5][2]= cb; ev_dm[5][4]=-cd; ev_dm[5][6]= cb; /*  -ED=-C +BAC */
	ev_dm[6][1]= cs; ev_dm[6][3]= cb; ev_dm[6][5]=-cs; ev_dm[6][7]= cb; /*   ES=+C +BAC */
	ev_dm[7][0]=-cs; ev_dm[7][2]=-cb; ev_dm[7][4]=-cs; ev_dm[7][6]= cb; /*   ES=+C +BAC */
	
	/*         {{0 ,-cs, 0 ,-cb, 0 ,-cs, 0 , cb},   -ES=-C -BAC */
	/*	   { cs, 0 , cb, 0 ,-cs, 0 , cb, 0 },   -ES=-C -BAC */
	/*	   { 0 , cd, 0 , cb, 0 ,-cd, 0 , cb},    ED=+C -BAC */
	/*	   {-cd, 0 ,-cb, 0 ,-cd, 0 , cb, 0 },    ED=+C -BAC */
	
	/*	   { 0 ,-cd, 0 ,-cb, 0 ,-cd, 0 , cb},   -ED=-C +BAC */
	/*	   { cd, 0 , cb, 0 ,-cd, 0 , cb, 0 },   -ED=-C +BAC */
	/*	   { 0 , cs, 0 , cb, 0 ,-cs, 0 , cb},    ES=+C +BAC */
	/*	   {-cs, 0 ,-cb, 0 ,-cs, 0 , cb, 0 }};   ES=+C +BAC */

	if((ES>0) && (ED>0)){
	  bi[0]=6; bi[1]=7; bi[2]=2; bi[3]=3;
	} else if((ES>0) && (ED<0)){
	  bi[0]=6; bi[1]=7; bi[2]=4; bi[3]=5;
	} else if((ES<0) && (ED>0)){
	  bi[0]=0; bi[1]=1; bi[2]=2; bi[3]=3;
	} else if((ES<0) && (ED<0)){
	  bi[0]=0; bi[1]=1; bi[2]=4; bi[3]=5;
	}
	
      } else { // if B=0

	ev_dm[0][1]= 1.0; ev_dm[0][3]= 0.0; ev_dm[0][5]= 1.0; ev_dm[0][7]= 0.0; /* -A -3C */
	ev_dm[1][0]=-1.0; ev_dm[1][2]= 0.0; ev_dm[1][4]= 1.0; ev_dm[1][6]= 0.0; /* -A -3C */
	ev_dm[2][1]= 0.0; ev_dm[2][3]= 1.0; ev_dm[2][5]= 0.0; ev_dm[2][7]= 1.0; /* -A -C */
	ev_dm[3][0]= 0.0; ev_dm[3][2]=-1.0; ev_dm[3][4]= 0.0; ev_dm[3][6]= 1.0; /* -A -C */
	
	ev_dm[4][1]= 0.0; ev_dm[4][3]=-1.0; ev_dm[4][5]= 0.0; ev_dm[4][7]= 1.0; /* +A +C */
	ev_dm[5][0]= 0.0; ev_dm[5][2]= 1.0; ev_dm[5][4]= 0.0; ev_dm[5][6]= 1.0; /* +A +C */
	ev_dm[6][1]=-1.0; ev_dm[6][3]= 0.0; ev_dm[6][5]= 1.0; ev_dm[6][7]= 0.0; /* +A +3C */
	ev_dm[7][0]= 1.0; ev_dm[7][2]= 0.0; ev_dm[7][4]= 1.0; ev_dm[7][6]= 0.0; /* +A +3C */
	
	/*         {{0 , 1 , 0 , 0 , 0 , 1 , 0 , 0 },   -A -3C */
	/*	   {-1 , 0 , 0 , 0 , 1 , 0 , 0 , 0 },   -A -3C */
	/*	   { 0 , 0 , 0 , 1 , 0 , 0 , 0 , 1 },   -A -C */
	/*	   { 0 , 0 ,-1 , 0 , 0 , 0 , 1 , 0 },   -A -C */
	
	/*	   { 0 , 0 , 0 ,-1 , 0 , 0 , 0 , 1 },   +A +C */
	/*	   { 0 , 0 , 1 , 0 , 0 , 0 , 1 , 0 },   +A +C */
	/*	   { 0 ,-1 , 0 , 0 , 0 , 1 , 0 , 0 },   +A +3C */
	/*	   { 1 , 0 , 0 , 0 , 1 , 0 , 0 , 0 }};  +A +3C */

	if((A3C>0) && (A1C>0)){
	  bi[0]=6; bi[1]=7; bi[2]=4; bi[3]=5;
	} else if((A3C>0) && (A1C<0)){
	  bi[0]=6; bi[1]=7; bi[2]=2; bi[3]=3;
	} else if((A3C<0) && (A1C>0)){
	  bi[0]=0; bi[1]=1; bi[2]=4; bi[3]=5;
	} else if((A3C<0) && (A1C<0)){
	  bi[0]=0; bi[1]=1; bi[2]=2; bi[3]=3;
	}

      } // endif B!=0

      /* normalize the ev_dm */
      for(l=0;l<4;l++){
	norm=0.0;
	for(k=0;k<8;k++) norm+=ev_dm[bi[l]][k]*ev_dm[bi[l]][k];
	norm=1.0/sqrt(norm);
	for(k=0;k<8;k++) ev_dm[bi[l]][k] *= norm;
      }
      
      for(h=0;h<4;h++){
	for(k=0;k<8;k++){
	  proj[j][h][k]=0;
	  for(l=0;l<4;l++) proj[j][h][k]+=ev_dm[bi[l]][h]*ev_dm[bi[l]][k];
	}
      }

    /* case p_j == -p_j */
    }else if(sm){ 

      if(A3C>0){
	bi[0]=0;
      } else {
	bi[0]=1;
      }
      if(A1C>0){
	bi[1]=2;
      } else {
	bi[1]=3;
      }
      for(h=0;h<4;h++){
	for(k=0;k<4;k++){
	  proj[j][h][k]=0;
	  for(l=0;l<2;l++) proj[j][h][k]+=ev_sm[bi[l]][h]*ev_sm[bi[l]][k];
	}
      }
    }
  }
  if(g_proc_id==g_nproc-1) fprintf(stdout,"OK!\n");
}

#endif
