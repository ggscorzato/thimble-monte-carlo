/*****************************************************************************
 file: residual_phase.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: December 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "global.h"
#include "init_model.h"
#include "ode_rk4.h"
#include "extract_transport_noise.h"
#include "mhessian.h"
#include "residual_phase.h"

void residual_phase_stoch(int it){

  int s,ls,r,j,ingame;
  double n2n,avph=0.0,stdph=0.0,iNR,*phases;

  iNR=1.0/((double)g_NR);

  phases=(double*)malloc(g_NR*sizeof(double));

  for(r=0;r<g_NR;r++){
    phases[r]=0.0;
    ingame=1;
    for(s=0;s<g_nt_tot;s++){

      if(g_np_ntau > 1){
	/* find the flow coord of the processors owning s */
	if((g_nt_1st<= s) && (s <= g_nt_1st+g_nt_loc)){
	  g_pc_flow_1st=g_proc_coords[g_flowdir];
	}
	if(g_pc_flow_1st!=0) MPI_Bcast(&g_pc_flow_1st,1,MPI_INT,g_proc_id,g_cart);

	/* skip if this proc is outside range */
	if(g_proc_coords[g_flowdir]<g_pc_flow_1st) ingame=0;
	
	/* reset s for this proc. */
	ls = s-g_nt_1st;
	ls = MAX(0,ls);
      } else {
	ls=s;
      }

      if(ingame){
	extract_transport_noise(g_noise,g_field,&n2n,ls,g_nt_tot);
	mhessian_model(g_ff,g_noise[ls],g_field,ls,0,0,0);
	
	/* phase += noise J H noise */
	for(j=0;j<g_csize;j++){
	  phases[r] += g_dtau * g_noise[ls][2*j] * g_ff[2*j+1];
	  phases[r] -= g_dtau * g_noise[ls][2*j+1] * g_ff[2*j];
	}
      }
      MPI_Barrier(g_cart);
    }
    MPI_Allreduce(&phases[r],&phases[r],1,MPI_DOUBLE,MPI_SUM,g_cart);
    avph += iNR * phases[r];
    stdph +=  iNR * phases[r] * phases[r];
  }
  stdph -= avph*avph;
  stdph = sqrt(stdph);

  if(g_proc_id==0) fprintf(stdout,"\n Mean Residual Phase for configuration %d is: %g, with std: %g.\n",
			   it,avph,stdph);
}

/* compute he residual phase with Lapack. No space-time parallelization. */
void residual_phase_exact(int it){

  int iwa=0,i,j,k;
  dcomplex cc, cdet=0.0,cdet1=0.0;
  dcomplex *vr = NULL,*vl = NULL;
  int lwork, info, n;
  n=g_csize; lwork = g_lwfact*n;

  if(g_nproc>1){
    if(g_proc_id==0) fprintf(stdout,"ERROR: parallel version for thimb_tan_basis not available.Exiting...\n");
    MPI_Abort(MPI_COMM_WORLD,-78);
    exit(-78);
  }

  /* evolve the basis from nt=g_nt_tot to nt=0 */
  for(j=0;j<g_tvol*CPS;j++){
    ode_rk4(g_thimb_tan_basis[j],&projected_mhessian,0,g_nt_tot,1,g_dtau,iwa,g_field,BW);
  }

  /* compute the determinant */

  for(j=0;j<g_csize;j++){
    for(i=0;i<g_csize;i++){
      g_B[i*g_csize+j] = g_thimb_tan_basis[j][0][2*i] + I * g_thimb_tan_basis[j][0][2*i+1];
    }
  }

  /* check orthonormality */ 
  if(g_debug_level>3){
    for(j=0;j<g_csize;j++){
      for(i=0;i<g_csize;i++){
	cc=0.0;
	for(k=0;k<g_csize;k++){
	  cc += g_B[i*g_csize+k] * conj(g_B[j*g_csize+k]);
	}
	fprintf(stdout,"cc[%d,%d]=(%g,%g)\n",i,j,creal(cc),cimag(cc));
      }
    }
  }

  /* Eigenvalues computation  */
  zgeev_("N", "N",&n, g_B, &n, g_evalues, vl, &n, vr, &n,
	 g_evwork, &lwork, g_rwork, &info);

  if(info!=0){
    if(g_proc_id==0) fprintf(stderr,"zgeev_ actual: info=%d, n=%d.\n",info,n); 
    fflush(stdout);
    exit(-79);
  }

  /* cdet = det[Bais]  */
  cdet=1.0;
  cdet1=0.0;
  for(j=0;j<g_csize;j++){
    cdet *=g_evalues[j];
    cdet1 += clog(g_evalues[j]);
  }

  cdet1 = cexp(cdet1);

  if(g_proc_id==0) fprintf(stdout,"\nExact (zgeev) Residual Phase for configuration %d is: (%g,%g) = (%g,%g).\n"
			   "(Failure to have unitary modulus indicates loss of orthogonality in the transport)\n",
			   it,creal(cdet),cimag(cdet),creal(cdet1),cimag(cdet1));
}

/* generate thimble-tangent-basis at nt = g_nt_tot.  No space-time parallelization */
void gen_thimb_tan_basis(){

  int k,j,i,l;
  dcomplex cc,cdet;

  /* Lapack variables */
  double *AA, *WORK, *WW;
  int NN, LDA, LWORK, INFO, len_jobz, len_uplo;
  dcomplex *vr = NULL,*vl = NULL;
  int lwork, info, n;
  n=g_csize; lwork=g_lwfact*n;
  NN=g_size; LDA=g_size; LWORK=g_lwfact*g_size; len_jobz=1; len_uplo=1;

  AA =(double *)calloc(g_size*g_size,sizeof(double));
  WORK =(double *)calloc(LWORK,sizeof(double));
  WW =(double *)calloc(g_size,sizeof(double));

  if(g_vol!=g_tvol){
    if(g_proc_id==0) fprintf(stdout,"ERROR: parallel version for thimb_tan_basis not available.Exiting...\n");
    MPI_Finalize();
    exit(-77);
  }

  for(j=0;j<g_size;j++) g_field_new[0][j]=g_stat_conf[j];
  for(j=0;j<g_size;j++) g_kk[j]=0.0;
  for(j=0;j<g_size;j++) {
    g_kk[j]=1.0;
    mhessian_model(g_ff,g_kk,g_field_new,0,0,0,0);
    for(i=0;i<g_size;i++) AA[j*g_size+i] = g_ff[i];
    g_kk[j]=0.0;
  }

  dsyev_("V", "U", &NN, AA, &LDA, WW, WORK, &LWORK, &INFO, len_jobz, len_uplo);
  if(INFO!=0){
    if(g_proc_id==0) fprintf(stderr,"dsyev ERROR");
    MPI_Abort(MPI_COMM_WORLD,-13);
    exit(-14);
  }

  l=0;
  for(j=0;j<g_size;j++){
    if(WW[j]<0){
      for(i=0;i<g_size;i++) g_thimb_tan_basis[l][g_nt_tot][i]=AA[j*g_size+i];
      l++;
    }
  }

  for(j=0;j<g_csize;j++){
    for(i=0;i<g_csize;i++){
      g_B[i*g_csize+j] = g_thimb_tan_basis[j][g_nt_tot][2*i] + I * g_thimb_tan_basis[j][g_nt_tot][2*i+1];
    }
  }
  
  /* check orthonormality */ 
  if(g_debug_level>3){      
    for(j=0;j<g_size;j++){
      for(i=0;i<g_size;i++){
	cc=0.0;
	for(k=0;k<g_size;k++){
	  cc += AA[i*g_size+k] * AA[j*g_size+k];
	}
	fprintf(stdout,"aa[%d,%d]=(%g,%g)\n",i,j,creal(cc),cimag(cc));
      }
    }
    for(j=0;j<g_csize;j++){
      for(i=0;i<g_csize;i++){
	cc=0.0;
	for(k=0;k<g_csize;k++){
	  cc += g_B[i*g_csize+k] * conj(g_B[j*g_csize+k]);
	}
	fprintf(stdout,"bb[%d,%d]=(%g,%g)\n",i,j,creal(cc),cimag(cc));
      }
    }
  }

  /* compute phase at the stationary point */
  /* Eigenvalues computation  */
  zgeev_("N", "N",&n, g_B, &n, g_evalues, vl, &n, vr, &n,
	 g_evwork, &lwork, g_rwork, &info);

  if(info!=0){
    if(g_proc_id==0) fprintf(stderr,"zgeev_ actual: info=%d, n=%d.\n",info,n); 
    fflush(stdout);
    exit(-79);
  }

  /* cdet = det[Bais]  */
  cdet=1.0;
  for(j=0;j<g_csize;j++){
    cdet *=g_evalues[j];
  }

  if(g_proc_id==0) fprintf(stdout,"\nExact (zgeevx) Residual Phase for the stationary configuration: (%g,%g).\n",
			   creal(cdet),cimag(cdet));

  free(AA);
  free(WW);
  free(WORK);
}


