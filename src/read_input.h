/****************************************************************************
 file: read_input.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _READ_INPUT_H
#define _READ_INPUT_H

void read_input(char * inputfilename);

#endif
