/*****************************************************************************
 file: action.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _ACTION_H
#define _ACTION_H

dcomplex action_model(double *phi);

#endif
