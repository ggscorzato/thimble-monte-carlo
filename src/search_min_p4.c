/*****************************************************************************
 file: search_min_p4.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: December 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "global.h"
#include "ferm_force.h"
#include "init_model.h"
#include "search_min.h"
#if (MODEL == P4)

dcomplex search_min_p4(dcomplex phi0){

  int j;
  double b,is2,ar,hr,phi0p;
  dcomplex phi0e;

  if(g_proc_id==0) fprintf(stdout,"Start computing the stationary point...\n");

  /*** Minimum ***/
  b=0.5*(g_mass*g_mass + 2 - 2 * cosh(g_chempot));
  is2=1/sqrt(2);

  if(b>-1e-15){
    phi0=0.0;
  } else {
    ar=0.25*g_lambda/fabs(b);
    hr=g_h/fabs(b);

    phi0p  = -is2/sqrt(ar);
    phi0p -= (1./4.)*hr;
    phi0p += is2*(3./16.)*sqrt(ar)*pow(hr,2);
    phi0p -= (1./8.)*ar*pow(hr,3);
    phi0p += is2*(105./512.)*pow(ar,3./2.)*pow(hr,4);
    phi0p -= (3./16.)*pow(ar,2)*pow(hr,5);
    phi0p += is2*(3003./8192.)*pow(ar,5./2.)*pow(hr,6);
    phi0p -= (3./8.)*pow(ar,3)*pow(hr,7);

    phi0e  = I * (I+csqrt(3.)) * cpow( -9.*sqrt(ar)*hr + csqrt(-24.+81.*ar*pow(hr,2)), 2./3.);
    phi0e -= 4. * cpow(-3.,1./3.);
    phi0e /= cpow( -9.*pow(ar,2.)*hr + sqrt(3.) * csqrt(pow(ar,3.) * (-8. + 27.*ar*hr*hr))  , 1./3.);
    phi0e /= 4. * cpow(3.,2./3.);
  }

  phi0=creal(phi0e);
  if(g_proc_id==0) fprintf(stdout,"Minimum=%g.\n",creal(phi0));
  if(g_debug_level>0) if(g_proc_id==0) fprintf(stdout,"check: imaginary part of min =%g.\n",cimag(phi0e));  
  if(g_debug_level>0) if(g_proc_id==0) fprintf(stdout,"check compute the minimum through an expansion:"
					       " min =%g.\n",phi0p);  

  for(j=0;j<g_vol;j++){
    g_stat_conf[DPS*j+0] = creal(phi0);
    g_stat_conf[DPS*j+1] = cimag(phi0);
    g_stat_conf[DPS*j+2] = 0.0;
    g_stat_conf[DPS*j+3] = 0.0;
  }

  return phi0;
}


dcomplex search_min_lapack_p4(dcomplex phi0){

  return phi0;
}
#endif
