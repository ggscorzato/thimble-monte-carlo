/*****************************************************************************
 file: read_input.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <mpi.h>
#include "global.h"
#include "read_input.h"

void read_input(char * inputfilename){

  FILE * fid=NULL;
  char var[128], value[128], temp[128];
  int mu;
  
  /* Put default values here, if necessary */
  
  for(mu=0;mu<DIM;mu++){
    g_nprocl[mu]=1;
    g_indexord[mu]=DIM-mu-1;
    g_lambda=1.0;
    g_mass=1.0;
    g_chempot=0.0;
    g_mu_plus=0.0;
    g_mu_minus=0.0;
    g_h=0.0;
    g_t=0;
    g_U=0;
    g_dbeta=0;
    g_NF=0;
    g_NR=0;
    g_ferm_stoch=0;
    g_dtau=1e-4;
    g_dt=1e-4;
    g_nt_tot=40;
    g_nt_loc_first=10;
    g_nt_loc_last=10;
    g_np_ntau=1;
    g_nsub=1;
    g_start=0;
    g_start_config_num=0;
    g_with_residual_phase=0;
    g_with_projected_mhessian=0;
    g_init_rescale=1e-2;
    g_niter=10;
    g_nskip=10;
    g_ntherm=0;
    g_timedir=0;
    g_si_tol=3e-1;
    g_noise_cg_tol=1e-10;
    g_noise_cg_itermax=100;
    g_noise_cgf_tol=1e-10;
    g_noise_cgf_itermax=100;
    g_sd_cg_tol=1e-10;
    g_sd_cg_itermax=500;
    g_sd_nr_tol=1e-10;
    g_sd_nr_itermax=10;
    g_weight_act_const=1.0;     
  }
  
  /* First read of inputfile (scalar variables) */
  
  if(g_proc_id==0){
    fprintf(stdout,"Start reading input file...");     
    
    if((fid = fopen(inputfilename, "r")) == (FILE*)NULL) 
      fprintf(stderr,"Error opening input file: %s",inputfilename);
    
    while (fgets(temp, 127, fid) != NULL) {
      if(temp[0] == '#') continue;
      
      sscanf(temp, "%[A-Z]=%s\n", var, value);
      
      if (!strcmp(var, "DEBUGLEVEL")) g_debug_level = atoi(value); 
      /* 0: only necessary info. 
	 1: print info from top level routines with no perf loss.
	 2: subdominant perf loss and modest printout clutter.
	 3: dominant perf loss, but still compatible with run.
	 4: superdominant perf loss: ok only for small lattices	and few iterations.
      */
      if (!strcmp(var, "LAMBDA")) g_lambda = atof(value);
      if (!strcmp(var, "MASS")) g_mass = atof(value);
      if (!strcmp(var, "CHEMPOT")) g_chempot = -atof(value);   // DEBUG set -mu to agree with Marco's projection.
      if (!strcmp(var, "MUPLUS")) g_mu_plus = atof(value);
      if (!strcmp(var, "MUMINUS")) g_mu_minus = atof(value);
      if (!strcmp(var, "SOURCE")) g_h = atof(value);
      if (!strcmp(var, "THUBBARD")) g_t = atof(value);
      if (!strcmp(var, "UHUBBARD")) g_U = atof(value);
      if (!strcmp(var, "DBETA")) g_dbeta = atof(value);
      if (!strcmp(var, "NFLAVORS")) g_NF = atoi(value);
      if (!strcmp(var, "NRANDFERM")) g_NR = atoi(value);
      if (!strcmp(var, "FERMSTOCH")) g_ferm_stoch = atoi(value);
      if (!strcmp(var, "LANGEVINDT")) g_dt = atof(value); /* discretization step for Langevin */
      if (!strcmp(var, "DTAU")) g_dtau = atof(value); /* discretization steps along the gradient flow */
      if (!strcmp(var, "TOTNTAU")) g_nt_tot = atoi(value); /* TOTNTAU+1 is the number of points (incl. extremes) 
							      in the direction of the gradient flow.
							      TOTNTAU*g_dtau is the total length */
      if (!strcmp(var, "LOCNTAUFIRST")) g_nt_loc_first = atoi(value); /* ntau in first slice */ 
      if (!strcmp(var, "LOCNTAULAST")) g_nt_loc_last = atoi(value); /* ntau in last slice */ 
      if (!strcmp(var, "NPNTAU")) g_np_ntau = atoi(value); /* number of processes in the flow direction */ 
      if (!strcmp(var, "NSUB")) g_nsub = atoi(value); /* for the initial proposal the ode is solved with a time
							 step which is NSUB times finer */
      if (!strcmp(var, "SEED")) g_seed = atoi(value);
      if (!strcmp(var, "START")) g_start = atoi(value);/* 0:random, 1:readin, 3:tests only, 4:measure only */
      if (!strcmp(var, "INITCONFNUM")) g_start_config_num = atoi(value); /* Conf number to be read in */
      if (!strcmp(var, "WITHRESIDUALPHASE")) g_with_residual_phase = atoi(value); /* 1:comp res-phase. 0: skip it*/
      if (!strcmp(var, "WITHPROJHESS")) g_with_projected_mhessian = atoi(value); /* 1: proj the hessian. 0: don't*/
      if (!strcmp(var, "INITRESCALE")) g_init_rescale = atof(value); /* rescale the initial field: the smaller the
									colder */
      if (!strcmp(var, "NITER")) g_niter = atoi(value); /* # of total iterations */
      if (!strcmp(var, "NSKIP")) g_nskip = atoi(value); /* save config every SKIP iterations after THERM
							   iterations */

      if (!strcmp(var, "NTHERM")) g_ntherm = atoi(value);
      if (!strcmp(var, "TIMEDIR")) g_timedir = atoi(value); /* Select the time direction */
      if (!strcmp(var, "SITOL")) g_si_tol = atof(value); /* tolerance on the imaginary part of the action */
      if (!strcmp(var, "NOISECGTOL")) g_noise_cg_tol = atof(value); /* quadratic tolerance in CGNE for noise
								       evolution */
      if (!strcmp(var, "NOISECGITERMAX")) g_noise_cg_itermax = atoi(value); /* max iterations of CGNE for noise
									       evolution */
      if (!strcmp(var, "NOISECGFERMTOL")) g_noise_cgf_tol = atof(value); /* quadratic tolerance in CGNE for 
									    noisy fermions */
      if (!strcmp(var, "NOISECGFERMITERMAX")) g_noise_cgf_itermax = atoi(value); /* max iterations of CGNE for 
										    for noisy fermions */
      if (!strcmp(var, "SDCGTOL")) g_sd_cg_tol = atof(value); /* quadratic tolerance in CGNE for SD evolution */
      if (!strcmp(var, "SDCGITERMAX")) g_sd_cg_itermax = atoi(value); /* max iterations of CGNE for SD
									 evolution */
      if (!strcmp(var, "SDNRTOL")) g_sd_nr_tol = atof(value); /* quadratic tolerance in Newton-Raphson for SD
								 evolution */
      if (!strcmp(var, "SDNRITERMAX")) g_sd_nr_itermax = atoi(value); /* max iterations of Newton-Raphson for SD
									 evolution */
      if (!strcmp(var, "WFIXACT")) g_weight_act_const = atof(value); /* weight associated to the constraint of
									keeping the real action fixed during
									steepest descent adjustment  */
      
    }
    fclose(fid);

    /* Second read of inputfile (now read vectors) */ 
    
    if((fid = fopen(inputfilename, "r")) == (FILE*)NULL) 
      fprintf(stderr,"Error opening input file: %s",inputfilename);
    
    while (fgets(temp, 127, fid) != NULL) {
      if(temp[0] == '#') continue;
      
      sscanf(temp, "%[A-Z]%d=%s\n", var, &mu, value);

      if(mu<DIM){
	if (!strcmp(var, "L")) g_L[mu] = atoi(value);              /* global lattice extension in direction mu */
	if (!strcmp(var, "NPROC")) g_nprocl[mu] = atoi(value);     /* number of mpi processes in direction mu */
	if (!strcmp(var, "INDEXORD")) g_indexord[mu] = atoi(value);/* indexord[0]: fastest (inmost) index in lexic
								      indexord[DIM-1]: slowest (outmost) index. */
      }
    }
    fclose(fid);
    fprintf(stdout,"OK!\n");
  }
  
  /* Broadcast values to all processes */
  
  MPI_Bcast(&g_debug_level, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_nt_tot, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_nt_loc_first, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_nt_loc_last, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_np_ntau, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_nsub, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_seed, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_start, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_start_config_num, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_with_residual_phase, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_with_projected_mhessian, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_niter, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_nskip, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_ntherm, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_timedir, 1, MPI_INT, 0, MPI_COMM_WORLD);

  MPI_Bcast(&g_lambda, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_mass, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_chempot, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_mu_plus, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_mu_minus, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_h, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_t, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_U, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_dbeta, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_NF, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_NR, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_ferm_stoch, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_dt, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_dtau, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_init_rescale, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_si_tol, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_noise_cg_tol, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_noise_cg_itermax, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_noise_cgf_tol, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_noise_cgf_itermax, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_sd_cg_tol, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_sd_cg_itermax, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_sd_nr_tol, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_sd_nr_itermax, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_weight_act_const, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  
  MPI_Bcast(&g_L[0], DIM, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_nprocl[0], DIM, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&g_indexord[0], DIM, MPI_INT, 0, MPI_COMM_WORLD);
  
  MPI_Barrier(MPI_COMM_WORLD);
  
  /* Write the input values (from the last process for check) */
  if(g_proc_id==g_nproc-1){
    fprintf(stdout,"Input variables broadcasted. They are:\n");
    fprintf(stdout,"DEBUGLEVEL=%d\n",g_debug_level);
    fprintf(stdout,"LAMBDA=%g\n",g_lambda);
    fprintf(stdout,"MASS=%g\n",g_mass);
    fprintf(stdout,"CHEMPOT=%g\n",g_chempot);
    fprintf(stdout,"MUPLUS=%g\n",g_mu_plus);
    fprintf(stdout,"MUMINUS=%g\n",g_mu_minus);
    fprintf(stdout,"SOURCE=%g\n",g_h);
    fprintf(stdout,"THUBBARD=%g\n",g_t);
    fprintf(stdout,"UHUBBARD=%g\n",g_U);
    fprintf(stdout,"DBETA=%g\n",g_dbeta);
    fprintf(stdout,"NFLAVORS=%d\n",g_NF);
    fprintf(stdout,"NRANDFERM=%d\n",g_NR);
    fprintf(stdout,"FERMSTOCH=%d\n",g_ferm_stoch);
    fprintf(stdout,"LANGEVINDT=%g\n",g_dt);
    fprintf(stdout,"DTAU=%g\n",g_dtau);
    fprintf(stdout,"TOTNTAU=%d\n",g_nt_tot);
    fprintf(stdout,"LOCNTAUFIRST=%d\n",g_nt_loc_first);
    fprintf(stdout,"LOCNTAULAST=%d\n",g_nt_loc_last);
    fprintf(stdout,"NPNTAU=%d\n",g_np_ntau);
    fprintf(stdout,"NSUB=%d\n",g_nsub);
    fprintf(stdout,"SEED=%d\n",g_seed);
    fprintf(stdout,"START=%d\n",g_start);
    fprintf(stdout,"INITCONFNUM=%d\n",g_start_config_num);
    fprintf(stdout,"WITHRESIDUALPHASE=%d\n",g_with_residual_phase);
    fprintf(stdout,"WITHPROJHESS=%d\n",g_with_projected_mhessian);
    fprintf(stdout,"INITRESCALE=%g\n",g_init_rescale);
    fprintf(stdout,"NITER=%d\n",g_niter);
    fprintf(stdout,"NSKIP=%d\n",g_nskip);
    fprintf(stdout,"NTHERM=%d\n",g_ntherm);
    fprintf(stdout,"TIMEDIR=%d\n",g_timedir);
    fprintf(stdout,"SITOL=%g\n",g_si_tol);
    fprintf(stdout,"NOISECGTOL=%g\n",g_noise_cg_tol);
    fprintf(stdout,"NOISECGITERMAX=%d\n",g_noise_cg_itermax);
    fprintf(stdout,"NOISECGFERMTOL=%g\n",g_noise_cgf_tol);
    fprintf(stdout,"NOISECGFERMITERMAX=%d\n",g_noise_cgf_itermax);
    fprintf(stdout,"SDCGTOL=%g\n",g_sd_cg_tol);
    fprintf(stdout,"SDCGITERMAX=%d\n",g_sd_cg_itermax);
    fprintf(stdout,"SDNRTOL=%g\n",g_sd_nr_tol);
    fprintf(stdout,"SDNRITERMAX=%d\n",g_sd_nr_itermax);
    fprintf(stdout,"WFIXACT=%g\n",g_weight_act_const);
    for(mu=0;mu<DIM;mu++) fprintf(stdout,"L[%d]=%d  ",mu,g_L[mu]);
    fprintf(stdout,"\n");
    for(mu=0;mu<DIM;mu++) fprintf(stdout,"NPROC[%d]=%d  ",mu,g_nprocl[mu]);
    fprintf(stdout,"\n");
    for(mu=0;mu<DIM;mu++) fprintf(stdout,"INDEXORD[%d]=%d  ",mu,g_indexord[mu]);
    fprintf(stdout,"\n");
  }
}
