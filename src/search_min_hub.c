/*****************************************************************************
 file: search_min_hub.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: December 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "global.h"
#include "ferm_force.h"
#include "init_model.h"
#include "search_min.h"
#if (MODEL == HUB)

dcomplex search_min_hub(dcomplex phi0){

  int fl,it=0,itermax=500,j,mu,np[DIM];
  double eps=1.,p[DIM],sp,gammap,ph,twopi=2*3.141592653589793,itvol;
  dcomplex alpha,fterm,su;
  
  if(g_proc_id==0) fprintf(stdout,"Start searching for a stationary point...\n");

  itvol=1.0/((double)g_tvol);

  if(g_U>0){ 
    su = I*csqrt(g_U); 
  }else{ 
    su = csqrt(-g_U);
  }
  
  while((eps > 1e-14) && (it < itermax)){
    
    if(g_debug_level>0) if(g_proc_id==0) fprintf(stdout,"Iteration %d, eps=%g\n",it,eps);

    fterm=0.0;
    for(fl=0;fl<g_NF;fl++){
      alpha=1.0 + g_udb * phi0 + g_Umu[fl];
      for(j=0;j<g_vol;j++){
	sp=0.0;
	for(mu=0;mu<DIM;mu++){
	  np[mu]=g_coord[mu][j];
	  ph=0.0;
	  if(mu==g_timedir) ph=0.5;
	  p[mu]=(double)(twopi*(np[mu]+ph))/g_L[mu];
          if(mu!=g_timedir){
	    sp += sin(p[mu]);
	  }
	}
	gammap = -2.0*g_tdb*sp;
	fterm += cexp(I*p[g_timedir]) / (1.0 - cexp(I * p[g_timedir])*(alpha + I * gammap));
      }
    }

    MPI_Allreduce(&fterm,&fterm,1,MPI_DOUBLE_COMPLEX,MPI_SUM,g_sub_cart);

    fterm *= (-su * itvol);
    eps = cabs(phi0 - fterm);
    phi0=fterm;
    it++;

    if(g_proc_id==0) fprintf(stdout,"phi[%d]=(%g,%g) \n",
			     it,creal(phi0),cimag(phi0));

  }

  if(eps < 1e-14){
    if(g_proc_id==0) fprintf(stdout,"Stationary point phi0=(%g,%g) found after %d iterations\n",
			     creal(phi0),cimag(phi0),it);
    fflush(stdout);

    for(j=0;j<g_vol;j++){
      g_stat_conf[DPS*j+0] = creal(phi0);
      g_stat_conf[DPS*j+1] = cimag(phi0);
    }

    return phi0;

  } else {
    if(g_proc_id==0) fprintf(stdout,"No convergence reached after %d iterations:  eps=%g\n",it,eps);
    fflush(stdout);
    exit(-56);
    return phi0;
  }
}

/*
  iterate: phi_j = conj(Tr[M^-1 d_j M])/dbeta   (where M=conj(FM))
  to find a stationary point

 */

dcomplex search_min_lapack_hub(dcomplex phi0){

  int it=0,itermax=500,j;
  double eps=1.;
  dcomplex res;

  if(g_proc_id==0) fprintf(stdout,"Start searching for a stationary point by numerical inversion...\n");

  while((eps > 1e-14) && (it < itermax)){
    
    for(j=0;j<g_csize;j++){
      g_kk[2*j  ]=creal(phi0);
      g_kk[2*j+1]=cimag(phi0);
      g_ff[2*j  ]=0;
      g_ff[2*j+1]=0;
    }      
    ferm_force_exact(g_ff,g_kk);
    res=conj(g_ff[0] + I*g_ff[1])/g_dbeta;

    eps=cabs(phi0-res);
    phi0=res;

    it++;    
    if(g_debug_level>0) if(g_proc_id==0) fprintf(stdout,"Iteration %d, eps=%g\n",it,eps);
    if(g_proc_id==0) fprintf(stdout,"phi[%d]=(%g,%g) \n",it,creal(phi0),cimag(phi0));
  }
  
  if(eps < 1e-14){
    if(g_proc_id==0) fprintf(stdout,"Stationary point phi0=(%g,%g) found after %d iterations\n",
			     creal(phi0),cimag(phi0),it);
    fflush(stdout);

    for(j=0;j<g_vol;j++){
      g_stat_conf[DPS*j+0] = creal(phi0);
      g_stat_conf[DPS*j+1] = cimag(phi0);
    }

    return phi0;

  } else {
    if(g_proc_id==0) fprintf(stdout,"No convergence reached after %d iterations:  eps=%g\n",it,eps);
    fflush(stdout);
    exit(-56); 
    return phi0;
  }
}
void test_search_min(){

  int i,itermax=2000;
  double step=100.0;
  dcomplex res, ii;

  for(i=-itermax;i<itermax;i++){
    ii=(dcomplex)i/step;
    ii = I*ii;
    res=search_min_lapack_hub(ii);
    fprintf(stdout,"smhLAP  %g  %g  %g\n",cimag(ii),creal(res),cimag(res));
  }

  for(i=-itermax;i<itermax;i++){
    ii=(dcomplex)i/step;
    ii = I*ii;
    res=search_min_hub(ii);
    fprintf(stdout,"smhMOM  %g  %g  %g\n",cimag(ii),creal(res),cimag(res));
  }

}

#endif
