/*****************************************************************************
 file: cgne.h
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#ifndef _CGNE_H
#define _CGNE_H

#include "matrix_mult_typedef.h"

int cgne(double ** y, matrix_mult_cgne A, matrix_mult_cgne At, double ** b, int maxiter, 
	 double tol, double **phi, int ns_p, int ne_p, int ns_r, int ne_r, int extra_cond);

int ferm_cgne(dcomplex * y, matrix_mult_ferm A, matrix_mult_ferm At, dcomplex * b, int maxiter, 
	      double tol, double *phi, int N, int fl);

#endif
