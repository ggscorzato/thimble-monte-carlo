/*****************************************************************************
 file: matrix_SD_IMR.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <mpi.h>
#include "global.h"
#include "mhessian.h"
#include "projector.h"
#include "force.h"
#include "xchange_fields.h"
#include "matrices_SD_IMR.h"

/*
  in: dof h= 0...ntau
  out: con k= 0...ntau-1  + EXTRACOND
  phi[g_nt_tot] is assumed to be projected
  Normally nts=0, nte=g_nt_tot
*/
void matrix_SD_IMR(double **out, double **in, double dtau, double **phi, int nts, int nte){

  int k,j,iwa=0,acc=1,dumm=0;
  double tmp,dtau2;
  dtau2=0.5*dtau;

  if(g_proc_coords[g_flowdir]==g_pc_flow_last) projector(in[nte],in[nte],g_proj,0);

  if(g_np_ntau>1) xchange_fields_flow_up(in); /* recv in[g_nt_loc] if not last; send in[0] if not first; */

  for(k=nts;k<nte;k++) for(j=0;j<g_size;j++) out[k][j] = in[k+1][j];
  for(k=nts;k<nte;k++) for(j=0;j<g_size;j++) out[k][j] -= in[k][j];
  
  for(k=nts;k<nte;k++){
    for(j=0;j<g_size;j++) g_kk[j] = -dtau2*(in[k][j]+in[k+1][j]);
    mhessian_model(out[k],g_kk,phi,k,PLUSHALF,acc,iwa); /* mhessian = - ddS */
  }

  if(g_proc_coords[g_flowdir]==g_pc_flow_1st){
    if(EXTRACOND>0){  /** Extra cond (**) **/
      force_model(g_kk,phi[nts],NULL,nts,dumm,0,1); /* force = - dS/dphi */
      tmp=0;
      for(j=0;j<g_size;j++) tmp -= g_weight_act_const*g_kk[j]*in[nts][j];
      MPI_Allreduce(&tmp,&tmp,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
      out[nte][0]=tmp;
    }
    if(EXTRACOND>1){  /** Extra cond (*)  **/
      tmp=0;
      for(j=0;j<g_size;j++) tmp += phi[nts][j]*in[nts][j];
      MPI_Allreduce(&tmp,&tmp,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
      out[nte][1]=tmp;
    }
  }
}


/*
  in: con  k= 0...ntau-1   +  EXTRACOND (1st procs along tau); k= 0... ntau (> 1st procs along tau) 
  out: dof  h= 0...ntau
  phi[g_nt_tot] is assumed to be projected
  Normally nts=0, nte=g_nt_tot
*/

void matrixt_SD_IMR(double **out, double **in, double dtau, double **phi, int nts, int nte){

  int h,j,p=0,iwa=0,acc=0,dumm=0,ntsm1;
  double dtau2;
  MPI_Status status[2];
  MPI_Request req[2];
  dtau2=0.5*dtau;
  ntsm1=g_nt_loc; /* if nts>0 it should not be accessed */

  if(g_np_ntau>1) xchange_fields_flow_dn(in); /* recv in[-1=g_nt_loc] if not first; send in[g_nt_loc-1] if not last; */
  
  h=nts;
  if(g_proc_coords[g_flowdir]==g_pc_flow_1st){
    for(j=0;j<g_size;j++) out[h][j] = -in[h][j];
  } else {
    for(j=0;j<g_size;j++) out[h][j] = in[ntsm1][j] - in[h][j];
  }
  
  for(h=nts+1;h<nte;h++) for(j=0;j<g_size;j++) out[h][j] = in[h-1][j] - in[h][j];

  h=nte;
  if(g_proc_coords[g_flowdir]==g_pc_flow_last) for(j=0;j<g_size;j++) out[h][j] = in[h-1][j];

  h=nte-1; /* do last term first, in order to send it */
  mhessian_model(g_kk,in[h],phi,h,PLUSHALF,acc,iwa); /* mhessian = - ddS */
  for(j=0;j<g_size;j++) out[h][j] -= dtau2*g_kk[j];
  if(g_proc_coords[g_flowdir] < g_pc_flow_last){
    if(nte==g_nt_loc){
      MPI_Isend(g_kk,g_size,MPI_DOUBLE,g_neigh_node[g_flowdir][UP],7901,g_cart,&req[p]);
      p++;
    }
  } else {
    for(j=0;j<g_size;j++) out[h+1][j] -= dtau2*g_kk[j];
  }

  if((g_proc_coords[g_flowdir] > g_pc_flow_1st) && (nts==0)){ /* recv the other contribution to the first term */
    MPI_Irecv(g_ff,g_size,MPI_DOUBLE,g_neigh_node[g_flowdir][DN],7901,g_cart,&req[p]);
    p++;
  }

  for(h=nts;h<nte-1;h++){
    mhessian_model(g_hh,in[h],phi,h,PLUSHALF,acc,iwa);
    for(j=0;j<g_size;j++) out[h][j] -= dtau2*g_hh[j];
    for(j=0;j<g_size;j++) out[h+1][j] -= dtau2*g_hh[j];
  }

  if(p>0) MPI_Waitall(p,req,status);

  if((g_proc_coords[g_flowdir] > g_pc_flow_1st) && (nts==0)){
    for(j=0;j<g_size;j++) out[0][j] -= dtau2*g_ff[j];
  }

  if(g_proc_coords[g_flowdir]==g_pc_flow_last) projector(out[nte],out[nte],g_proj,0);

  if(g_proc_coords[g_flowdir]==g_pc_flow_1st){
    if(EXTRACOND>0){  /** Extra cond (**) **/
      force_model(g_kk,phi[nts],NULL,nts,dumm,0,1); /* force = - dS/dphi */
      for(j=0;j<g_size;j++) out[nts][j] -= g_weight_act_const*g_kk[j]*in[nte][0];
    }
    if(EXTRACOND>1){  /** Extra cond (*) **/
      for(j=0;j<g_size;j++) out[nts][j] += phi[nts][j]*in[nte][1];
    }
  }
}
