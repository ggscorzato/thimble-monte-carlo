/*****************************************************************************
 file: load_store_config.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <string.h>
#include <complex.h>
#include "global.h"
#include "starting_config.h"
#include "xchange_fields.h"
#include "init_model.h"
#include "load_store_config.h"

void load_store_config(int cnum, double **phi, int ls_flag){

  int mu,start_ind[DIM+2],len_loc[DIM+2],len_glob[DIM+2],periods[DIM+2],nprocp[DIM+2],lsize,rank;
  char filename[30];
  MPI_File fp;
  MPI_Status status;
  MPI_Datatype filetype;
  MPI_Comm io_cart;

  /* define local and global sizes of the DIM+2 array */
  /* VALID ONLY IF g_indexord[mu]=DIM-mu-1. To allow other choices, I should either change the rank-coord mapping
     in MPI_Cart_create, and adapt the code below, or permute the configuration before printing (and after reading). */

  for(mu=0;mu<DIM+2;mu++) periods[mu]=1;
  nprocp[0]=g_np_ntau;
  len_loc[0] = g_nt_loc; /* the flow direction is always the slowest */
  len_glob[0] = g_nt_tot+1;
  start_ind[0] = g_nt_1st;
  if(g_proc_coords[g_flowdir]==g_np_ntau-1) len_loc[0]++;

  for(mu=0;mu<DIM;mu++){
    nprocp[mu+1] = g_nprocl[mu];
    len_loc[mu+1] = g_l[mu]; /* mu=0 is here the next-to-slowest, and so on. */ 
    len_glob[mu+1] = g_L[mu];
    start_ind[mu+1] = g_proc_coords[mu]*g_l[mu];
  }
  nprocp[DIM+1] = 1;
  len_loc[DIM+1] = DPS; /* internal degrees of freedom are always the fastest */
  len_glob[DIM+1] = DPS;
  start_ind[DIM+1] = 0;

  lsize=g_size*len_loc[0];

  /* create MPI type for subarray */
  MPI_Cart_create(g_cart, DIM+2, nprocp, periods, 0, &io_cart);
  MPI_Comm_rank(io_cart,&rank);
  if(g_proc_id != rank){
    fprintf(stderr,"load_store ERROR: rank mismatch");
    exit(-17);
  }
  MPI_Type_create_subarray(DIM+2, len_glob, len_loc, start_ind, MPI_ORDER_C, MPI_DOUBLE, &filetype);
  MPI_Type_commit(&filetype);

  /* configuration name */
  sprintf(filename,"conf_%04d",cnum);
  MPI_Barrier(g_cart);

  if(ls_flag==STORE){

    MPI_File_open(io_cart, filename, MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &fp);
    MPI_File_set_view(fp, 0, MPI_DOUBLE, filetype, "native", MPI_INFO_NULL);
    MPI_File_write_all(fp, phi[0], lsize, MPI_DOUBLE, &status);
    if(g_proc_id==0) printf("config written.\n");

  }else if(ls_flag==LOAD){

    MPI_File_open(io_cart, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &fp);
    MPI_File_set_view(fp, 0, MPI_DOUBLE, filetype, "native", MPI_INFO_NULL);
    MPI_File_read_all(fp, phi[0], lsize, MPI_DOUBLE, &status);
    if(g_np_ntau>1) xchange_fields_flow_up(phi);
    if(g_proc_id==0) printf("config read.\n");
  }
  MPI_File_close(&fp);
  MPI_Barrier(g_cart);

}

void load_store_test(){

  int h,j,ok,Nt;
  Nt=g_nt_loc;

  if(g_proc_id==0) fprintf(stdout,"load store check: config started\n");
  for(h=0;h<Nt;h++) for(j=0;j<g_size;j++) g_field[h][j]=3.0;//g_field[0][j];
  if(g_proc_id==0) fprintf(stdout,"load store check: config copied\n");
  load_store_config(17,g_field,STORE);
  if(g_proc_id==0) fprintf(stdout,"load store check: config stored\n");
  for(h=0;h<Nt;h++) for(j=0;j<g_size;j++) g_field_new[h][j]=7.0;
  if(g_proc_id==0) fprintf(stdout,"load store check: config reset\n");
  load_store_config(17,g_field_new,LOAD);
  if(g_proc_id==0) fprintf(stdout,"load store check: config loaded\n");
  ok=1;
  for(h=0;h<Nt;h++){
    for(j=0;j<g_size;j++){
      if(g_field[h][j]!=g_field_new[h][j]){
	fprintf(stderr,"f[%d,%d] stored: %g neq read: %g\n",h,j,g_field[h][j],g_field_new[h][j]);
	ok=0;
      }
    }
  }
  if(g_proc_id==0 && (ok==1)) fprintf(stdout,"load store check: OK\n");

}

void load_store_toy(int cnum,  int ls_flag){

  int i,j,pe,start_ind[2],len_loc[2],len_glob[2],lsize,shift,periods[2],pdims[2],pcoords[2],rank;
  char filename[30];
  double toy[70];
  MPI_File fp;
  MPI_Status status;
  MPI_Datatype filetype;
  MPI_Comm comm_cart;

  periods[0]=1;
  periods[1]=1;
  len_glob[0] = 7;
  len_glob[1] = 10;

  if(g_np_ntau==1){
    shift=0;
    pdims[0]=1;
    pdims[1]=1;
    len_loc[0] = 7;
    len_loc[1] = 10;
    start_ind[0] = 0;
    start_ind[1] = 0;
    for(j=0;j<len_loc[0];j++){
      for(i=0;i<len_loc[1];i++){
	if(ls_flag==STORE) toy[j*len_loc[1]+i]=j*len_glob[1]+i;
      }
    }
  }
  if(g_np_ntau==2){
    pdims[0]=1;
    pdims[1]=2;
    len_loc[0] = 7;
    len_loc[1] = 5;
    if(g_proc_coords[g_flowdir]==0){
      shift=0;
      start_ind[0] = 0;
      start_ind[1] = 0;
      for(j=0;j<len_loc[0];j++){
	for(i=0;i<len_loc[1];i++){
	  if(ls_flag==STORE) toy[j*len_loc[1]+i]=j*len_glob[1]+i;
	}
      }
    } else {
      shift=0;
      start_ind[0] = 0;
      start_ind[1] = 5;
      for(j=0;j<len_loc[0];j++){
	for(i=0;i<len_loc[1];i++){
	  if(ls_flag==STORE) toy[j*len_loc[1]+i]=j*len_glob[1]+i+start_ind[1];
	}
      }
    }
  }


  if(g_np_ntau==1){
    lsize=70;
  }
  if(g_np_ntau==2){
    lsize=35;
  }


  /* create MPI type for subarray */
  MPI_Cart_create(MPI_COMM_WORLD,2,pdims,periods,0,&comm_cart);
  MPI_Comm_rank(comm_cart,&rank);
  MPI_Cart_coords(comm_cart,rank,2,pcoords);
  start_ind[0] = pcoords[0] * len_loc[0];
  start_ind[1] = pcoords[1] * len_loc[1];
  MPI_Type_create_subarray(2, len_glob, len_loc, start_ind, MPI_ORDER_C, MPI_DOUBLE, &filetype);
  MPI_Type_commit(&filetype);

  /* configuration name */
  sprintf(filename,"toytest_%04d",cnum);
  MPI_Barrier(g_cart);

  if(ls_flag==STORE){

    MPI_File_open(comm_cart, filename, MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &fp);
    MPI_File_set_view(fp, shift, MPI_DOUBLE, filetype, "native", MPI_INFO_NULL);
    MPI_File_write_all(fp, toy, lsize, MPI_DOUBLE, &status);
    if(g_proc_id==0) printf("toy-test written.\n");

  }else if(ls_flag==LOAD){

    MPI_File_open(comm_cart, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &fp);
    MPI_File_set_view(fp, shift, MPI_DOUBLE, filetype, "native", MPI_INFO_NULL);
    MPI_File_read_all(fp, toy, lsize, MPI_DOUBLE, &status);

    if(g_proc_id==0) printf("toy-test read.\n");

    pe=g_proc_coords[g_flowdir];
    if(g_np_ntau==1){
      for(j=0;j<len_loc[0];j++){
	for(i=0;i<len_loc[1];i++){
	  fprintf(stdout,"toy read (pe=%d)[%d]: %f\n",pe,j*len_glob[1]+i,toy[j*len_loc[1]+i]);
	}
      }
    }
    if(g_np_ntau==2){
      if(g_proc_coords[g_flowdir]==0){
	for(j=0;j<len_loc[0];j++){
	  for(i=0;i<len_loc[1];i++){
	    fprintf(stdout,"toy read (pe=%d)[%d]: %f\n",pe,j*len_glob[1]+i,toy[j*len_loc[1]+i]);
	  }
	}
      } else {
	for(j=0;j<len_loc[0];j++){
	  for(i=0;i<len_loc[1];i++){
	    fprintf(stdout,"toy read (pe=%d)[%d]: %f\n",pe,j*len_glob[1]+i+start_ind[1],toy[j*len_loc[1]+i]);
	  }
	}
      }
    }
    

  }
  MPI_File_close(&fp);
  MPI_Barrier(g_cart);

}
