/*****************************************************************************
 file: observables_hub.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: December 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <complex.h>
#include "global.h"
#include "init_model.h"
#include "ferm_action.h"
#include "ferm_matrices.h"
#include "observables.h"
#if (MODEL == HUB)

/* nothing yet */

void online_measurements_hub(double *phi){

  
}

#endif
