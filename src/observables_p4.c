/*****************************************************************************
 file: observables_p4.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <complex.h>
#include "global.h"
#include "bicomplex.h"
#include "init_model.h"
#include "observables.h"
#if (MODEL == P4)

dcomplex density_p4(double *phi){

  int j,k;
  double shmu,chmu;
  dcomplex dens;
  bicomplex field,fieldup;
  
  chmu=cosh(g_chempot);
  shmu=sinh(g_chempot);
  dens=0;
  for(j=0;j<g_vol;j++){
    field.br=phi[j*DPS]   + I * phi[j*DPS+1];
    field.bi=phi[j*DPS+2] + I * phi[j*DPS+3];

    k=g_ineigh[g_timedir][UP][j];
    fieldup.br=phi[k*DPS]   + I * phi[k*DPS+1];
    fieldup.bi=phi[k*DPS+2] + I * phi[k*DPS+3];

    /* <n> = 1/V d/dmu log Z (change sign in Eq. 2.16 of arXiv:0902.4686) */ 
    dens -= shmu*(field.br*fieldup.br + field.bi*fieldup.bi);
    dens += I*chmu*(field.bi*fieldup.br - field.br*fieldup.bi); /* epsilon={{0,-1;1,0}}} */

  }
  MPI_Allreduce(&dens,&dens,1,MPI_DOUBLE_COMPLEX,MPI_SUM,g_sub_cart);

  return dens;
}

dcomplex phi2_p4(double *phi){

  int j;
  dcomplex phi2,ctmp;
  bicomplex field;
  
  phi2=0;
  for(j=0;j<g_vol;j++){
    field.br=phi[j*DPS]   + I * phi[j*DPS+1];
    field.bi=phi[j*DPS+2] + I * phi[j*DPS+3];
    ctmp=0.5*(field.br*field.br + field.bi*field.bi);
    phi2+=ctmp;
  }

  MPI_Allreduce(&phi2,&phi2,1,MPI_DOUBLE_COMPLEX,MPI_SUM,g_sub_cart);

  return phi2;

}

void online_measurements_p4(double *phi){

  dcomplex cdens,cphi2;
  double rdens,rphi2,idens,iphi2;

  cdens=density_model(phi)/g_tvol;
  rdens=creal(cdens);
  idens=cimag(cdens);

  if(g_sub_print_proc==1) fprintf(stdout,"dens: %g, %g; ",rdens,idens);

  cphi2=phi2_model(phi)/g_tvol;
  rphi2=creal(cphi2);
  iphi2=cimag(cphi2);

  if(g_sub_print_proc==1) fprintf(stdout,"phi^2: %g, %g; ",rphi2,iphi2);
  if(g_sub_print_proc==1) fprintf(stdout,"\n");
  
}

#endif
