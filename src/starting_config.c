/*****************************************************************************
 file: starting_config.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: April 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <math.h>
#include <mpi.h>
#include "global.h"
#include "ranlxd.h"
#include "ode_rk4.h"
#include "force.h"
#include "observables.h"
#include "projector.h"
#include "starting_config.h"

/*
  This routine extracts a vector phi[0] projected on the tangent space of the thimble at the origin; it rescales it
  by a factor rescale (<1); it generates the full string of configurations phi[1]...phi[g_nt_tot] with ode_rk4; it
  checks that phi[g_nt_tot] is invariant under a projection (up to tol); if this is not the case, it further rescales
  phi[0] and repeats the procedure until the invriance of phi[g_ntua] under projection is fulfilled.

  phi: (output):
*/

void starting_config(double **phi){

  int j, it=0, rescale_max=10;
  double diff=1.0, tol=1e-7, resc_first, resc_furth=0.33333;
  resc_first=g_init_rescale;

  if(g_proc_id==0) fprintf(stdout,"Generating the first cold configuration ..."); 

  if(g_proc_coords[g_flowdir]==0){
    gauss_vectord(phi[0],g_size);
    for(j=0;j<g_size;j++) phi[0][j] += g_stat_conf[j];

    projector(phi[0],phi[0],g_proj,1);

    for(j=0;j<g_size;j++){
      phi[0][j]  = resc_first*(phi[0][j]-g_stat_conf[j]);
      phi[0][j] += g_stat_conf[j];
    }
  }

  while(diff > tol  && it < rescale_max){
    ode_rk4(phi,&force_model,0,g_nt_loc,1,g_dtau,1,NULL,FW);

    if(g_proc_coords[g_flowdir]==g_np_ntau-1){
      projector(g_kk,phi[g_nt_loc],g_proj,1);

      diff=0.0;
      for(j=0;j<g_size;j++){
	diff+=fabs(g_kk[j]-phi[g_nt_loc][j]);
      }
      MPI_Allreduce(&diff,&diff,1,MPI_DOUBLE,MPI_SUM,g_sub_cart);
    }
    MPI_Bcast(&diff,1,MPI_DOUBLE,g_nproc-1,g_cart);

    if(diff > tol){
      if(g_proc_id==g_nproc-1){
	fprintf(stdout,"\nDifference under projection of the evolved=%g>%g"
		" => further rescale by=%g\n",diff,tol,resc_furth);
      }
      if(g_proc_coords[g_flowdir]==0){
	for(j=0;j<g_size;j++){
	  phi[0][j]  = resc_furth*(phi[0][j]-g_stat_conf[j]);
	  phi[0][j] += g_stat_conf[j];
	}
      }
    }
    it++;
  }
  if(diff > tol){
    if(g_proc_id==0) fprintf(stdout,"Difference under projection still too large: %g>%g. Exiting...\n",diff,tol);
    MPI_Finalize();
    exit(-33);
  }
  if(g_proc_id==0) fprintf(stdout,"OK!\n"); 
}
