/*****************************************************************************
 file: ferm_matrix_hub.c
 package: thimble monte carlo
 author: Luigi Scorzato
 created on date: October 2013
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <math.h>
#include "global.h"
#include "xchange_fields.h"
#include "init_model.h"
#include "ferm_matrices.h"
#if (MODEL == HUB)

/* 
   CONJGATE fermion matrix for the Hubbard model
   eta and etap cannot be the same vector. 
   etap is overwritten.
*/

void ferm_matrix_hub(dcomplex *etap, dcomplex *eta, int fl, int id, double *phi){

  int j,k,l,s,mu;
  dcomplex ctmp;

  xchange_fields((double*)eta);
  
  for(j=0;j<g_vol;j++) etap[j]=id*eta[j];
  
  for(j=0;j<g_vol;j++){
    k=g_ineigh[g_timedir][DN][j];
    s=1;
    if(j<k) s=-1;
    ctmp=s*(1.0 + conj(g_udb*(phi[2*k]+I*phi[2*k+1])) + g_Umu[fl]);
    etap[j]-=ctmp*eta[k];
    for(mu=0;mu<DIM;mu++){
      if(mu!=g_timedir){
	l=g_ineigh[mu][DN][k];
	etap[j]+=s*g_tdb*eta[l];
	l=g_ineigh[mu][UP][k];
	etap[j]-=s*g_tdb*eta[l];
      }
    }
  }
}
#endif
