MATRIX NOISE

		phi 			in 			out

FE	nts+1/2 … nte-1/2		nts … nte		nts … nte-1
					nts … nte-1 (last) 

FB	nts+1/2 … nte-1/2		nts+1 … nte (1st)	nts … nte-1
					nts … nte

FE^t	nts+1/2 … nte-1/2		nts … nte-1 (1st)	nts … nte-1
					nts-1 … nte-1	
					nts … nte-1 (last)
						
FB^t	nts+1/2 … nte-1/2		nts … nte-1 (1st)	nts+1 … nte-1 (1st)
					nts-1 … nte-1		nts … nte-1
								nts … nte (last)
								
MATRIX SD

		phi 			in 			out

M	nts+1/2 … nte-1/2		nts … nte		nts … nte-1 (*)
					

M^t	nts+1/2 … nte-1/2		nts … nte-1 (1st,*)	nts … nte-1
					nts-1 … nte-1		nts … nte (last)



(*): except nte for ncon in 1st
A: ndof -> ncon
A^t: ncon -> ndof


note about ncon vecotrs
g_nt_loc is used for ntsm1 only when g_proc_coords[g_flowdir]>0, 
while 
g_nt_loc is used for conds only when g_proc_coords[g_flowdir]==0.